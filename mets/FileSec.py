from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class FileSec(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="fileSec", namespace='mets')

        self.entries.append(
            XmlEntry(
                [],
                "fileGrp",
                "1",
                [
                    XmlAttribute('ID', 'ancillaryData_' + Constants.dummy_id + '_fileGrp')
                ],
                None,
                namespace='mets'))
        self.entries.append(
            XmlEntry(
                [],
                "fileGrp",
                "1",
                [
                    XmlAttribute('ID', 'playlists_' + Constants.dummy_id + '_fileGrp')
                ],
                None,
                namespace='mets'))
        self.entries.append(
            XmlEntry(
                [],
                "fileGrp",
                "1..n",
                [
                    XmlAttribute('ID', '__prefix___' + Constants.dummy_id + '_fileGrp')
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
