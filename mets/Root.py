from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from mets.Hdr import Hdr
import datetime
from RV import RV


class Root(AbstractFormat):
    optional_checker_reports_element = XmlEntry(
        None,
        "div",
        "0..1",
        [XmlAttribute('TYPE', 'checkerReports')],
        None,
        namespace='mets')

    def __init__(self):
        AbstractFormat.__init__(self, name="mets", namespace='mets')

        self.optional_dmdSec_element = XmlEntry(
                None,
                "dmdSec",
                "0..1",
                [XmlAttribute('ID', Constants.unknown_optional_metadata_place_holder)],
                None,
                namespace='mets')

        self.entries.append(
            XmlEntry(
                None,
                "metsHdr",
                "1",
                [XmlAttribute('CREATEDATE', datetime.datetime.now().isoformat())],
                None,
                namespace='mets'))
        self.entries.append(self.optional_dmdSec_element)
        self.entries.append(
            XmlEntry(
                None,
                "amdSec",
                "1",
                [],
                None,
                namespace='mets'))
        self.entries.append(
            XmlEntry(
                None,
                "fileSec",
                "1",
                [],
                None,
                namespace='mets'))
        self.entries.append(
            XmlEntry(
                [
                    XmlEntry(
                        [
                            XmlEntry(
                                None,
                                "div",
                                "1",
                                [
                                    XmlAttribute('TYPE', 'ancillaryData')
                                ],
                                None,
                                namespace='mets'
                            ),
                            XmlEntry(
                                None,
                                "div",
                                "1",
                                [
                                    XmlAttribute('TYPE', 'playlists')
                                ],
                                None,
                                namespace='mets'
                            ),
                            XmlEntry(
                                None,
                                "div",
                                "1",
                                [
                                    XmlAttribute('TYPE', 'subPackages')
                                ],
                                None,
                                namespace='mets'
                            )
                        ],
                        "div",
                        "1",
                        [
                            XmlAttribute('ID', 'preservationPackage_' + Constants.dummy_id),
                            XmlAttribute('TYPE', 'CPP-Package'),
                            XmlAttribute('DMDID', Constants.unknown_metadata_place_holder),
                            XmlAttribute('LABEL', Constants.unknown_metadata_place_holder),
                            XmlAttribute('ADMID', Constants.unknown_metadata_place_holder)
                        ],
                        None,
                        namespace='mets')
                ],
                "structMap",
                "1",
                [XmlAttribute('TYPE', 'physical')],
                None,
                namespace='mets'))
        self.entries.append(
            XmlEntry(
                [
                    XmlEntry(
                        None,
                        "div",
                        "1..n",
                        [
                            XmlAttribute('DMDID', Constants.unknown_metadata_place_holder),
                            XmlAttribute('LABEL', Constants.unknown_metadata_place_holder)
                        ],
                        None,
                        namespace='mets'
                    )
                ],
                "structMap",
                "0..1",
                [XmlAttribute('TYPE', 'logical')],
                None,
                namespace='mets'))

    def add_dmd_sec_element(self):
        rv = RV.info('Adding optional dmdSec element to existing Mets element')

        if self.elem is None:
            return rv.add_child(RV.error('Mets element has not been created yet!'))

        dmd_sec_elem = self.elem.find('./mets:dmdSec', Constants.ns)
        if dmd_sec_elem is None:
            self.add_children(self.elem, self.optional_dmdSec_element, force=True)

        return rv

    @staticmethod
    def checker_reports_elem(pkl_root_elem):
        checker_report_div_elem = pkl_root_elem.find(
            './mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="checkerReports"]', Constants.ns)
        return checker_report_div_elem

    @staticmethod
    def add_checker_reports_div_element(pkl_root_elem):
        rv = RV.info('Adding optional checkerReports element to existing Mets element')

        if pkl_root_elem is None:
            return rv.add_child(RV.error('Mets element has not been created yet!')), False

        parent_elem = pkl_root_elem.find('./mets:structMap[@TYPE="physical"]/mets:div', Constants.ns)

        if parent_elem is not None:
            Root.add_children(parent_elem, Root.optional_checker_reports_element, force=True)

        return rv, True

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        rv = RV.info('Collecting metadata for: ' + self.name)
        rv_sub, self.root_elem = self.gen_stub()

        # append metsHdr
        metsHdr_elem = self.root_elem.find("./mets:metsHdr", Constants.ns)
        if metsHdr_elem is None:
            return rv.add_child(RV.error('METS Header element is None - unexpected behaviour. Aborting'))

        else:
            # generate and append metsHdr
            metsHdr = Hdr()
            rv_sub, elem = metsHdr.gen_stub()
            if not rv_sub.is_info():
                rv.add_child(rv_sub)
                if rv_sub.is_error():
                    return rv, None
            insert_cnt = 0
            for child in elem:
                metsHdr_elem.insert(insert_cnt, child)
                insert_cnt += 1

        '''
        I THINNK THIS IS NOT NECESSARY ANYMORE
        
        # append dmdSec
        dmdSec_elem = root_elem.find("./mets:dmdSec", Constants.ns)
        if dmdSec_elem is None:
            log.error('Timed Text Package Format element is None - unexpected behaviour. Aborting')
            return
        else:
            # generate and append imagePackageFormat
            mdRef = MdRef()
            elem = mdRef.collect_metadata(media_source_file, xml_metadata_file_in)
            insert_cnt = 0
            for child in elem:
                dmdSec_elem.insert(insert_cnt, child)
                insert_cnt += 1

        # append amdSec
        amdSec_elem = root_elem.find("./mets:amdSec", Constants.ns)
        if amdSec_elem is None:
            log.error('Timed Text Package Format element is None - unexpected behaviour. Aborting')
            return
        else:
            # generate and append imagePackageFormat
            digiprovMD = DigiprovMD()
            elem = digiprovMD.collect_metadata(media_source_file, xml_metadata_file_in)
            amdSec_elem.insert(0, elem)

        # append fileSec
        fileSec_elem = root_elem.find("./mets:fileSec", Constants.ns)
        if fileSec_elem is None:
            log.error('Timed Text Package Format element is None - unexpected behaviour. Aborting')
            return
        else:
            # generate and append imagePackageFormat
            fileSec = FileSec()
            elem = fileSec.collect_metadata(media_source_file, xml_metadata_file_in)
            insert_cnt = 0
            for child in elem:
                fileSec_elem.insert(insert_cnt, child)
                insert_cnt += 1
        '''
        rv.message = 'Successfully collected metadata for: ' + self.name
        return rv, self.root_elem
