from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class File(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="file", namespace='mets', attributes=[
            XmlAttribute('ID', Constants.unknown_metadata_place_holder),
            XmlAttribute('MIMETYPE', Constants.unknown_metadata_place_holder),
            XmlAttribute('SIZE', Constants.unknown_metadata_place_holder),
            XmlAttribute('CHECKSUM', Constants.unknown_metadata_place_holder),
            XmlAttribute('CHECKSUMTYPE', Constants.unknown_metadata_place_holder)
        ])

        self.entries.append(
            XmlEntry(
                None,
                "FLocat",
                "1",
                [
                    XmlAttribute('LOCTYPE', 'URL'),
                    XmlAttribute('type', 'simple', 'xlink'),
                    XmlAttribute('href', Constants.unknown_metadata_place_holder, 'xlink')
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
