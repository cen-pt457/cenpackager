import sys
import os
from lxml import etree
from utils.log import log
from mets.Root import Root
import uuid


def main(argv):
    print(str(uuid.uuid4()))

    source_file = '/Users/sbg/Documents/python_playground/cenpackager/unittest/test_material/short_j2c_clip/image_000000.j2c'
    cmd = Root()
    d = cmd.collect_metadata(media_source_file=source_file, xml_metadata_file_in=None)
    if d is None:
        return
    text_rep = etree.tostring(d, pretty_print=True, encoding='utf8', method='xml').decode()
    print(text_rep)
    return


if __name__ == "__main__":
    main(sys.argv)
