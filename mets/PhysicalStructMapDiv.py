from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class PhysicalStructMapDiv(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="div", namespace='mets', attributes=[
            XmlAttribute('ID', Constants.unknown_metadata_place_holder),
            XmlAttribute('LABEL', Constants.unknown_metadata_place_holder),
            XmlAttribute('TYPE', Constants.unknown_metadata_place_holder)
        ])

        self.entries.append(
            XmlEntry(
                [],
                "div",
                "0..1",
                [
                    XmlAttribute('TYPE', 'ancillaryData'),
                    XmlAttribute('ID', 'ancillaryData_' + Constants.dummy_id)
                ],
                None,
                namespace='mets'))

        self.entries.append(
            XmlEntry(
                [],
                "div",
                "0..1",
                [
                    XmlAttribute('TYPE', 'data'),
                    XmlAttribute('ID', 'data_' + Constants.dummy_id),
                    XmlAttribute('ADMID', Constants.unknown_metadata_place_holder)
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
