from ebucore.XmlTypes import XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class FileGrp(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="fileGrp", namespace='mets', attributes=[
            XmlAttribute('ID', Constants.unknown_metadata_place_holder)
        ])

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
        return cpp_metadata_elem
