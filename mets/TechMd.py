from ebucore.XmlTypes import XmlEntry, XmlAttribute
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from RV import RV


class TechMd(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="techMd", namespace='mets', attributes=[
            XmlAttribute('ID', Constants.unknown_metadata_place_holder)
        ])

        self.entries.append(
            XmlEntry(
                [],
                "mdRef",
                "1",
                [
                    XmlAttribute('LOCTYPE', 'URL'),
                    XmlAttribute('href', Constants.unknown_metadata_place_holder, 'xlink'),
                    XmlAttribute('MDTYPE', 'OTHER'),
                    XmlAttribute('OTHERMDTYPE', 'EBUCORE'),
                    XmlAttribute('MIMETYPE', 'text/xml'),
                    XmlAttribute('SIZE', Constants.unknown_metadata_place_holder),
                    XmlAttribute('CHECKSUM', Constants.unknown_metadata_place_holder),
                    XmlAttribute('CHECKSUMTYPE', Constants.unknown_metadata_place_holder)
                ],
                None,
                namespace='mets'))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        rv = RV.info('Collecting metadata for ' + self.name)

        rv_sub, tech_md_elem = self.gen_stub()
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, None

        '''
        md_ref = MdRef()
        rv_sub, md_ref_elem = md_ref.gen_stub()
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, None

        tech_md_elem.insert(len(list(tech_md_elem)), md_ref_elem)
        '''
        rv.message = 'Successfully collected metadata for ' + self.name
        return rv, tech_md_elem
