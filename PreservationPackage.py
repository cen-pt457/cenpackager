import os
import uuid
from utils.log import log
import Config
from utils.utils import num_provenance_metadata_files, open_xml_and_remove_namespace
import shutil
from PreservationPackagePkl import PreservationPackagePkl
from ebucore.EbuCoreMain import EbuCoreMain
from ebucore.CoreMetadataWorkDescriptive import CoreMetadataDescriptive
from ebucore.CoreMetadataVariantDescriptive import CoreMetadataVariantDescriptive
from Content.AudioVisualPackage import AudioVisualPackage
from Content.ImagePackage import ImagePackage
from Content.SoundPackage import SoundPackage
from Content.AbstractPackage import AbstractPackage
from Content.ComponentizedPackage import ComponentizedPackage
from Content.TimedTextPackage import TimedTextPackage
from Content.ExtraPackage import ExtraPackage
from Content.Playlist import Playlist
from lxml import etree
from RV import RV
from SubPackagePkl import SubPackagePkl
from SubPackage import SubPackage
from utils.utils import get_id_from_filename
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
import lxml


class PreservationPackage:
	"""
	Main Class of the current project. Handles create/update/add/delete requests for a CEN Package.
	Most of the functions are implemented in a static way thus allowing convenient usage from
	command line without managing a session
	"""

	sub_package_types = [
		"imagePackage", "soundPackage", "timedTextPackage", "audiovisualPackage", "componentizedPackage",
		"extraPackage"]

	'''
	Sub-package can only be added to CPP's root folder. Additional content types can be added to the root folder 
	or to a sub-folder as specified below:
								CPP		sub-package
	ancillaryData				x		x
	playlist					x
	checkerReport				x
	provMD						x		x
	descMD						x
	'''
	additional_content_types = ["ancillaryData", "playlist", "checkerReport", "provMD", "descMD"]

	stub_generators = ["variantDescMD"]

	def __init__(self):
		"""Constructor: Unused"""
		pass

	@staticmethod
	def read(cpp_path_abs):
		"""
		Reads an existing package structure - not implemented yet

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:return:
		"""
		log.warning("[NOT IMPLEMENTED YET] CENPackage - read called, path: " + cpp_path_abs)

	@staticmethod
	def create(cpp_path_abs):
		"""
		Creates an empty CEN Package, builds the desired folder structure and creates the CENPackingList.xml

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info('Creating new CEN package: ' + cpp_path_abs)
		if os.path.exists(cpp_path_abs):
			return rv.add_child(RV.error("Given path already exists: " + cpp_path_abs))

		os.mkdir(cpp_path_abs)

		# generate new UUID for this package
		new_cen_packing_list_uuid = str(uuid.uuid4())

		# write mandatory folders
		os.mkdir(os.path.join(cpp_path_abs, 'metadata'))

		# write root metadata as specified in 10.2 Root metadata
		dm_file_name = 'descMD_' + new_cen_packing_list_uuid + '_0000.xml'

		ebucore_main = EbuCoreMain()
		rv_sub, ebucore_main_elem = ebucore_main.collect_metadata(
			media_source_file=None,
			xml_metadata_file_in=None)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv
		if ebucore_main_elem is None:
			return rv.add_child(RV.error('Error while trying to generate EBUCORE Main element'))

		cmd = CoreMetadataDescriptive()
		rv_sub, cmd_elem = cmd.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		if cmd_elem is None:
			return rv.add_child(RV.error('Error while trying to generate Root ebucore Stub'))

		ebucore_main_elem.insert(0, cmd_elem)

		tree = etree.ElementTree(ebucore_main_elem)
		tree.write(
			os.path.join(cpp_path_abs, 'metadata', dm_file_name), pretty_print=True, xml_declaration=True, encoding="utf-8")

		# write packing list according to 7.3
		pkl = PreservationPackagePkl()
		rv_sub = pkl.update_preservation_packing_list(cpp_path_abs, new_cen_packing_list_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		return rv

	@staticmethod
	def add_subpackage(content_type, path_package, path_item, name_item=''):
		"""
		Adds a sub-package to an existing CEN Preservation Package

		Parameters
		----------
		content_type : str, mandatory
			content type of data to be added. Only restricted support for content types - see usage
		path_package : str, mandatory
			absolute path to existing CEN Preservation Package
		path_item : str, mandatory
			absolute path the content that shall be added to CEN Preservation Package
		name_item : str, optional
			optional, additional name for the added item. Not used at the moment but as placeholder for
			future developments
		"""
		rv = RV.info('Adding subpackage ' + path_item + ' to existing package: ' + path_package)
		sub_package_uuid = uuid.uuid4()
		sub_package_path = os.path.join(path_package, content_type + '_' + str(sub_package_uuid))

		try:
			data_path = os.path.join(sub_package_path, 'data')
			metadata_path = os.path.join(sub_package_path, 'metadata')

			if os.path.isdir(path_item) is False:
				if not os.path.exists(data_path):
					os.makedirs(data_path)

			if os.path.exists(metadata_path) is False:
				os.makedirs(metadata_path)

			if content_type == 'componentizedPackage':
				content_instance = ComponentizedPackage()
			elif content_type == 'audiovisualPackage':
				content_instance = AudioVisualPackage()
			elif content_type == 'imagePackage':
				content_instance = ImagePackage()
			elif content_type == 'soundPackage':
				content_instance = SoundPackage()
			elif content_type == 'timedTextPackage':
				content_instance = TimedTextPackage()
			elif content_type == 'extraPackage':
				content_instance = ExtraPackage()
			else:
				content_instance = AbstractPackage()

			# -----------------------------------------------------------------
			# Copy data to new subpackage folder
			# -----------------------------------------------------------------
			rv_sub = content_instance.copy_subpackage_data(path_item, data_path)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

			# -----------------------------------------------------------------
			# generate technical metadata for each file in data folder
			# -----------------------------------------------------------------
			rv_sub = content_instance.write_subpackage_metadata(sub_package_uuid, sub_package_path)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

			# -----------------------------------------------------------------
			# write Subpackage's Packing List
			# -----------------------------------------------------------------
			rv_sub = SubPackagePkl.update(sub_package_path, content_type, sub_package_uuid)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv
			return rv
		except IndexError as e:
			log.error("Caught index error: " + e.message)
			pass
		finally:
			if rv_sub.is_error():
				# clean-up
				if os.path.exists(sub_package_path):
					shutil.rmtree(sub_package_path)

	@staticmethod
	def update_subpackage(path_package, path_item):
		"""
		Updates existing sub package. Might be called after missing metadata has been added manually to
		the sub package's metadata file

		Parameters
		----------
		path_package : str, mandatory
			absolute path to existing CEN Preservation Package
		path_item : str, mandatory
			absolute path the content that shall be added to CEN Preservation Package
		"""
		rv = RV.info('Updating subpackage ' + path_item + ' to existing package: ' + path_package)

		rv_sub, content_type = SubPackage.get_content_type_from_path(path_item)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, False

		# get ./packingList modification time
		mod_time_pkl = os.path.getmtime(os.path.join(path_package, path_item, 'packingList.xml'))

		# check if ./metadata/techMd_ is newer
		sub_package_id = get_id_from_filename(path_item)
		expected_metadata_file = os.path.join(
			path_package, path_item, 'metadata', 	'techMD_' + sub_package_id + '_0000.xml')
		if not os.path.exists(expected_metadata_file):
			return rv.add_child(RV.error('No matching metadata file: ' + expected_metadata_file)), False
		mod_time_metadata = os.path.getmtime(expected_metadata_file)

		rv_sub, subpackage_data_changed = SubPackagePkl.check_data_changed(path_package, path_item)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, False

		subpackage_metadata_changed = mod_time_pkl < mod_time_metadata

		if not subpackage_metadata_changed and not subpackage_data_changed:
			# package seems to be unchanged
			return rv.add_child(RV.warning('No change in subpackage''s data or metadata detected')), False

		if subpackage_data_changed:
			sub_package_id = str(uuid.uuid4())

		# check if metadata's modification time is newer than pkl's annotation time (metadata altered manually)
		if subpackage_metadata_changed:
			if Config.force_add_and_skip_schema_check is False:
				rv_sub = AbstractFormat.xsd_schema_check(expected_metadata_file)
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv, False

			# ToDo: Implement option so a subpackage will get copied in order to have a backup!
			full_subpackage_path = os.path.join(path_package, path_item)
			rv_sub = SubPackagePkl.update(full_subpackage_path, content_type, sub_package_id)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, False

		else:
			# ToDo: Discuss in group if we should expect  someone altered the SubpackagePKL manually
			log.info('pkl is newer')

		if subpackage_data_changed:
			# -----------------------------------------------------------------
			# write Subpackage's Packing List
			# -----------------------------------------------------------------
			full_subpackage_path = os.path.join(path_package, path_item)
			rv_sub = SubPackagePkl.update(full_subpackage_path, content_type, sub_package_id)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, False

			# rename techMD file - use new UUID
			new_md_file_name = 'techMD_' + sub_package_id + '_0000.xml'
			new_md_file_path = os.path.join(path_package, path_item, 'metadata', new_md_file_name)
			os.rename(expected_metadata_file, new_md_file_path)
			# update sub_package_folder - use new UUID
			old_subpackage_folder_name = os.path.join(path_package, path_item)
			new_subpackage_folder_name = os.path.join(path_package, content_type + '_' + sub_package_id)
			os.rename(old_subpackage_folder_name, new_subpackage_folder_name)

		return rv, subpackage_data_changed

	@staticmethod
	def add_ancillary_data(path_destination_abs, path_item):
		"""
		Adds ancillary data to an existing CEN Preservation Package
		preservationPackingList.xml will be updated by calling function

		Parameters
		----------
		path_destination_abs : str, mandatory
			absolute path to existing CEN Preservation Package
		path_item : str, mandatory
			absolute path the content that shall be added to CEN Preservation Package
		"""
		rv = RV.info('Adding ancillary data (' + path_item + ') to package: ' + path_destination_abs)

		if not os.path.exists(path_destination_abs):
			return rv.add_child(RV.error('Package path not existing: ' + path_destination_abs))
		if not os.path.exists(path_item):
			return rv.add_child(RV.error('Item to add not existing: ' + path_item))

		ancillary_data_path = os.path.join(path_destination_abs, 'ancillaryData')
		if not os.path.exists(ancillary_data_path):
			os.makedirs(ancillary_data_path)

		if os.path.isdir(path_item):
			return rv.add_child(RV.error('Given path points to a folder - provide path to a file: ' + path_item))

		# ToDo: Add some checks if this item shall be added.
		try:
			shutil.copy(path_item, ancillary_data_path)
		except OSError as e:
			return rv.add_child(RV.error('While trying to copy ancillary data file. Error: %s' % e))

		return rv

	@staticmethod
	def add_item(content_type, path_package, path_item, name_item=''):
		"""
		Adds an existing item to an existing CEN Package and updates all necessary packing list recursively

		Parameters
		----------
		content_type : str, mandatory
			can be one of the following list: BinPackage, ComponentizedPackage, ClipPackage, ExtrasPackage,
			MetadataPackage
		path_package : str, mandatory
			path of the existing CEN Package
		path_item : str, mandatory
			points to the content to be added to CEN Package
		name_item : str, mandatory
			User defined name e.g. for BinPackage/ImagePackage
		"""
		rv = RV.info('Adding ' + content_type + ' to existing package: ' + path_package)
		if not os.path.exists(path_package):
			return rv.add_child(RV.error("Given path for existing CENPackage does not exist: " + path_package))

		if not os.path.exists(path_item):
			return rv.add_child(RV.error("Given path for content to add does not exist: " + path_item))

		if content_type not in PreservationPackage.sub_package_types and \
			content_type not in PreservationPackage.additional_content_types:
			return rv.add_child(RV.error("Given type of the item unknown: " + content_type + '(' + content_type + ')'))

		if content_type == 'ancillaryData':
			rv_sub = PreservationPackage.add_ancillary_data(path_package, path_item)
		elif content_type == 'playlist':
			rv_sub = PreservationPackage.add_playlist(path_package, path_item)
		elif content_type == 'checkerReport':
			rv_sub = PreservationPackage.add_checker_report(path_package, path_item)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
			return rv
		elif content_type == 'provMD':
			rv_sub = PreservationPackage.add_provenance_metadata(path_package, path_item)
		elif content_type == 'descMD':
			rv_sub = PreservationPackage.add_variant_descriptive_metadata(path_package, path_item)
		else:
			rv_sub = PreservationPackage.add_subpackage(content_type, path_package, path_item, name_item)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# -----------------------------------------------------------------
		# write Preservation Package Packing List according to 7.3
		# -----------------------------------------------------------------
		new_cen_packing_list_uuid = str(uuid.uuid4())
		pkl = PreservationPackagePkl()

		rv_sub = pkl.update_preservation_packing_list(path_package, new_cen_packing_list_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		rv.message = 'Successfully added ' + content_type + ' to package: ' + path_package
		return rv

	@staticmethod
	def update_item(package_path, sub_package_path):
		"""
		Update sub folder packingList.xml list as well as preservationPackingList.xml.

		Parameters
		----------
		package_path : str, mandatory
			absolute path the the existing EN package
		sub_package_path : str, mandatory
			relative path from package_path pointing to the sub package subject to be updated
		"""
		rv = RV.info('Updating CEN package ' + package_path + ' to existing package: ' + sub_package_path)

		update_path = os.path.join(package_path, sub_package_path)

		sub_package_data_changed = False
		cen_package_metadata_changed = False

		if sub_package_path != "metadata":
			path_technical_metadata = os.path.join(update_path, 'metadata')
			if os.path.exists(path_technical_metadata) is False:
				return rv.add_child(RV.error('No meta data sub-folder in given sub package path: ' + str(update_path)))

			rv_sub, sub_package_data_changed = PreservationPackage.update_subpackage(package_path, sub_package_path)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

		else:
			# check if any file in metadata sub-folder is never than preservationPackingList.xml
			rv_sub, cen_package_metadata_changed = PreservationPackage.check_metadata_change(package_path)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

		# -----------------------------------------------------------------
		# write Preservation Package Packing List according to 7.3
		# -----------------------------------------------------------------
		rv_sub, packing_list_uuid = PreservationPackagePkl.preservation_package_uuid(package_path)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		if sub_package_data_changed is False and cen_package_metadata_changed is False:
			return rv.add_child(RV.warning("No change(s) detected for: " + str(update_path)))

		# Metadata change will not update CEN Packages UUID - only changes in sub-packages will
		if sub_package_data_changed:
			packing_list_uuid = str(uuid.uuid4())
		pkl = PreservationPackagePkl()

		rv_sub = pkl.update_preservation_packing_list(package_path, packing_list_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv
		return rv

	@staticmethod
	def check_metadata_change(package_path):

		rv = RV.info('Checking for CEN package metadata change: ' + package_path)

		preservation_pkl_path = os.path.join(package_path, 'preservationPackingList.xml')
		if os.path.exists(preservation_pkl_path) is False:
			return rv.add_child(RV.error(
				"Path for preservationPackingList does not exist: " + preservation_pkl_path)), False

		mod_time_pkl = os.path.getmtime(preservation_pkl_path)

		preservation_package_md_path = os.path.join(package_path, 'metadata')
		if os.path.exists(preservation_pkl_path) is False:
			return rv.add_child(RV.error("CEN Package has no metadata sub-folder: " + package_path)), False

		from utils.utils import get_folders_and_files
		files_and_folders = get_folders_and_files(preservation_package_md_path, 1)

		for file_or_folder in files_and_folders:
			if file_or_folder[2] == '':
				continue
			metadata_path_abs = os.path.join(preservation_package_md_path, file_or_folder[1], file_or_folder[2])
			metadata_mod_time = os.path.getmtime(metadata_path_abs)

			package_metadata_changed = mod_time_pkl < metadata_mod_time
			if package_metadata_changed:
				return rv, True

		# If we reach this code, no metadata was modified after the last write time of cen packages
		# preservationPackingList.xml - returning False here.
		return rv, False





	@staticmethod
	def add_variant_descriptive_metadata(cpp_path_abs, variant_descriptive_path_abs):
		"""
		Adds variant descriptive metadata to an existing CPP.
		New file will be stored in CPP's metadata sub-folder. It will be renamed to "descMD_" + a unique ID.
		Function will also update the CPP's PPKL if necessary.

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param variant_descriptive_path_abs: absolute path pointing to a variant descriptive metadata file that
											shall be added to the CPP
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info(
			'Adding variant descriptive metadata ' + variant_descriptive_path_abs + ' + to existing package: ' +
			cpp_path_abs)

		destination_metadata_folder = os.path.join(cpp_path_abs, 'metadata')
		if os.path.exists(destination_metadata_folder) is False:
			return rv.add_child(RV.error('Given package has no metadata folder: ' + destination_metadata_folder))

		if Config.force_add_and_skip_schema_check is False:
			rv_sub = AbstractFormat.xsd_schema_check(variant_descriptive_path_abs)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv
		else:
			rv.add_child(RV.warning("Skipping schema check since -f (force) option was selected!"))

		num_existing_descriptive_md_files = \
			PreservationPackagePkl.num_descriptive_metadata_files(destination_metadata_folder)

		new_file_id = str(num_existing_descriptive_md_files).zfill(4)
		rv_sub, cpp_uuid = PreservationPackagePkl.preservation_package_uuid(cpp_path_abs)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		new_filename_rel = 'descMD_' + cpp_uuid + '_' + new_file_id + '.xml'

		new_filename_abs = os.path.join(destination_metadata_folder, new_filename_rel)
		try:
			shutil.copy(variant_descriptive_path_abs, new_filename_abs)
		except OSError as e:
			return rv.add_child(RV.error('While trying to copy ancillary data file. Error: %s' % e))

		return rv

	@staticmethod
	def add_provenance_metadata(cpp_path_abs, provenance_metadata_path_abs):

		"""
		Adds provenance metadata to an existing CPP __OR__ to an existing sub-package.
		New file will be stored in CPP's metadata sub-folder. It will be renamed to "digiProvMD_" + a unique ID.
		Function will also update the CPP's PPKL if necessary.

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param provenance_metadata_path_abs: absolute path pointing to a provenance metadata file that
											shall be added to the CPP
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info(
			'Adding provenance metadata ' + provenance_metadata_path_abs + ' + to existing package: ' +
			cpp_path_abs)

		# add provenance metadata to cen package
		# get first available provenance md number
		destination_metadata_folder = os.path.join(cpp_path_abs, 'metadata')
		if os.path.exists(destination_metadata_folder) is False:
			return rv.add_child(RV.error('Given package has no metadata folder: ' + destination_metadata_folder))

		num_existing_provenance_md_files = num_provenance_metadata_files(destination_metadata_folder)

		new_file_id = str(num_existing_provenance_md_files).zfill(4)
		rv_sub, cpp_uuid = PreservationPackagePkl.preservation_package_uuid(cpp_path_abs)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		new_filename_rel = 'digiProvMD_' + cpp_uuid + '_' + new_file_id + '.xml'

		new_filename_abs = os.path.join(destination_metadata_folder, new_filename_rel)
		try:
			shutil.copy(provenance_metadata_path_abs, new_filename_abs)
		except OSError as e:
			return rv.add_child(RV.error('While trying to copy ancillary data file. Error: %s' % e))
		return rv
	'''
	@staticmethod
	def add_provenance_metadata_to_sub_package(cpp_path_abs, sub_package_path_rel, provenance_metadata_path_abs):
		# ToDo: Move to Subpackage??
		"""
		Add provenance metadata to existing subpackage.

		Parameters
		----------
		cpp_path_abs : str, mandatory
			absolute path the the existing CEN package
		sub_package_path_rel : str, mandatory
			relative path from package_path pointing to the sub package subject to be updated
		provenance_metadata_path_abs : str, mandatory
			absolute path pointing to existing provenance metadata file to be added
		"""
		sub_package_path_abs = os.path.join(cpp_path_abs, sub_package_path_rel)
		rv = RV.info(
			'Adding provenance metadata ' + provenance_metadata_path_abs + ' + to existing package: ' +
			sub_package_path_abs)

		if os.path.exists(cpp_path_abs) is False:
			return rv.add_child(RV.error('Given CEN package does not exists: ' + cpp_path_abs))

		if os.path.exists(provenance_metadata_path_abs) is False:
			return rv.add_child(RV.error(
				'Given path to existing provenance metadata does not exists: ' + provenance_metadata_path_abs))

		if os.path.exists(sub_package_path_abs) is False:
			return rv.add_child(RV.error('Given sub package does not exists: ' + sub_package_path_abs))

		destination_metadata_folder = os.path.join(sub_package_path_abs, 'metadata')
		if os.path.exists(destination_metadata_folder) is False:
			return rv.add_child(RV.error('Given sub package has no metadata folder: ' + destination_metadata_folder))

		# get UUID of existing sub package
		sub_package_id = PreservationPackingList.get_id_from_filename(sub_package_path_abs)

		# get first available provenance md number
		num_existing_provenance_md_files = \
			PreservationPackingList.num_provenance_metadata_files(destination_metadata_folder)

		new_file_id = str(num_existing_provenance_md_files).zfill(4)
		new_filename_rel = 'digiProvMD_' + sub_package_id + '_' + new_file_id + '.xml'

		new_filename_abs = os.path.join(destination_metadata_folder, new_filename_rel)
		# ToDo: Add some checks if this item shall be added.
		try:
			shutil.copy(provenance_metadata_path_abs, new_filename_abs)
		except OSError as e:
			return rv.add_child(RV.error('While trying to copy ancillary data file. Error: %s' % e))

		# -----------------------------------------------------------------
		# write Subpackage's Packing List
		# -----------------------------------------------------------------
		rv_sub, subpackage_type = CENPackage.get_content_type_from_path(sub_package_path_abs)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		rv_sub = SubpackagePkl.update(sub_package_path_abs, subpackage_type, sub_package_id)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv
		return rv
	'''

	@staticmethod
	def add_checker_report(cpp_path_abs, new_checker_reports_list_path_abs):
		"""
		Adds checker reports to an existing CPP.
		New file will be stored in CPP's checkerReports sub-folder.

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param new_checker_reports_list_path_abs: absolute path pointing to a checker-report that shall be added to CPP
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info(
			'Adding checker-reports ' + new_checker_reports_list_path_abs + ' to existing package: ' + cpp_path_abs)

		if os.path.exists(cpp_path_abs) is False:
			return rv.add_child(RV.error('Given path does not exist: ' + cpp_path_abs))
		if os.path.exists(new_checker_reports_list_path_abs) is False:
			return rv.add_child(RV.error('Given checker report does not exist: ' + new_checker_reports_list_path_abs))
		if os.path.isdir(new_checker_reports_list_path_abs) is True:
			return rv.add_child(RV.error(
				'Given checker report is a directory (path to checkerReportsList.xml expected)'
				': ' + new_checker_reports_list_path_abs))

		checker_reports_folder_path_abs = os.path.join(cpp_path_abs, 'checkerReports')

		# check if there is already a file called ./checkerReports/checkerReportsList.xml
		checker_reports_xml_path_abs = os.path.join(checker_reports_folder_path_abs, 'checkerReportsList.xml')
		if os.path.exists(checker_reports_xml_path_abs) is True:
			return rv.add_child(RV.error(
				'Found existing checkerReportsList.xml file. Merging of checker-reports not is'
				' not implemented. If checker reports shall be added to existing reports, '
				'manual merging is necessary. Existing checker-reports can be removed from the'
				' package without corrupting the package: ' + checker_reports_xml_path_abs))

		# execute xsd check on given checkerReportsList.xml
		rv_sub = PreservationPackage.checker_report_xsd_check(new_checker_reports_list_path_abs)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		try:
			# We remove the namespace to make sure that there is no mix of tags with namespaces and tags without
			rv_sub, root = open_xml_and_remove_namespace(new_checker_reports_list_path_abs, Constants.ns['crl'])
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv
		except etree.XMLSyntaxError as err:
			return rv.add_child(RV.error('While trying to parse checkerReportsList.xml file: ' + str(err)))

		new_checker_reports_dir_path_abs = os.path.dirname(new_checker_reports_list_path_abs)
		location_elems = root.findall('//checkerFileDescription/location', Constants.ns)

		for location_elem in location_elems:
			checker_result_path_abs = os.path.join(new_checker_reports_dir_path_abs, location_elem.text)
			if os.path.exists(checker_result_path_abs) is False:
				return rv.add_child(RV.error(
					'Referenced checker result not part of given checker report set: ' + checker_result_path_abs))

		try:
			shutil.copytree(new_checker_reports_dir_path_abs, checker_reports_folder_path_abs)
		except OSError as e:
			return rv.add_child(RV.error('While trying to copy checker reports. Error: %s' % e))

		return rv

	@staticmethod
	def add_playlist(cpp_path_abs, playlist_path_abs):
		"""
		Adds a playlist to an existing CPP.
		New file will be stored in CPP's playlists sub-folder. It will be renamed to "playlist_" + a unique ID.
		Function will also update the CPP's PPKL if necessary.

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param playlist_path_abs: absolute path pointing to a playlist which shall be added to CPP
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info('Adding playlist ' + playlist_path_abs + ' + to existing package: ' + cpp_path_abs)

		if os.path.exists(playlist_path_abs) is False:
			return rv.add_child(RV.error('Given playlist file does not exist: ' + playlist_path_abs))

		if Config.force_add_and_skip_schema_check is False:
			rv_sub = PreservationPackage.playlist_xsd_check(playlist_path_abs)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

		if os.path.exists(cpp_path_abs) is False:
			return rv.add_child(RV.error('Given path does not exist: ' + cpp_path_abs))

		# check whether linked parts are in the current package. Otherwise raise warnings
		rv_sub, state = Playlist.check(cpp_path_abs, playlist_path_abs)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)  # just add warnings - don't return false here

		playlist_folder_path_abs = os.path.join(cpp_path_abs, 'playlists')
		if os.path.exists(playlist_folder_path_abs) is False:
			os.mkdir(playlist_folder_path_abs)
		else:
			if os.path.isdir(playlist_folder_path_abs) is False:
				return rv.add_child(RV.error('Playlist is a file but should be a folder: ' + cpp_path_abs))

		while True:
			# get num existing playlists reports
			playlist_uuid = str(uuid.uuid4())
			playlist_filename_abs = os.path.join(playlist_folder_path_abs, 'playlist_' + playlist_uuid + '.xml')
			if os.path.exists(playlist_filename_abs) is False:
				break

		try:
			shutil.copy(playlist_path_abs, playlist_filename_abs)
		except OSError as e:
			return rv.add_child(RV.error('While trying to copy playlist. Error: %s' % e))

		return rv

	@staticmethod
	def delete_item(path_package_abs, sub_package_path):
		"""
		Deletes item(s) from CEN Package and updates all necessary packing list recursively

		Parameters
		----------
		path_package_abs : str, mandatory
			path of the existing CEN Package
		sub_package_path : str, mandatory
			relative path to the content to be deleted from CEN Package
		"""
		rv = RV.info('Deleting  ' + sub_package_path + ' from CEN Package: ' + path_package_abs)

		if not os.path.exists(path_package_abs):
			return rv.add_child(RV.error("Given path for existing CENPackage does not exist: " + path_package_abs))

		full_path = os.path.join(path_package_abs, sub_package_path)

		if not os.path.exists(full_path):
			return rv.add_child(RV.error("Given path for content to remove does not exist: " + sub_package_path))

		# delete folder and sub folders
		try:
			shutil.rmtree(full_path)
		except OSError as e:
			return rv.add_child(RV.error('While trying to delete asset from CENPackage. Error: %s' % e))

		# Update preservation packing list
		new_cen_packing_list_uuid = str(uuid.uuid4())
		pkl = PreservationPackagePkl()
		rv_sub = pkl.update_preservation_packing_list(path_package_abs, new_cen_packing_list_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		return rv

	@staticmethod
	def checker_report_xsd_check(checker_report_path_abs):
		"""
		Applies XSD schema check for checker report files.

		:param checker_report_path_abs: absolute path pointing to a check report which shall be checked
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info('Applying checker report XSD schema check for file: ' + checker_report_path_abs)

		filename_xsd_rel = './xsd/checker_report_registry.xsd'
		with open(filename_xsd_rel) as file_xsd:
			doc = etree.parse(file_xsd)
		try:
			schema = etree.XMLSchema(doc)
		except lxml.etree.XMLSchemaParseError as e:
			return rv.add_child(RV.error(str(e)))
		with open(checker_report_path_abs) as file_ebucore:
			doc = etree.parse(file_ebucore)
		try:
			schema.assertValid(doc)
		except lxml.etree.DocumentInvalid as e:
			for error in schema.error_log:
				rv.add_child(RV.error("  Line {}: {}".format(error.line, error.message)))
			return rv
		return rv

	@staticmethod
	def playlist_xsd_check(playlist_path_abs):
		"""
		Applies XSD schema check for playlist files.

		:param playlist_path_abs: absolute path pointing to a playlist which shall be checked
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info('Applying playlist XSD schema check for file: ' + playlist_path_abs)

		filename_xsd_rel = './xsd/playlist.xsd'
		with open(filename_xsd_rel) as file_xsd:
			doc = etree.parse(file_xsd)
		try:
			schema = etree.XMLSchema(doc)
		except lxml.etree.XMLSchemaParseError as e:
			return rv.add_child(RV.error(str(e)))
		with open(playlist_path_abs) as file_ebucore:
			doc = etree.parse(file_ebucore)
		try:
			schema.assertValid(doc)
		except lxml.etree.DocumentInvalid as e:
			for error in schema.error_log:
				rv.add_child(RV.error("  Line {}: {}".format(error.line, error.message)))
			return rv
		return rv

	@staticmethod
	def generate_descriptive_metadata_stub(variant_descriptive_stub_path_abs):
		"""
		Generates a stub for descriptive metadata.

		:param variant_descriptive_stub_path_abs: absolute path where the stub will be stored
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""

		rv = RV.info('Generating variant descriptive metadata stub: ' + variant_descriptive_stub_path_abs)
		if os.path.exists(variant_descriptive_stub_path_abs):
			return rv.add_child(RV.ERROR(
				"File for storing variant descriptive metadata already exists: " + variant_descriptive_stub_path_abs))

		dir_name_abs = os.path.dirname(variant_descriptive_stub_path_abs)
		if os.path.exists(dir_name_abs) is False:
			return rv.add_child(RV.ERROR(
				"Directory for storing descriptive metadata stub not existing: " + dir_name_abs))

		# generate ebucore main
		ebucore_main = EbuCoreMain()
		rv_sub, ebucore_main_md = ebucore_main.collect_metadata(
			media_source_file=None,
			xml_metadata_file_in=None)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# generate CoreMetadataVariantDescriptive
		md_variant_desc = CoreMetadataVariantDescriptive()
		rv_sub, md_variant_desc_elem = md_variant_desc.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		ebucore_main_md.insert(len(ebucore_main_md), md_variant_desc_elem)
		try:
			if ebucore_main_md is not None:
				et = etree.ElementTree(ebucore_main_md)
				et.write(variant_descriptive_stub_path_abs, pretty_print=True, xml_declaration=True, encoding="utf-8")
			return rv
		except:
			return rv.add_child(RV.ERROR(
				"While trying to store stub: " + variant_descriptive_stub_path_abs))

	@staticmethod
	def is_preservation_package(cpp_path_abs):
		"""
		Checks if a given folder is a CPP. Checks for a metadata sub folder as well as preservationPackingList.xml

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:return:True if given folder is a CPP, otherwise False
		"""
		if not os.path.exists(cpp_path_abs):
			return False

		# check for metadata sub-folder
		if not os.path.exists(os.path.join(cpp_path_abs, 'metadata')):
			return False

		# check for preservationPackingList.xml
		if not os.path.exists(os.path.join(cpp_path_abs, 'preservationPackingList.xml')):
			return False

		return True
