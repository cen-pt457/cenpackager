from AbstractFormat import AbstractFormat
from ebucore.ReferencedStandard import ReferencedStandard


class AncillaryData(AbstractFormat):
	referenced_standards = list()

	referenced_standards.append(
		ReferencedStandard(
			'ISO 19005-1', 'PDF/A-1', '', 'https://www.iso.org/obp/ui/#iso:std:iso:19005:-1:ed-1:v2:en'
		)
	)
	referenced_standards.append(
		ReferencedStandard(
			'ISO 32000-2', 'PDF/A-2', '', 'https://www.iso.org/obp/ui/#iso:std:iso:32000:-2:ed-2:v1:en'
		)
	)
	referenced_standards.append(
		ReferencedStandard(
			'ISO 32000-3', 'PDF/A-3', '', 'https://www.iso.org/obp/ui/#iso:std:iso:32000:-2:ed-2:v1:en'
		)
	)
	referenced_standards.append(
		ReferencedStandard(
			'ISO 32000-4', '', 'PDF/A-4', 'https://www.iso.org/obp/ui/#iso:std:iso:32000:-2:ed-2:v1:en'
		)
	)
	referenced_standards.append(
		ReferencedStandard(
			'ISO 12234-2', 'TIFF_6', '', 'https://www.iso.org/standard/29377.html'
		)
	)

	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata")

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		pass
