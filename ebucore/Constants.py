
class Constants:
    one_convention = 'the following element is mandatory, exactly one element shall be present'
    zero_to_one_convention = 'the following element is optional, it shall appear zero or one time'
    one_to_n_convention = 'The following element shall appear at least one time and may be repeated'
    zero_to_n_convention = 'The following element may not be present, or may be present one or multiple times'

    ns = {
        "ebucore": "urn:ebu:metadata-schema:ebucore",
        "dc": "http://purl.org/dc/elements/1.1/",
        "xml": "http://www.w3.org/XML/1998/namespace",
        "xlink": "http://www.w3.org/1999/xlink/",
        "mets": "http://www.loc.gov/METS/",
        "crl": "urn:cen.eu:en17650:2022:ns:checkerReportsList"
    }

    unknown_metadata_place_holder = '__MANDATORY_METADATA__TYPE_VALUE_HERE__'
    unknown_optional_metadata_place_holder = '__OPTIONAL_METADATA__TYPE_VALUE_HERE_OR_DELETE__'

    dummy_id = '1a2a3a4a-5a6a-7a8a-9a0a-1a2a3a4a'

    def __init__(self):
        pass
