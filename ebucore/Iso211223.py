class InterlacedMode:
	def __init__(self, field_value, meaning):
		self.field_value = field_value
		self.meaning = meaning


class SamplingStructure:
	def __init__(self, field_value, meaning, num_components):
		self.field_value = field_value
		self.meaning = meaning
		self.num_components = num_components


class Iso211223:
	interlaced_modes = list()
	interlaced_modes.append(
		InterlacedMode(0, 'Progressive frame (frame contains one full-height picture)')
	)
	interlaced_modes.append(
		InterlacedMode(1, 'Interlaced frame (first picture is top field)')
	)
	interlaced_modes.append(
		InterlacedMode(2, 'Interlaced frame (second picture is top field)')
	)
	interlaced_modes.append(
		InterlacedMode(3, 'Reserved')
	)

	sampling_structures = list()
	sampling_structures.append(SamplingStructure(0, '4:2:2 (YCbCr)', 3))
	sampling_structures.append(SamplingStructure(1, '4:4:4 (YCbCr)', 3))
	sampling_structures.append(SamplingStructure(2, '4:4:4 (RGB)', 3))
	sampling_structures.append(SamplingStructure(3, '4:2:0 (YCbCr)', 3))
	sampling_structures.append(SamplingStructure(4, '4:2:2:4 (YCbCrAux)', 4))
	sampling_structures.append(SamplingStructure(5, '4:4:4:4 (YCbCrAux)', 4))
	sampling_structures.append(SamplingStructure(6, '4:4:4:4 (RGBAux)', 4))
	sampling_structures.append(SamplingStructure(7, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(8, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(9, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(10, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(11, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(12, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(13, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(14, 'Reserved', 0))
	sampling_structures.append(SamplingStructure(15, 'Reserved', 0))
