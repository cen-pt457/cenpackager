from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.ImagePackageContainerFormatMatch import ImagePackageContainerFormatMatch
from RV import RV


class ImagePackageContainerFormat(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="containerFormat", namespace='ebucore')

		self.attributes = [
					XmlAttribute('containerFormatName', Constants.unknown_metadata_place_holder),
					XmlAttribute('containerFormatDefinition', Constants.unknown_optional_metadata_place_holder)
				]

		self.entries.append(
			XmlEntry(
					None,
					"technicalAttributeString",
					"1",
					[XmlAttribute('typeLabel', 'containerStandardReference')],
					XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
					None,
					"technicalAttributeUnsignedInteger",
					"1",
					[XmlAttribute('typeLabel', 'containerWidth')],
					XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
					None,
					"technicalAttributeUnsignedInteger",
					"1",
					[XmlAttribute('typeLabel', 'containerHeight')],
					XmlText(Constants.unknown_metadata_place_holder)))
		self.entries.append(XmlEntry(
					None,
					"technicalAttributeUri",
					"1",
					[XmlAttribute('typeLabel', 'containerStandardReference')],
					XmlText(Constants.unknown_metadata_place_holder)))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for ' + self.name)
		rv_sub, cpp_metadata_elem = \
			self.match_xpath_metadata(
				media_source_file, ImagePackageContainerFormatMatch, xml_metadata_file_in)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None
		# rv.message = 'Successfully collected metadata for ' + self.name
		return rv, cpp_metadata_elem
