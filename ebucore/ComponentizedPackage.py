from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore.VideoFormat import VideoFormat
from ebucore.AudioFormat import AudioFormat
from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from ebucore_match.ComponentizedPackageFormatMatch import ComponentizedPackageFormatMatch
from ebucore_match.CplFormatMatch import CplFormatMatch

import os
from lxml import etree
from RV import RV


class ComponentizedPackage(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata", namespace='ebucore')
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"0..1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"title",
				"0..1",
				[XmlAttribute('typeLabel', "subpackageName")],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "fileOrigin"),
					XmlAttribute('typeNamespace', "en17650:2022"),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#compFileOrigin')
				],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "userText")
				],
				None))
		self.optional_audio_format = XmlEntry(
						None, "format", "0..n", [XmlAttribute('formatName', 'audioFormat')], None)

		self.entries.append(
			XmlEntry(

				[
					XmlEntry(
							None,
							"technicalAttributeString",
							"1",
							[
								XmlAttribute('typeLabel', 'packageType'),
								XmlAttribute('typeNamespace', 'en17650:2022'),
								XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#componentizedType')
							],
							XmlText(Constants.unknown_metadata_place_holder)),

					XmlEntry(
							None,
							"technicalAttributeString",
							"0..1",
							[
								XmlAttribute('typeLabel', 'packageSubtype')
							],
							XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
							None,
							"technicalAttributeString",
							"1",
							[
								XmlAttribute('typeLabel', 'pklAnnotationText')
							],
							XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
							None,
							"technicalAttributeString",
							"0..n",
							[
								XmlAttribute('typeLabel', 'referencedPackage')
							],
							XmlText(Constants.unknown_metadata_place_holder)),
					XmlEntry(
							None,
							"technicalAttributeBoolean",
							"1",
							[
								XmlAttribute('typeLabel', 'selfContainedPackage')
							],
							XmlText(Constants.unknown_metadata_place_holder))
				],
				"format",
				"1",
				[XmlAttribute('formatName', 'componentizedPackageFormat')],
				None))
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
							None,
							"format",
							"0..1",
							[],
							XmlText(Constants.unknown_optional_metadata_place_holder),
							namespace='dc'),
					XmlEntry(
							None,
							"videoFormat",
							"1..n",
							[],
							None),
					XmlEntry(None,
							"audioFormat",
							"0..n",
							[],
							None),
					XmlEntry(
							[
								XmlEntry(
									None,
									"editUnitNumber",
									"1",
									[],
									XmlText(Constants.unknown_metadata_place_holder))
							],
							"duration",
							"1",
							[
								XmlAttribute('typeLabel', 'durationInFrames')
							],
							None),
					XmlEntry(
							None,
							"technicalAttributeString",
							"1",
							[
								XmlAttribute('typeLabel', 'cplContentTitleText')
							],
							XmlText(Constants.unknown_metadata_place_holder)),
							],
				"format",
				"1..n",
				[XmlAttribute('formatName', 'cplFormat')],
				None))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for ' + self.name)

		if os.path.isdir(media_source_file):
			rv.add_child(
				RV.error(
					'Given Componentized package source path is a directory. Please provide file path to a '
					'PacKingList (PKL) you want to add. ' + media_source_file))
			return rv, None

		# ------------------------------------------------------------
		# get type of componentized package
		# ------------------------------------------------------------
		from Content.ComponentizedPackage import ComponentizedPackage as ContentComponentizedPackage
		rv_sub, abs_cpl_paths = ContentComponentizedPackage.get_cpls(os.path.dirname(media_source_file))
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		if len(abs_cpl_paths) < 1:
			rv.add_child(RV.error('Package includes no CPLs: ' + media_source_file))
			return rv, None

		rv_sub, referenced_standard = ComponentizedPackageFormatMatch.cpl_type(abs_cpl_paths[0])
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		# ------------------------------------------------------------
		# componentizedPackage
		# ------------------------------------------------------------
		rv_sub, cp_elem = self.match_xpath_metadata(
			media_source_file, ComponentizedPackageFormatMatch, xml_metadata_file_in)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None
		if cp_elem is None:
			rv.add_child(RV.error('Componentized Package Format XML element is None.'))
			return rv, None

		# ------------------------------------------------------------
		# cplFormat
		# ------------------------------------------------------------
		cpl_elem = cp_elem.find("./ebucore:format[@formatName='cplFormat']", Constants.ns)
		format_cpl_format_stub = etree.fromstring(etree.tostring(cpl_elem))
		cp_elem.remove(cpl_elem)

		if cpl_elem is None:
			rv.add_child(RV.error('Container Format element is None - unexpected behaviour. Aborting'))
			return rv, None
		else:

			# cp_elem.remove(cpl_elem)
			# ------------------------------------------------------------
			# 10.3.1.7.3
			# go through all files in folder that holds the PKL.
			# check if any of those files is a CPL
			# Add a cplFormat block for each CPL
			# ------------------------------------------------------------

			cp_folder = os.path.dirname(media_source_file)
			for file in sorted(os.listdir(cp_folder)):
				full_data_path = os.path.join(cp_folder, file)
				if not ContentComponentizedPackage.is_cpl(full_data_path):
					continue

				abs_cpl_path = full_data_path

				# ------------------------------------------------------------
				# new format cpl-format element
				# ------------------------------------------------------------
				format_cpl_format = etree.fromstring(etree.tostring(format_cpl_format_stub))

				# ------------------------------------------------------------
				# Match CPLs metadata
				# ------------------------------------------------------------
				rv_sub, format_cpl_format = self.match_xpath_metadata(abs_cpl_path, CplFormatMatch, format_cpl_format)

				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv, None

				video_format_element = format_cpl_format.find('./ebucore:videoFormat', Constants.ns)
				if video_format_element is None:
					rv.add_child(RV.error('Video Format element is None - unexpected behaviour. Aborting'))
					return rv, None

				# ------------------------------------------------------------
				# Match video metadata
				# ------------------------------------------------------------
				# late import to avoid circular imports
				if referenced_standard['cpl_type'] == 'IMF':
					from Content.ComponentizedPackage import ComponentizedImfPackage as ContentComponentizedPackage
				else:
					from Content.ComponentizedPackage import ComponentizedPackage as ContentComponentizedPackage

				rv_sub, abs_cpls_picture_track_file_paths = \
					ContentComponentizedPackage.get_picture_track_files(abs_cpl_path, media_source_file)
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv, None
				if len(abs_cpls_picture_track_file_paths) == 0:
					rv.add_child(RV.error('No CPL track file paths returned while collecting metadata. Aborting'))
					return rv, None

				# ------------------------------------------------------------
				# generate _generic_ videoFormat
				# ------------------------------------------------------------
				video_format = VideoFormat()
				rv_sub, elem = video_format.collect_metadata(abs_cpls_picture_track_file_paths[0], xml_metadata_file_in)
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv, None

				# ------------------------------------------------------------
				# Add package-specific metadata for DCPs
				# ------------------------------------------------------------
				if referenced_standard['cpl_type'] == 'DCP':
					from ebucore_match.VideoFormatMatch import VideoFormatMatchDcp as VideoFormatMatch
					video_format = VideoFormat()
					rv_sub, elem = video_format.match_xpath_metadata(
						abs_cpls_picture_track_file_paths[0], VideoFormatMatch, elem)
					if not rv_sub.is_info():
						rv.add_child(rv_sub)
						if rv_sub.is_error():
							return rv, None

				idx_video_format = video_format_element.getparent().index(video_format_element)
				format_cpl_format.remove(video_format_element)
				format_cpl_format.insert(idx_video_format, elem)

				audio_format_element = format_cpl_format.find('./ebucore:audioFormat', Constants.ns)

				# ------------------------------------------------------------
				# generate and append audioFormat
				# ------------------------------------------------------------
				audio_format = AudioFormat()
				rv_sub, elem = audio_format.collect_metadata(abs_cpl_path, xml_metadata_file_in)
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv, None

				idx_audio_format = audio_format_element.getparent().index(audio_format_element)
				format_cpl_format.remove(audio_format_element)
				format_cpl_format.insert(idx_audio_format, elem)

				cp_elem.insert(len(cp_elem), format_cpl_format)

		# ------------------------------------------------------------
		# set dc title with subpackage name
		# ------------------------------------------------------------
		sub_package_data_folder_abs = os.path.abspath(os.path.join(media_source_file, os.pardir))

		if sub_package_data_folder_abs.endswith('/data'):
			sub_package_folder_abs = os.path.abspath(os.path.join(sub_package_data_folder_abs, os.pardir))
			dc_title = os.path.basename(sub_package_folder_abs)
			dc_title_elem = cp_elem.find("./ebucore:title/dc:title", Constants.ns)
			dc_title_elem.text = dc_title
		else:
			rv.add_child(RV.warning('File not in subpackage\'s data sub-folder, path is: ' + str(media_source_file)))

		return rv, cp_elem
