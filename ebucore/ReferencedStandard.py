class ReferencedStandard:
	def __init__(self, standard, short_designation_loc, short_designation_cpp, link):
		self.standard = standard
		self.short_designation_loc = short_designation_loc
		self.short_designation_cpp = short_designation_cpp
		self.link = link


class ReferencedStandards:
	timed_text = list()
	timed_text.append(
		ReferencedStandard(
			'W3C ttml-imsc1.1', '', 'IMSC1.1', 'https://www.w3.org/TR/ttml-imsc1.1/'
		)
	)
	timed_text.append(
		ReferencedStandard(
			'SMPTE ST 428-7', '', 'DCDM_Subtitle', 'https://ieeexplore.ieee.org/document/7291893'
		)
	)
	timed_text.append(
		ReferencedStandard(
			'ISO/IEC 15948', 'PNG', '', 'https://www.iso.org/standard/29581.html'
		)
	)

	sound_package = list()
	sound_package.append(ReferencedStandard(
		'RIFF Wave', 'WAVE', '', 'https://en.wikipedia.org/wiki/WAV')
	)
	sound_package.append(ReferencedStandard(
		'ITU-R BS.1352-3', 'WAVE_BWF_1', '', 'https://standards.globalspec.com/std/1080161/BR.1352-3')
	)
	sound_package.append(ReferencedStandard(
		'ITU-R BS.2088', 'WAVE_BWF_2', '', 'https://www.itu.int/rec/R-REC-BS.2088/en')
	)

	image_package = list()
	image_package.append(
		ReferencedStandard(
			'SMPTE ST 268', 'DPX_2', '', 'https://ieeexplore.ieee.org/document/7292028'
		)
	)
	image_package.append(
		ReferencedStandard(
			'ISO 12234-2', 'TIFF_6', '', 'https://www.iso.org/standard/29377.html'
		)
	)
	image_package.append(
		ReferencedStandard(
			'Open EXR', '', 'OPENEXR', 'https://www.openexr.com/'
		)
	)
	image_package.append(
		ReferencedStandard(
			'ISO/IEC 15444-1', 'JP2_FF', '', 'https://www.iso.org/standard/78321.html'
		)
	)

	audiovisual_package = list()
	audiovisual_package.append(
		ReferencedStandard('ISO/IEC 14496-10', 'MPEG-4_AVC', '', 'https://www.iso.org/standard/75400.html')
	)
	audiovisual_package.append(
		ReferencedStandard('ISO/IEC 23008-2', '', 'H265_HEVC', 'https://www.iso.org/standard/75484.html')
	)
	audiovisual_package.append(
		ReferencedStandard('ISO/IEC CD 23090-3', '', 'H266_VCC', 'https://www.iso.org/standard/73022.html')
	)
	audiovisual_package.append(
		ReferencedStandard('ISO/IEC 15444-1', 'J2K_C', '', 'https://www.iso.org/standard/78321.html')
	)

	audiovisual_package_container = list()
	audiovisual_package_container.append(
		ReferencedStandard(
			'ISO/IEC 14496-14', 'MP4_FF_2', '', 'https://www.iso.org/standard/75929.html'
		)
	)
	audiovisual_package_container.append(
		ReferencedStandard(
			'SMPTE 377-1', 'MXF', '', 'https://ieeexplore.ieee.org/abstract/document/8984681'
		)
	)

	ancillary_data = list()
	ancillary_data.append(
		ReferencedStandard(
			'ISO 19005-1', 'PDF/A-1', '', 'https://www.iso.org/obp/ui/#iso:std:iso:19005:-1:ed-1:v2:en'
		)
	)
	ancillary_data.append(
		ReferencedStandard(
			'ISO 32000-2', 'PDF/A-2', '', 'https://www.iso.org/obp/ui/#iso:std:iso:32000:-2:ed-2:v1:en'
		)
	)
	ancillary_data.append(
		ReferencedStandard(
			'ISO 32000-3', 'PDF/A-3', '', 'https://www.iso.org/obp/ui/#iso:std:iso:32000:-2:ed-2:v1:en'
		)
	)
	ancillary_data.append(
		ReferencedStandard(
			'ISO 32000-4', '', 'PDF/A-4', 'https://www.iso.org/obp/ui/#iso:std:iso:32000:-2:ed-2:v1:en'
		)
	)
	ancillary_data.append(
		ReferencedStandard(
			'ISO 12234-2', 'TIFF_6', '', 'https://www.iso.org/standard/29377.html'
		)
	)

	componentized_package = list()
	componentized_package.append(
		ReferencedStandard(
			'SMPTE 429-* or ISO 26429-*', '', 'DCPSMPTE', 'https://www.iso.org/obp/ui/#!iso:std:50204:en'
		)
	)
	componentized_package.append(
		ReferencedStandard(
			'SMPTE ST 2067-21', '', 'IMF2E', 'https://ieeexplore.ieee.org/document/9097487'
		)
	)
	componentized_package.append(
		ReferencedStandard(
			'SMPTE ST 2067-40', '', 'IMF4', 'https://ieeexplore.ieee.org/document/7565459'
		)
	)
	componentized_package.append(
		ReferencedStandard(
			'SMPTE ST 2067-50', '', 'IMF5', 'https://ieeexplore.ieee.org/document/8320049'
		)
	)

