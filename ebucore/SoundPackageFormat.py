from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from AbstractFormat import AbstractFormat
from ebucore_match.SoundPackageFormatMatch import SoundPackageFormatMatch
from ebucore.Constants import Constants
from RV import RV


class SoundPackageFormat(AbstractFormat):
    def __init__(self):
        AbstractFormat.__init__(self, name="soundPackageFormat", namespace='ebucore')

        self.entries.append(XmlEntry(
                [
                    XmlEntry(None,
                             "normalPlayTime",
                             "1",
                             [],
                             XmlText(Constants.unknown_optional_metadata_place_holder))
                ],
                "duration",
                "0..1",
                [],
                None))
        self.entries.append(XmlEntry(
                [
                    XmlEntry(None,
                             "editUnitNumber",
                             "1",
                             [
                                 XmlAttribute('editRate', Constants.unknown_optional_metadata_place_holder),
                                 XmlAttribute('factorNumerator', Constants.unknown_optional_metadata_place_holder),
                                 XmlAttribute('factorDenominator', Constants.unknown_optional_metadata_place_holder)],
                             XmlText(Constants.unknown_optional_metadata_place_holder))
                ],
                "duration",
                "0..1",
                [],
                None))
        self.entries.append(
            XmlEntry(None,
                     "technicalAttributeString",
                     "1",
                     [XmlAttribute('typeLabel', 'filenamePrefix')],
                     XmlText(Constants.unknown_metadata_place_holder)))
        self.entries.append(XmlEntry(None,
                     "technicalAttributeString",
                     "1",
                     [XmlAttribute('typeLabel', 'filenameExtension')],
                     XmlText(Constants.unknown_metadata_place_holder)))
        self.entries.append(XmlEntry(None,
                     "technicalAttributeUnsignedByte",
                     "1",
                     [XmlAttribute('typeLabel', 'filenameDigits')],
                     XmlText(Constants.unknown_metadata_place_holder)))


        self.entries.append(XmlEntry(None,
                     "technicalAttributeUnsignedInteger",
                     "0..1",
                     [XmlAttribute('typeLabel', 'firstActiveSample')],
                     XmlText(Constants.unknown_optional_metadata_place_holder)))
        self.entries.append(XmlEntry(None,
                     "technicalAttributeUnsignedInteger",
                     "0..1",
                     [XmlAttribute('typeLabel', 'lastActiveSample')],
                     XmlText(Constants.unknown_optional_metadata_place_holder)))

    def collect_metadata(self, media_source_file, xml_metadata_file_in):
        rv = RV.info('Collecting metadata for ' + self.name)
        rv_sub, cpp_metadata_elem = self.match_xpath_metadata(media_source_file, SoundPackageFormatMatch, xml_metadata_file_in)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, None
        return rv, cpp_metadata_elem
