from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from AbstractFormat import AbstractFormat
from ebucore.Constants import Constants
from ebucore_match.Dummy import Dummy


class CoreMetadataDescriptive(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata", namespace='ebucore')

		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"1",
					[
						XmlAttribute(
							'lang',
							Constants.unknown_metadata_place_holder,
							namespace='xml'
						)
					],
					XmlText(Constants.unknown_metadata_place_holder),
					namespace='dc')],
				"title",
				"1",
				[XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder)],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"1",
					[
						XmlAttribute(
							'lang',
							Constants.unknown_metadata_place_holder,
							namespace='xml'
						)
					],
					XmlText(Constants.unknown_metadata_place_holder),
					namespace='dc')],
				"alternativeTitle",
				"0..n",
				[XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder)],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[XmlEntry(
						None,
						"name",
						"1",
						[],
						XmlText(Constants.unknown_metadata_place_holder))],
					"contactDetails",
					"1..n",
					[XmlAttribute('contactId', Constants.unknown_metadata_place_holder)],
					None),
				XmlEntry(
					None,
					"role",
					"1",
					[XmlAttribute('typeLabel', 'cast')],
					None)],
				"contributor",
				"0..n",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[XmlEntry(
						None,
						"name",
						"1",
						[],
						XmlText(Constants.unknown_metadata_place_holder))],
					"contactDetails",
					"1..n",
					[XmlAttribute('contactId', Constants.unknown_metadata_place_holder)],
					None),
				XmlEntry(
					None,
					"role",
					"1",
					[XmlAttribute('typeLabel', 'credits')],
					None),
				XmlEntry(
						None,
						"role",
						"1",
						[XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder)],
						None)],
				"contributor",
				"0..n",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[XmlEntry(
						None,
						"organisationName",
						"1",
						[],
						XmlText(Constants.unknown_metadata_place_holder))],
					"organisationDetails",
					"1..n",
					[XmlAttribute('organisationId', Constants.unknown_metadata_place_holder)],
					None),
				XmlEntry(
					None,
					"role",
					"1",
					[XmlAttribute('organisationName', 'productionCompany')],
					None)],
				"publisher",
				"0..n",
				[],
				None))
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
						[
							XmlEntry(
								[
									XmlEntry(
										None,
										"name",
										"1",
										[
											XmlAttribute(
												'lang',
												Constants.unknown_metadata_place_holder,
												namespace='xml'
											)
										],
										XmlText(Constants.unknown_metadata_place_holder)
									)
								],
								"location",
								"1",
								[
									XmlAttribute('typeLabel', 'countryOfReference'),
									XmlAttribute('locationId', Constants.unknown_metadata_place_holder)
								],
								None)
						],
						"spatial",
						"1",
						[],
						None)],
				"coverage",
				"1",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"language",
					"1",
					[],
					XmlText(Constants.unknown_metadata_place_holder),
					namespace='dc')],
				"language",
				"1",
				[XmlAttribute('typeLabel', 'originalLanguage')],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"created",
					"1",
					[
						XmlAttribute('startYear', Constants.unknown_metadata_place_holder),
						XmlAttribute('endYear', Constants.unknown_metadata_place_holder)
					],
					None)],
				"date",
				"1",
				[XmlAttribute('typeLabel', 'yearOfReference')],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"identifier",
					"1",
					[],
					XmlText(Constants.unknown_metadata_place_holder),
					namespace='dc')],
				"identifier",
				"1..n",
				[
					XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder),
					XmlAttribute('formatLabel', 'URI')
				],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"genre",
					"1",
					[
						XmlAttribute('typeLabel', Constants.unknown_metadata_place_holder),
					],
					None)],
				"type",
				"1",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					[
						XmlEntry(
							None,
							"identifier",
							"1",
							[],
							XmlText(Constants.unknown_metadata_place_holder),
							namespace='dc')
					],
					"relationIdentifier",
					"1",
					[],
					None)],
				"isRelatedTo",
				"0..n",
				[],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"organisationDetails",
					"1",
					[],
					None)],
				"metadataProvider",
				"1",
				[],
				None))

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		cpp_metadata_elem = self.match_xpath_metadata(media_source_file, Dummy, xml_metadata_file_in)
		return cpp_metadata_elem
