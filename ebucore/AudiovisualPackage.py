from AbstractFormat import AbstractFormat
from ebucore.AudioFormat import AudioFormat
from ebucore.AudiovisualPackageContainerFormat import AudiovisualPackageContainerFormat
from ebucore.Constants import Constants
from ebucore.VideoFormat import VideoFormat
from ebucore.XmlTypes import XmlEntry, XmlAttribute, XmlText
from ebucore_match.AudioVisualPackageFormatMatch import AudioVisualPackageFormatMatch
from RV import RV
import os


class AudiovisualPackage(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="coreMetadata", namespace='ebucore')

		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"title",
					"0..1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"title",
				"0..1",
				[XmlAttribute('typeLabel', "subpackageName")],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "fileOrigin"),
					XmlAttribute('typeNamespace', "en17650:2022"),
					XmlAttribute('typeLink', 'urn:cen.eu:en17650:2022:ns:metadata#commonFileOrigin')
				],
				None))
		self.entries.append(
			XmlEntry([
				XmlEntry(
					None,
					"description",
					"1",
					[],
					XmlText(Constants.unknown_optional_metadata_place_holder),
					namespace='dc')],
				"description",
				"0..1",
				[
					XmlAttribute('typeLabel', "userText")
				],
				None))
		self.optional_audio_format = XmlEntry(
				None, "format", "0..n", [XmlAttribute('formatName', 'audioFormat')], None)

		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
						[
							XmlEntry(
								None,
								"normalPlayTime",
								"1",
								[],
								XmlText(Constants.unknown_optional_metadata_place_holder))
						],
						"duration",
						"0..1",
						[],
						None),
					XmlEntry(
						[
							XmlEntry(
								None,
								"timecode",
								"1",
								[],
								XmlText(Constants.unknown_optional_metadata_place_holder))
						],
						"duration",
						"0..1",
						[],
						None),
					XmlEntry(
							[
								XmlEntry(
									None,
									"timecode",
									"0..1",
									[],
									XmlText(Constants.unknown_optional_metadata_place_holder))
							],
							"duration",
							"1",
							[],
							None),
					XmlEntry(
							[
								XmlEntry(
									None,
									"timecode",
									"0..1",
									[],
									XmlText(Constants.unknown_optional_metadata_place_holder))
							],
							"start",
							"0..1",
							[],
							None),
					XmlEntry(
							[
								XmlEntry(
									None,
									"timecode",
									"0..1",
									[],
									XmlText(Constants.unknown_optional_metadata_place_holder))
							],
							"end",
							"0..1",
							[],
							None)
				],
				"format",
				"1",
				[XmlAttribute('formatName', 'audiovisualPackageFormat')],
				None))
		self.entries.append(
			XmlEntry(
				[
					XmlEntry(
						None,
						"mimeType",
						"0..1",
						[XmlAttribute('typeLabel', Constants.unknown_optional_metadata_place_holder)],
						None)
				],
				"format",
				"1",
				[XmlAttribute('formatName', 'containerFormat')],
				None))

		self.entries.append(
			XmlEntry(
				None,
				"format",
				"1..n",
				[XmlAttribute('formatName', 'videoFormat')],
				None))
		self.entries.append(self.optional_audio_format)

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for ' + self.name)

		rv_sub, av_package_elem = self.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		# ------------------------------------------------------------
		# audiovisualPackageFormat
		# ------------------------------------------------------------
		avpf_elem = av_package_elem.find("./ebucore:format[@formatName='audiovisualPackageFormat']", Constants.ns)
		if avpf_elem is None:
			rv.add_child(RV.error('Package Format element is None'))
			return rv, None
		else:
			rv_sub, elem = self.match_xpath_metadata(
				media_source_file, AudioVisualPackageFormatMatch, avpf_elem)

			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None

		# ------------------------------------------------------------
		# containerFormat
		# ------------------------------------------------------------
		cf_elem = av_package_elem.find("./ebucore:format[@formatName='containerFormat']", Constants.ns)
		if cf_elem is None:
			rv.add_child(RV.error('Container Format element is None'))
			return rv, None
		else:
			# generate and append containerFormat
			container_format = AudiovisualPackageContainerFormat()
			rv_sub, elem = container_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None

			cf_elem.insert(0, elem)

		# ------------------------------------------------------------
		# videoFormat
		# ------------------------------------------------------------
		vf_elem = av_package_elem.find("./ebucore:format[@formatName='videoFormat']", Constants.ns)
		if vf_elem is None:
			rv.add_child(RV.error('Video Format element is None'))
			return rv, None
		else:
			# generate and append containerFormat
			image_format = VideoFormat()
			rv_sub, elem = image_format.collect_metadata(media_source_file, xml_metadata_file_in)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, None

			vf_elem.insert(len(vf_elem), elem)

		# ------------------------------------------------------------
		# audioFormat
		# ------------------------------------------------------------
		# Find format element for audioFormat for the first time. It is optional and might be there.
		format_elem = av_package_elem.find("./ebucore:format[@formatName='audioFormat']", Constants.ns)

		# check if source file contains audio
		from Content.AudioVisualPackage import AudioVisualPackage
		if not AudioVisualPackage.has_audio(media_source_file):
			if format_elem is not None:
				av_package_elem.remove(format_elem)
			rv.message = 'Successfully collected metadata for ' + self.name
			return rv, av_package_elem

		if format_elem is None:
			AbstractFormat.add_children(av_package_elem, self.optional_audio_format, force=True)
		format_elem = av_package_elem.find("./ebucore:format[@formatName='audioFormat']", Constants.ns)

		audio_format = AudioFormat()
		rv_sub, elem = audio_format.collect_metadata(media_source_file, xml_metadata_file_in)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None
		format_elem.insert(len(format_elem), elem)

		# ------------------------------------------------------------
		# set dc title with subpackage name
		# ------------------------------------------------------------
		sub_package_data_folder_abs = os.path.abspath(os.path.join(media_source_file, os.pardir))

		if sub_package_data_folder_abs.endswith('/data'):
			sub_package_folder_abs = os.path.abspath(os.path.join(sub_package_data_folder_abs, os.pardir))
			dc_title = os.path.basename(sub_package_folder_abs)
			dc_title_elem = av_package_elem.find("./ebucore:title/dc:title", Constants.ns)
			dc_title_elem.text = dc_title
		else:
			rv.add_child(RV.warning('File not in subpackage\'s data sub-folder, path is: ' + str(media_source_file)))

		return rv, av_package_elem
