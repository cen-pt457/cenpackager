from AbstractFormat import AbstractFormat
from RV import RV
from datetime import datetime, timezone


class EbuCoreMain(AbstractFormat):
	def __init__(self):
		AbstractFormat.__init__(self, name="ebuCoreMain", namespace='ebucore')

	def collect_metadata(self, media_source_file, xml_metadata_file_in):
		rv = RV.info('Collecting metadata for ' + self.name)
		rv_sub, cpp_metadata_elem = self.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv, None

		now = datetime.now(tz=timezone.utc)
		date_str = now.strftime("%Y-%m-%d")
		time_str = now.strftime("%H:%M:%SZ")
		cpp_metadata_elem.set('dateLastModified', date_str)
		cpp_metadata_elem.set('timeLastModified', time_str)
		return rv, cpp_metadata_elem
