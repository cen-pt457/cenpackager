import magic
import re
import os
from utils.log import log
import subprocess
from lxml import etree
from RV import RV


def type_detector(path):
	return magic.from_file(path, mime=True)


def group_sequence(lst):
	res = [[lst[0]]]

	for i in range(1, len(lst)):
		if lst[i-1][0]+1 == lst[i][0]:
			res[-1].append(lst[i])

		else:
			res.append([lst[i]])
	return res


def check_mediainfo_exists():
	rv = RV.info("Check if mediainfo is available on current system")
	current_environment = os.environ.copy()
	os_info = os.uname()
	if os_info.sysname == 'Darwin':
		current_environment['PATH'] = '/usr/local/bin:' + current_environment['PATH']
	try:
		process = subprocess.run('mediainfo --Help', stdout=subprocess.PIPE, shell=True, env=current_environment)
		if process.returncode == 0:
			return rv.add_child(RV.info('Successfully called mediainfo'))
		else:
			return rv.add_child(RV.error('Calling mediainfo failed'))
	except subprocess.SubprocessError as err:
		return rv.add_child(RV.error('Calling mediainfo raise a SubprocessError exception: ' + str(err)))


def image_sequence_continuous_checker(path_to_image_sequence):

	numbers = []
	files = os.listdir(path_to_image_sequence)
	files.sort()
	for f in files:
		basename = os.path.basename(f)

		search = re.findall(r'(.*?)(\d+)(\.)(.*)', basename)

		if len(search) != 1:
			continue

		if len(search[0]) < 2:
			continue

		try:
			image_number = int(search[0][1])
			numbers.append([image_number, f])
		except ValueError as _:
			continue

	seq = group_sequence(numbers)
	ret_val = []
	for sequence in seq:
		one_sequence = []
		for entry in sequence:
			one_sequence.append(entry[1])
		ret_val.append(one_sequence)
	return ret_val


def get_file_names_prefix(path_to_media):

	if os.path.isdir(path_to_media):
		# filter hidden files (starting with a .)
		files = [f for f in os.listdir(path_to_media) if not f.startswith('.')]
		files.sort()
	else:
		files = [os.path.basename(path_to_media)]

	num_digits, min, max = get_file_numbering_info(path_to_media)
	prefix = None
	for f in files:
		local_prefix, _ = os.path.splitext(f)
		if len(f) < num_digits + 1:
			log.warning('Filename is too short for removing digits: ' + str(local_prefix))
			continue
		local_prefix = local_prefix[:len(local_prefix) - num_digits ]
		if prefix is None:
			prefix = local_prefix

		else:
			if local_prefix != prefix:
				log.warning('file prefix not consistent in folder: ' + path_to_media)
	if prefix.endswith('_'):
		prefix = prefix[:-1]

	return prefix


def get_file_names_extension(path_to_media):
	if os.path.isdir(path_to_media):
		# filter hidden files (starting with a .)
		files = [f for f in os.listdir(path_to_media) if not f.startswith('.')]
		files.sort()
	else:
		files = [os.path.basename(path_to_media)]

	suffix = None
	for f in files:
		_, local_suffix = os.path.splitext(f)

		if suffix is None:
			suffix = local_suffix

		else:
			if local_suffix != suffix:
				log.warning('file suffix not consistent in folder: ' + path_to_media)

	if suffix.startswith('.'):
		suffix = suffix[1:]
	return suffix


def get_file_numbering_info(path_to_media):

	if os.path.isdir(path_to_media):
		# filter hidden files (starting with a .)
		files = [f for f in os.listdir(path_to_media) if not f.startswith('.')]
		files.sort()
	else:
		files = [path_to_media]

	numbers_as_string = []
	numbers_as_int = []
	for f in files:
		basename = os.path.basename(f)
		search = re.findall(r'(.*?)(\d+)(\.)(.*)', basename)

		if len(search) != 1:
			continue

		if len(search[0]) < 2:
			continue

		numbers_as_string.append(search[0][1])
		numbers_as_int.append(int(search[0][1]))

	num_digits = -1
	if len(numbers_as_string) > 0:
		num_digits = len(numbers_as_string[0])

	for number in numbers_as_string:
		if len(number) != num_digits:
			log.warning('Length of digits for numbering not identical for all files in: ' + path_to_media)

	if num_digits == -1:
		return 0, 0, 0
	return num_digits, min(numbers_as_int), max(numbers_as_int)


def get_folders(dir_path, folder_depth=999999):
	"""
	Provides a list of all folders in given path and deeper path structure.

	Parameters
	----------
	dir_path : str, mandatory
		path of the folder to be scanned for files, sub folders and files in sub folders
	folder_depth : int, optional
		search depth
	"""
	all_folders = []
	dirs = [x[0] for x in os.walk(dir_path)]
	for directory in dirs:
		dir_rel = directory.replace(dir_path, '')
		if dir_rel == '':
			continue
		if dir_rel.count(os.path.sep) <= folder_depth:
			if dir_rel.startswith('/'):
				dir_rel = dir_rel[1:]
			all_folders.append((dir_rel.count(os.path.sep), dir_rel))

	all_folders.sort(reverse=True)
	l = list(dict.fromkeys(all_folders))
	l.sort(reverse=False)

	return l


def get_folders_and_files(dir_path, folder_depth=999999):
	"""
	Provides a list of all files and folders in given path and deeper path structure.
	Example return value for folders
	[(0, ''),
	(1, 'subfolder1')] where 1st tupel value is the depth (0=same folder, 1=one folder underneath...) and 2nd tupel
	value is the path to the sub folder.
	Example return value for files
	[(0, '', 'filename1.txt),
	(1, 'subfolder1', , 'filename1.txt)] where 1st tupel value is the depth (0=same folder, 1=one folder
	underneath...), 2nd tupel value is the path to the sub folder and 3rd tupel value the file name.

	Parameters
	----------
	dir_path : str, mandatory
		path of the folder to be scanned for files, sub folders and files in sub folders
	folder_depth : int, optional
		search depth
	"""
	all_folders = []
	dirs = [x[0] for x in os.walk(dir_path)]
	for directory in dirs:
		dir_rel = directory.replace(dir_path, '')

		if dir_rel.count(os.path.sep) <= folder_depth:
			if dir_rel.startswith('/'):
				dir_rel = dir_rel[1:]
			all_folders.append((dir_rel.count(os.path.sep), dir_rel))

	# all files with path
	all_files = []

	for root, dirs, files in os.walk(dir_path):
		for file in files:
			if os.path.basename(file) in [".DS_Store"]:
				continue
			relative_path = os.path.relpath(root, dir_path)
			if relative_path == '.':
				relative_path = ''
			path_abs = os.path.join(dir_path, relative_path, file)

			num_seps = path_abs.count(os.path.sep) - dir_path.count(os.path.sep)
			if num_seps <= folder_depth:
				if relative_path == '':
					all_files.append((0, relative_path, file))
				else:
					if relative_path.count(os.path.sep) == 0:
						all_files.append((1, relative_path, file))
					else:
						all_files.append((relative_path.count(os.path.sep), relative_path, file))
	all_files.sort(reverse=True)
	l = list(dict.fromkeys(all_files))
	l.sort(reverse=False)

	return l


def num_descriptive_metadata_files(path_descriptive_md_abs):
	"""
	Counts and returns the number of descriptive metadata files. This are all files which filename starts with
	'descMD_'

	:param path_descriptive_md_abs: absolute path pointing to the CPP's metadata folder
	:return: Number of descriptive metadata files - can be zero
	"""
	if os.path.exists(path_descriptive_md_abs) is False:
		return 0

	cnt = 0
	for f in os.listdir(path_descriptive_md_abs):
		if f.startswith('descMD_') is True:
			cnt += 1
	return cnt


def num_provenance_metadata_files(path_provenance_md_abs):
	"""
	Counts and returns the number of provenance metadata files. This are all files which filename starts with
	'digiProvMD_'

	:param path_provenance_md_abs: absolute path pointing to the CPP's metadata folder
	:return: Number of provenance metadata files - can be zero
	"""
	if os.path.exists(path_provenance_md_abs) is False:
		return 0

	cnt = 0
	for f in os.listdir(path_provenance_md_abs):
		if f.startswith('digiProvMD_') is True:
			cnt += 1
	return cnt


def get_id_from_filename(file_name, keep_file_counter=False):
	"""
	Extracts a CPP-compatible UUID from a given filename

	:param file_name: file name (basename without path) containing a UUID we want to extract
	:param keep_file_counter: option to keep a file-numbering if exists
	:return: uuid if it can be extracted, otherwise an empty string ('')
	"""

	regex = r"[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}"
	if keep_file_counter:
		# 5, => five or more numbers
		regex = r"[a-z0-9]{8}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{4}-[a-z0-9]{12}_[0-9]{5,}"
	matches = re.findall(regex, file_name)

	if len(matches) == 0:
		log.warning('Unable to find an ID in string: ' + str(file_name))
		return ''
	elif len(matches) > 1:
		log.warning('More than one ID found - returning first id. String is: ' + str(file_name))
		return ''

	return matches[0]


def open_xml_and_remove_namespace(filename_path_abs, namespace):
	rv = RV.info('Opening xml file ' + filename_path_abs + ' to remove namespace: ' + namespace)
	try:
		it = etree.iterparse(filename_path_abs)
	except etree.XMLSyntaxError as err:
		return rv.add_child(RV.error('While trying to parse checkerReportsList.xml file: ' + str(err)))

	for _, el in it:
		prefix, has_namespace, postfix = el.tag.partition('}')
		if has_namespace:
			if namespace in prefix:
				el.tag = postfix  # strip all namespaces
	root = etree.ElementTree(it.root)
	return rv, root


def convert_iso8601_to_hh_mm_ss_s(iso8601_duration):
	rv = RV.info('Converting iso8601_to_HH_MM_SS_S: ' + iso8601_duration)

	if iso8601_duration.startswith('PT') is False:
		return rv.add_child(RV.error(iso8601_duration + " not starting with expected PT literal")), ''

	regex = r"^PT([0-9]+H)?([0-9]+M)?([0-9\.]+S)$"

	matches = re.findall(regex, iso8601_duration)
	if len(matches) != 1:
		return rv.add_child(RV.error("Unable to match expected values in " + iso8601_duration)), ''

	if len(matches[0]) != 3:
		return rv.add_child(RV.error("Unable to match expected values in " + iso8601_duration)), ''

	out_representation = ''
	if matches[0][0] != '':
		hours = matches[0][0]
		hours = hours[:-1]	# removing 'H'
		hours = int(hours)
		out_representation += '{:02d}'.format(hours)
		out_representation += ':'
	else:
		out_representation += '00:'

	if matches[0][1] != '':
		minutes = matches[0][1]
		minutes = minutes[:-1]	# removing 'M'
		minutes = int(minutes)
		out_representation += '{:02d}'.format(minutes)
		out_representation += ':'
	else:
		out_representation += '00:'

	if matches[0][2] != '':
		seconds = matches[0][2]
		seconds = seconds[:-1]	# removing 'S'
		seconds_regex = r"^([0-9]*)(\.[0-9]*)$"

		seconds_matches = re.findall(seconds_regex, seconds)
		if len(seconds_matches) != 1:
			return rv.add_child(RV.error("Unable to match expected values in " + iso8601_duration)), ''
		if len(seconds_matches[0]) != 2:
			return rv.add_child(RV.error("Unable to match expected values in " + iso8601_duration)), ''

		full_seconds = seconds_matches[0][0]

		out_representation += full_seconds.zfill(2)
		if seconds_matches[0][1] == '':
			out_representation += '.0'
		else:
			fraction_seconds = seconds_matches[0][1]
			# fraction_seconds = fraction_seconds[0:2]
			out_representation += fraction_seconds

	else:
		out_representation += '000'

	return rv.add_child(RV.info("Successfully converted to " + out_representation)), out_representation


def hh_mm_ss_s_to_frames(hh_mm_ss_s, framerate):
	rv = RV.info('Converting HH_MM_SS_S to frames: ' + hh_mm_ss_s)

	# 00:00:01.000
	regex = r"([0-9]{2}):([0-9]{2}):([0-9]{2}).([0-9]{1,3})"

	matches = re.findall(regex, hh_mm_ss_s)
	if len(matches) != 1:
		return rv.add_child(RV.error("Unable to match expected values in " + hh_mm_ss_s)), ''

	hours = int(matches[0][0])
	minutes = int(matches[0][1])
	seconds = int(matches[0][2])

	frames = hours * 3600 * framerate
	frames = frames + minutes * 60 * framerate
	frames = frames + seconds * framerate

	return rv.add_child(RV.info("Successfully converted to " + str(frames))), frames


def get_namespace(path):
	rv = RV.info('Reading the namespace(s) from: ' + path)

	if os.path.exists(path) is False:
		return rv.add_child(RV.error("Given path does not exist")), ''
	if os.path.isdir(path):
		return rv.add_child(RV.error("Given path is a directory, not to a file")), ''
	try:
		tree = etree.parse(path)
		try:
			ns = tree.xpath('namespace-uri(.)')
			return rv.add_child(RV.info("Successfully read namespace from file " + path)), ns
		except IndexError:
			return rv.add_child(RV.error("While trying to read the namespace")), ''
	except etree.XMLSyntaxError as err:
		return rv.add_child(RV.error("While trying to parse source file")), ''