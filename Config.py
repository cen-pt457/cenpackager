APP_NAME = 'CEN PT-457 Reference Software'
APP_VERSION = '0.01'
APP_HOME_URL = 'https://gitlab.com/cen-pt457'

include_arity_comments = False
include_optional_parameter = True
force_add_and_skip_schema_check = False
