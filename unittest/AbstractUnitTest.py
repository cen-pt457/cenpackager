import unittest
import os
from lxml import etree
from ebucore.Constants import Constants
import logging
log = logging.getLogger(__name__)


class AbstractUnitTest(unittest.TestCase):
    def check_subpackage_pkl(self, sub_package_pkl_path, num_expected_tech_md_files=1):
        sub_package_path = os.path.dirname(sub_package_pkl_path)
        # check existing
        self.assertEqual(True, os.path.exists(sub_package_pkl_path), msg="Sub-package PKL exists")

        root = etree.parse(sub_package_pkl_path)

        # check if expected number of techMD files exists
        if num_expected_tech_md_files > 0:
            element = root.xpath('/mets:mets/mets:amdSec/mets:techMd/mets:mdRef', namespaces=Constants.ns)
            self.assertEqual(num_expected_tech_md_files, len(element))

            for md_ref_element in element:
                tmp_md_path = md_ref_element.attrib['{' + Constants.ns['xlink'] + '}href']
                sub_package_tech_md_path = os.path.join(sub_package_path, tmp_md_path)
                self.assertEqual(True, os.path.exists(sub_package_tech_md_path))

        # get mets:file and mets:ftpr - make sure they are consistent
        mets_file_list = root.xpath('/mets:mets/mets:fileSec/mets:fileGrp/mets:file', namespaces=Constants.ns)
        mets_file_ftpr_list = root.xpath('/mets:mets/mets:structMap[@TYPE="physical"]/mets:div/mets:div/mets:ftpr',
                                         namespaces=Constants.ns)
        self.assertEqual(len(mets_file_list), len(mets_file_ftpr_list))

        for mets_file_ftpr in mets_file_ftpr_list:
            file_ftpr_id = mets_file_ftpr.attrib["ID"]
            id_match = False
            for mets_file in mets_file_list:
                file_id = mets_file.attrib["ID"]
                if file_ftpr_id == file_id:
                    id_match = True
                    # check file exists
                    mets_flocat_element = mets_file.xpath('./mets:FLocat', namespaces=Constants.ns)
                    self.assertEqual(1, len(mets_flocat_element))
                    data_file_path_rel = mets_flocat_element[0].attrib['{' + Constants.ns['xlink'] + '}href']
                    data_file_path_rel = os.path.join(sub_package_path, data_file_path_rel)
                    self.assertEqual(True, os.path.exists(data_file_path_rel))
                    break
            self.assertEqual(True, id_match)

    def sub_package_paths_by_type(self, sub_package_type):
        sub_package_paths = list()
        preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
        root = etree.parse(preservation_package_path_abs)

        sub_packages_elements = root.xpath(
            '/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
            '/mets:mptr', namespaces=Constants.ns)

        for sub_package_element in sub_packages_elements:
            tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
            if tmp_path.startswith(sub_package_type):
                sub_package_paths.append(os.path.join(self.package_path, tmp_path))

        return sub_package_paths

    def get_tech_md_paths_from_sub_package_pkl(self, path_subpackage_pkl):
        # check existing
        self.assertEqual(True, os.path.exists(path_subpackage_pkl), msg="Sub-package PKL exists")

        root = etree.parse(path_subpackage_pkl)

        # check if expected number of techMD files exists
        element = root.xpath('/mets:mets/mets:amdSec/mets:techMd/mets:mdRef', namespaces=Constants.ns)

        paths_tech_md = []
        for md_ref_element in element:
            tmp_md_path = md_ref_element.attrib['{' + Constants.ns['xlink'] + '}href']
            paths_tech_md.append(tmp_md_path)
        return paths_tech_md
