from AbstractUnitTest import AbstractUnitTest
import datetime
import time
import os
import shutil
import errno
from PreservationPackage import PreservationPackage
from lxml import etree
from utils.utils import num_descriptive_metadata_files
from ebucore.Constants import Constants
from AbstractFormat import AbstractFormat
import Config
import logging
log = logging.getLogger(__name__)


class UnitTestMetadata(AbstractUnitTest):
    package_path = os.path.join('.', 'meta_data_test_package_' + str(datetime.datetime.now()))

    def test_10_create_new(self):
        # create a new package
        PreservationPackage.create(self.package_path)

        log.info('Generating empty preservation package: ' + self.package_path)

        # check if main folder metadata folder are exsiting
        sub_folders = ["metadata"]
        full_paths = list()
        full_paths.append(self.package_path)
        for sub_folder in sub_folders:
            full_path = os.path.join(os.path.abspath(self.package_path), sub_folder)
            full_paths.append(full_path)

        for path in full_paths:
            self.assertEqual(os.path.exists(path), True)

        # check if Preservation Package PKL and descriptive metadata file exist
        self.assertEqual(os.path.exists(os.path.join(self.package_path, 'preservationPackingList.xml')), True)

        num_desc_md = num_descriptive_metadata_files(os.path.join(self.package_path, 'metadata'))
        self.assertEqual(num_desc_md, 1)

    def test_20_add_image_sub_package(self):

        source_folder = os.path.join('.', 'test_material', 'short_j2c_clip')
        self.assertEqual(True, os.path.exists(source_folder))
        self.assertEqual(True, os.path.exists(self.package_path))

        rv_add_item = PreservationPackage.add_item("imagePackage", self.package_path, source_folder, '')
        self.assertEqual(False, rv_add_item.is_error())

        # get sub-package folder from ppkl
        # check ppkl entries
        preservation_package_path_abs = os.path.join(self.package_path, 'preservationPackingList.xml')
        root = etree.parse(preservation_package_path_abs)

        sub_packages_elements = root.xpath(
            '/mets:mets/mets:structMap[@TYPE="physical"]/mets:div[@TYPE="CPP-Package"]/mets:div[@TYPE="subPackages"]'
            '/mets:mptr', namespaces=Constants.ns)

        for sub_package_element in sub_packages_elements:
            tmp_path = sub_package_element.attrib['{' + Constants.ns['xlink'] + '}href']
            if tmp_path.startswith('imagePackage_'):
                sub_package_pkl_path = os.path.join(self.package_path, tmp_path)
                self.check_subpackage_pkl(sub_package_pkl_path)

                sub_package_data_path = os.path.join(self.package_path, os.path.dirname(tmp_path), 'data')

                for i in range(0, 24):
                    image_file_name = 'image_' + str(i).zfill(8) + '.j2c'
                    image_file_path = os.path.join(sub_package_data_path, image_file_name)
                    self.assertEqual(True, os.path.exists(image_file_path))

    def test_30_check_image_package_tech_md_with_wrong_data(self):

        # get subpackage path from preservationPackingList
        image_packages_pkl_paths = self.sub_package_paths_by_type('imagePackage_')

        self.assertEqual(1, len(image_packages_pkl_paths))

        image_package_pkl_path = image_packages_pkl_paths[0]

        # get techMD file from subpackage's pkl
        paths_tech_md = self.get_tech_md_paths_from_sub_package_pkl(image_package_pkl_path)
        self.assertEqual(1, len(paths_tech_md))

        path_sub_package = os.path.dirname(image_package_pkl_path)
        path_tech_md = os.path.join(path_sub_package, paths_tech_md[0])
        self.assertEqual(True, os.path.exists(path_tech_md))

        rv_sub = AbstractFormat.xsd_schema_check(path_tech_md, '../xsd/ebucore.xsd')

        # expecting error since there is a lot wrong data in the md file
        self.assertEqual(True, rv_sub.is_error())
        print(rv_sub.dump())
        return

    def test_40_update_image_package_tech_md_with_valid_sample(self):
        # copy valid sample to sub package folder

        # get subpackage path from preservationPackingList
        image_packages_pkl_paths = self.sub_package_paths_by_type('imagePackage_')

        self.assertEqual(1, len(image_packages_pkl_paths))

        image_package_pkl_path = image_packages_pkl_paths[0]

        # get techMD file from subpackage's pkl
        paths_tech_md = self.get_tech_md_paths_from_sub_package_pkl(image_package_pkl_path)
        self.assertEqual(1, len(paths_tech_md))

        path_sub_package = os.path.dirname(image_package_pkl_path)
        path_tech_md = os.path.join(path_sub_package, paths_tech_md[0])

        try:
            shutil.copy(
                './test_material/metadata/image_package/techMD_01a4c11e-6fcd-4193-a773-952fa17ccb45_0000.xml',
                path_tech_md)
        except OSError as e:
            print('ERROR While trying to copy techMD in unit test. Error: %s' % e)
            self.assertEqual(1, 2)  # make sure to raise exception

        # ToDo: compare hashes from pkl and from new metadata file - MUST be different

        # update sub package so pkl will be rewritten (make sure sub package UUID will stay the same)
        Config.force_add_and_skip_schema_check = True
        path_sub_package_rel = os.path.basename(path_sub_package)
        PreservationPackage.update_item(self.package_path, path_sub_package_rel)

        # ToDo: compare hashes from pkl and from new metadata file - MUST be identical

        # run xsd check on new metadata file
        rv_sub = AbstractFormat.xsd_schema_check(path_tech_md, '../xsd/ebucore.xsd')

        # expecting no error
        self.assertEqual(True, rv_sub.is_info())
        print(rv_sub.dump())
        return

    def test_99_delete_test_package(self):
        self.assertEqual(os.path.exists(self.package_path), True)
        shutil.rmtree(self.package_path)
        self.assertEqual(os.path.exists(self.package_path), False)
