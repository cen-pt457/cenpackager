from RV import RV
from ebucore.Constants import Constants
from ebucore.ReferencedStandard import ReferencedStandards
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute


class AudioVisualPackageContainerFormatMatch:

	@staticmethod
	def container_standard_reference_match_name(in_param, out_param):
		rv = RV.info('Matching container standard name')
		# find container name from ebucore:containerFormat
		container_format_elem = in_param.find('./ebucore:containerFormat', Constants.ns)

		if container_format_elem is None:
			return rv.add_child(RV.error('Unable to find containerFormat element')), False

		container_format_name = container_format_elem.get("containerFormatName")
		if container_format_name is None:
			rv.add_child(RV.warning('No container format name found'))
			return rv, False

		if container_format_name  == 'MPEG-4':
			container_format_name = 'MP4_FF_2'

		for referenced_standard in ReferencedStandards.audiovisual_package_container:
			if container_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.short_designation_loc
				return rv.add_child(RV.info(
					'Successful matched metadata for containerStandardReference, value: ' +
					referenced_standard.short_designation_loc)), True
			elif container_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.short_designation_cpp
				return rv.add_child(RV.info(
					'Successful matched metadata for containerStandardReference, value: ' +
					referenced_standard.short_designation_cpp)), True
		rv.add_child(RV.warning('Provided codec not supported by CPP metadata: ' + container_format_name))
		return rv, False

	@staticmethod
	def container_standard_reference_match_uri(in_param, out_param):
		rv = RV.info('Matching container standard uri')
		# find container name from ebucore:containerFormat
		container_format_elem = in_param.find('./ebucore:containerFormat', Constants.ns)

		if container_format_elem is None:
			return rv.add_child(RV.warning(
				'Unable to find containerFormat element for container standard reference')), False

		container_format_name = container_format_elem.get("containerFormatName")
		if container_format_name is None:
			rv.add_child(RV.warning('No container format name found'))
			return rv, False
		if container_format_name == 'MPEG-4':
			container_format_name = 'MP4_FF_2'

		for referenced_standard in ReferencedStandards.audiovisual_package_container:
			if container_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.link
				return rv.add_child(RV.info(
					'Successful matched metadata for containerStandardReference, value: ' +
					referenced_standard.link)), True
			elif container_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.link
				return rv.add_child(RV.info(
					'Successful matched metadata for containerStandardReference, value: ' +
					referenced_standard.link)), True
		rv.add_child(RV.warning('Provided codec not supported by CPP metadata: ' + container_format_name))
		return rv, False

	src_root_xpath = './ebucore:coreMetadata'
	assignments = list()
	assignments.append(
		Assignment(
			name='Container Standard Reference Name',
			xpath_src=XPathAttribute(
				xpath='./ebucore:format'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:technicalAttributeString[@typeLabel="containerStandardReference"]'),
			regex=None,
			complex_map_fct=container_standard_reference_match_name.__func__)
	)
	assignments.append(
		Assignment(
			name='Container Standard Reference Uri',
			xpath_src=XPathAttribute(
				xpath='./ebucore:format'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:technicalAttributeUri[@typeLabel="containerStandardReference"]'),
			regex=None,
			complex_map_fct=container_standard_reference_match_uri.__func__)
	)
	assignments.append(
		Assignment(
			name='width',
			xpath_src=XPathAttribute(xpath='./ebucore:format/ebucore:videoFormat/ebucore:width'),
			xpath_dest=
			XPathAttribute('./ebucore:technicalAttributeUnsignedInteger[@typeLabel="containerWidth"]'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='height',
			xpath_src=XPathAttribute(xpath='./ebucore:format/ebucore:videoFormat/ebucore:height'),
			xpath_dest=
			XPathAttribute('./ebucore:technicalAttributeUnsignedInteger[@typeLabel="containerHeight"]'),
			regex=None)
	)

