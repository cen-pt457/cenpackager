from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute
from utils.log import log
from ebucore.Constants import Constants
import os
from utils.utils import get_file_names_prefix, get_file_numbering_info, get_file_names_extension
from RV import RV


class SoundPackageFormatMatch:
	@staticmethod
	def filename_prefix_match(in_param, out_param):
		rv = RV.info('Matching filename prefix for sound package format')

		path_first_image = in_param.text

		if os.path.exists(path_first_image) is False:
			return rv.add_child(RV.warning(
				'Given path to first file of sequence does not exists: ' + str(path_first_image))), False

		prefix = get_file_names_prefix(path_first_image)
		out_param.text = prefix
		return rv.add_child(RV.info('Successfully matched sound package format')), True

	@staticmethod
	def file_numbering_match(in_param, out_param):
		rv = RV.info('Matching file numbering for sound package format')

		locator_elem = in_param.find('./ebucore:locator', Constants.ns)
		if locator_elem is None:
			return rv.add_child(RV.warning('Unable to find ebucore:locator element for file numbering match')), False

		path_first_audio = locator_elem.text
		if os.path.exists(path_first_audio) is False:
			return rv.add_child(
				RV.warning('Given path to first file of sequence does not exists: ' + str(path_first_audio))), False

		num_digits, min_no, max_no = get_file_numbering_info(os.path.dirname(path_first_audio))

		# num_digits
		filename_digits_elem = out_param.find(
			'./ebucore:technicalAttributeUnsignedByte[@typeLabel="filenameDigits"]', Constants.ns)
		if filename_digits_elem is None:
			return rv.add_child(RV.warning('Unable to find filename_digits_elem')), False

		filename_digits_elem.text = str(num_digits)
		return rv.add_child(RV.info('Successfully matched file numbering for sound package format')), True

	@staticmethod
	def filename_extension(in_param, out_param):
		rv = RV.info('Matching file name extension for sound package format')
		path_first_audio = in_param.text

		if os.path.exists(path_first_audio) is False:
			return rv.add_child(RV.warning(
				'Given path to first file of sequence does not exists: ' + str(path_first_audio))), False
		extension = get_file_names_extension(path_first_audio)
		out_param.text = extension
		return rv.add_child(RV.info('Successfully matched file name extension for sound package format')), True

	@staticmethod
	def playtime_match(in_param, out_param):
		rv = RV.info('Matching playtime for sound package format')
		out_param.text = in_param.text
		return rv.add_child(RV.info('Successfully matched playtime for sound package format')), True

	src_root_xpath = './ebucore:coreMetadata/ebucore:format'
	assignments = list()
	assignments.append(
		Assignment(
			name='Container Standard Reference Name',
			xpath_src=XPathAttribute(xpath='./ebucore:locator'),
			xpath_dest=XPathAttribute(
				'./ebucore:technicalAttributeString[@typeLabel="filenamePrefix"]'),
			regex=None,
			complex_map_fct=filename_prefix_match.__func__
		)
	)
	assignments.append(
		Assignment(
			name='filename digits, first index, last index',
			xpath_src=XPathAttribute(xpath='.'),
			xpath_dest=XPathAttribute(
				'.'),
			regex=None,
			complex_map_fct=file_numbering_match.__func__
		)
	)
	assignments.append(
		Assignment(
			name='filename extension',
			xpath_src=XPathAttribute(xpath='./ebucore:locator'),
			xpath_dest=XPathAttribute(
				'./ebucore:technicalAttributeString[@typeLabel="filenameExtension"]'),
			regex=None,
			complex_map_fct=filename_extension.__func__
		)
	)
	assignments.append(
		Assignment(
			name='normalPlayTime',
			xpath_src=XPathAttribute(xpath='./ebucore:duration/ebucore:normalPlayTime'),
			xpath_dest=XPathAttribute(
				'./ebucore:duration/ebucore:normalPlayTime'),
			regex=None,
			complex_map_fct=playtime_match.__func__
		)
	)
