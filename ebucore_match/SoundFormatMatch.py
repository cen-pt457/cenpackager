from RV import RV
from ebucore.Constants import Constants
from ebucore.ReferencedStandard import ReferencedStandards
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute


class SoundFormat:
	@staticmethod
	def audio_codec_standard_reference_match_name(in_param, out_param):
		rv = RV.info('Matching audio coding standard reference')
		audio_format_elem = in_param.find('./ebucore:audioFormat', Constants.ns)
		if audio_format_elem is None:
			return rv.add_child(RV.error('Unable to find audioFormat tag in xml file')), False

		audio_format_name = audio_format_elem.get("audioFormatName")
		if audio_format_name is None:
			return rv.add_child(RV.error('Unable to read audioFormatName in xml file')), False

		if audio_format_name == 'PCM':
			audio_format_name = 'WAVE'

		for referenced_standard in ReferencedStandards.sound_package:
			if audio_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.short_designation_loc
				return rv.add_child(
					RV.info('Successful matched metadata: ' + referenced_standard.short_designation_loc)), True
			elif audio_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.short_designation_cpp
				return rv.add_child(
					RV.info('Successful matched metadata: ' + referenced_standard.short_designation_cpp)), True
		return rv.add_child(RV.warning('No match for audio coding standard reference')), False

	@staticmethod
	def audio_codec_standard_reference_match_uri(in_param, out_param):
		rv = RV.info('Matching audio coding standard reference uri')
		audio_format_elem = in_param.find('./ebucore:audioFormat', Constants.ns)
		if audio_format_elem is None:
			return rv.add_child(RV.error('Unable to find ebucore:audioFormat tag in xml file')), False

		audio_format_name = audio_format_elem.get("audioFormatName")
		if audio_format_name == 'PCM':
			audio_format_name = 'WAVE'

		for referenced_standard in ReferencedStandards.sound_package:
			if audio_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.link
				return rv.add_child(
					RV.info('Successful matched metadata: ' + referenced_standard.link)), True
			elif audio_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.link
				return rv.add_child(
					RV.info('Successful matched metadata: ' + referenced_standard.link)), True

		return rv.add_child(RV.warning('No match for audio coding standard reference uri')), False

	src_root_xpath = './ebucore:coreMetadata'
	assignments = list()

	assignments.append(
		Assignment(
			name='Audio Codec Standard Reference Name',
			xpath_src=XPathAttribute(
				xpath='./'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:technicalAttributeString[@typeLabel="audioCodecStandardReference"]'),
			regex=None,
			complex_map_fct=audio_codec_standard_reference_match_name.__func__)
	)
	assignments.append(
		Assignment(
			name='Audio Codec Standard Reference Uri',
			xpath_src=XPathAttribute(
				xpath='./'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:technicalAttributeUri[@typeLabel="audioCodecStandardReference"]'),
			regex=None,
			complex_map_fct=audio_codec_standard_reference_match_uri.__func__)
	)

	assignments.append(
		Assignment(
			name='sampling rate',
			xpath_src=XPathAttribute(xpath='./ebucore:format/ebucore:audioFormat/ebucore:samplingRate'),
			xpath_dest=XPathAttribute('./ebucore:samplingRate'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='channels',
			xpath_src=XPathAttribute(xpath='./ebucore:format/ebucore:audioFormat/ebucore:channels'),
			xpath_dest=XPathAttribute('./ebucore:channels'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='channel layout',
			xpath_src=XPathAttribute(
				xpath='./ebucore:format/ebucore:audioFormat/ebucore:technicalAttributeString[@typeLabel="ChannelPositions"]'),
			xpath_dest=XPathAttribute('./ebucore:technicalAttributeString[@typeLabel="channelLayout"]'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='sample size',
			xpath_src=XPathAttribute(
				xpath='./ebucore:format/ebucore:audioFormat/ebucore:sampleSize'),
			xpath_dest=XPathAttribute('./ebucore:sampleSize'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='sample type',
			xpath_src=XPathAttribute(
				xpath='./ebucore:format/ebucore:audioFormat/ebucore:sampleSize'),
			xpath_dest=XPathAttribute('./ebucore:sampleSize'),
			regex=None)
	)