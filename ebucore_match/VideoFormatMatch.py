from RV import RV
from ebucore.ColorSpaceDefinition import ColorSpaceDefinition
from ebucore.Constants import Constants
from ebucore.Iso211223 import Iso211223
from ebucore.ReferencedStandard import ReferencedStandards
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute


class VideoFormatMatch:

	@staticmethod
	def video_codec_standard_reference_match_name(in_param, out_param):
		rv = RV.info('Matching video codec standard reference name')

		video_format_name = in_param.get("videoFormatName")
		if video_format_name is None:
			rv.add_child(RV.warning('No container format name found'))
			return rv, False
		# if video_format_name == 'JPEG 2000':
		# 		video_format_name = 'J2K_C'

		for referenced_standard in ReferencedStandards.audiovisual_package:
			if video_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.short_designation_loc
				return rv.add_child(RV.info('Successful matched: ' + referenced_standard.short_designation_loc)), True
			elif video_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.short_designation_cpp
				return rv.add_child(RV.info('Successful matched: ' + referenced_standard.short_designation_loc)), True
		return rv.add_child(RV.warning('Provided codec not supported by CPP metadata: ' + video_format_name)), None

	@staticmethod
	def video_codec_standard_reference_match_uri(in_param, out_param):
		rv = RV.info('Matching video codec standard reference uri')
		video_format_name = in_param.get("videoFormatName")
		if video_format_name is None:
			rv.add_child(RV.warning('No container format name found'))
			return rv, False
		if video_format_name == 'JPEG 2000':
			video_format_name = 'J2K_C'
		for referenced_standard in ReferencedStandards.audiovisual_package:
			if video_format_name in referenced_standard.short_designation_loc:
				out_param.text = referenced_standard.link
				return rv.add_child(RV.info('Successful matched: ' + referenced_standard.link)), True
			elif video_format_name in referenced_standard.short_designation_cpp:
				out_param.text = referenced_standard.link
				return rv.add_child(RV.info('Successful matched: ' + referenced_standard.link)), True

		return rv.add_child(RV.warning('Provided codec not supported by CPP metadata: ' + video_format_name)), None

	@staticmethod
	def get_human_readable_colour_space_desc(chroma_subsampling_elem, color_space_elem):
		chroma_subsampling = chroma_subsampling_elem.text
		color_space = color_space_elem.text
		if color_space == 'YUV':
			color_space = 'YCbCr'
		return chroma_subsampling + ' (' + color_space + ')'

	@staticmethod
	def number_of_colour_components_match(in_param, out_param):
		rv = RV.info('Matching number of colour components')
		chroma_subsampling_elem = \
			in_param.find('./ebucore:technicalAttributeString[@typeLabel="ChromaSubsampling"]', Constants.ns)

		if chroma_subsampling_elem is None:
			return rv.add_child(RV.warning('Unable to read chroma subsampling from source video/image.')), False

		color_space_elem = \
			in_param.find('./ebucore:technicalAttributeString[@typeLabel="ColorSpace"]', Constants.ns)

		if color_space_elem is None:
			return rv.add_child(RV.warning('Unable to read color space from source video/image.')), False

		search_pattern = \
			VideoFormatMatch.get_human_readable_colour_space_desc(chroma_subsampling_elem, color_space_elem)

		for sampling_structure in Iso211223.sampling_structures:
			if search_pattern == sampling_structure.meaning:
				out_param.text = str(sampling_structure.num_components)
				return rv.add_child(
					RV.warning('Successful matched metadata: ' + str(sampling_structure.num_components))), True

		return rv.add_child(RV.warning('Provided codec not supported by CPP metadata: ' + search_pattern)), None

	# As stated in 10.3.1.3.6
	# Index of colour primaries according to ITU-T H.273 shall be the index of the table 2 of the
	# ITU-T H.273:2016 standard.
	@staticmethod
	def colour_primaries_match(in_param, out_param):
		rv = RV.info('Matching colour primaries')
		matching_color_spaces = list()
		for colour_space in ColorSpaceDefinition.spaces:
			if in_param.text in colour_space.definition:
				matching_color_spaces.append(colour_space)

		if len(matching_color_spaces) == 0:
			return rv.add_child(RV.warning('No matching colour space found for ' + in_param.text)), False

		if len(matching_color_spaces) > 1:
			msg = 'More than one color space match for ' + in_param.text + ' Candidates are:\n'
			for colour_space in matching_color_spaces:
				msg += colour_space.defintion + '\n'
			return rv.add_child(RV.warning(msg)), False

		if ' or ' in matching_color_spaces[0].colour_primaries:
			rv.add_child(
				RV.warning('Colour primaries for ' + in_param.text + ' can be either ' +
						matching_color_spaces[0].colour_primaries + ' - Adjust manually'))
		else:
			rv.add_child(RV.info('Successful matched metadata for colour primaries, value: ' +
					 matching_color_spaces[0].colour_primaries))

		out_param.text = matching_color_spaces[0].colour_primaries
		return rv, True

	@staticmethod
	def transfer_characteristics_match(in_param, out_param):
		rv = RV.info('Matching transfer characteristics')
		matching_color_spaces = list()
		for colour_space in ColorSpaceDefinition.spaces:
			if in_param.text in colour_space.definition:
				matching_color_spaces.append(colour_space)

		if len(matching_color_spaces) == 0:
			return rv.add_child(RV.warning('No matching colour space found for ' + in_param.text)), False

		if len(matching_color_spaces) > 1:
			msg = 'More than one color space match for ' + in_param.text + ' Candidates are:\n'
			for colour_space in matching_color_spaces:
				msg += colour_space.defintion + '\n'
			return rv.add_child(RV.warning(msg)), False
		if ' or ' in matching_color_spaces[0].transfer_characteristics:
			rv.add_child(
				RV.warning('Colour primaries for ' + in_param.text + ' can be either ' +
						   matching_color_spaces[0].colour_primaries + ' - Adjust manually'))
		else:
			rv.add_child(RV.info('Successful matched metadata for colour primaries, value: ' +
								 matching_color_spaces[0].colour_primaries))
		out_param.text = matching_color_spaces[0].transfer_characteristics
		return rv, True

	@staticmethod
	def matrix_coefficients_match(in_param, out_param):
		rv = RV.info('Matching transfer coefficients')
		matching_color_spaces = list()
		for colour_space in ColorSpaceDefinition.spaces:
			if in_param.text in colour_space.definition:
				matching_color_spaces.append(colour_space)

		if len(matching_color_spaces) == 0:
			return rv.add_child(RV.warning('No matching colour space found for ' + in_param.text)), False

		if len(matching_color_spaces) > 1:
			msg = 'More than one color space match for ' + in_param.text + ' Candidates are:\n'
			for colour_space in matching_color_spaces:
				msg += colour_space.defintion + '\n'
			return rv.add_child(RV.warning(msg)), False

		if ' or ' in matching_color_spaces[0].matrix_coefficients:
			rv.add_child(
				RV.warning('Colour primaries for ' + in_param.text + ' can be either ' +
						   matching_color_spaces[0].colour_primaries + ' - Adjust manually'))
		else:
			rv.add_child(RV.info('Successful matched metadata for colour primaries, value: ' +
								 matching_color_spaces[0].colour_primaries))
		out_param.text = matching_color_spaces[0].matrix_coefficients
		return rv, True

	@staticmethod
	def chroma_subsampling_match(in_param, out_param):
		rv = RV.info('Matching chroma subsampling')
		chroma_subsampling_elem = \
			in_param.find('./ebucore:technicalAttributeString[@typeLabel="ChromaSubsampling"]', Constants.ns)

		if chroma_subsampling_elem is None:
			return rv.add_child(RV.warning('Unable to read chroma subsampling from source video/image.')), False

		color_space_elem = \
			in_param.find('./ebucore:technicalAttributeString[@typeLabel="ColorSpace"]', Constants.ns)

		if color_space_elem is None:
			return rv.add_child(RV.warning('Unable to read color space from source video/image.')), False

		search_pattern = \
			VideoFormatMatch.get_human_readable_colour_space_desc(chroma_subsampling_elem, color_space_elem)

		for sampling_structure in Iso211223.sampling_structures:
			if search_pattern == sampling_structure.meaning:
				out_param.text = str(sampling_structure.field_value)
				return rv.add_child(RV.info('Successful matched chroma subsampling: ' +
									sampling_structure.meaning)), True

		return rv.add_child(RV.warning('Provided codec not supported by CPP metadata: ' + search_pattern)), False

	@staticmethod
	def progressive_mode_match(in_param, out_param):
		rv = RV.info('Matching progressive mode')
		# according to C.7.2.3
		if in_param.text == 'progressive':
			out_param.text = str(0)
			return rv.add_child(RV.info('Successfully matched: progressive')), True
		# ToDo: Add more progressive modes according to C.7.2.3
		else:
			return rv.add_child(RV.info('Successfully matched: interlaced')), True

	@staticmethod
	def video_codec_frame_rate(in_param, out_param):
		rv = RV.info('Matching frame rate')

		# check for frame rate attribute
		frame_rate_elem_in = in_param.find('./ebucore:frameRate', Constants.ns)

		if frame_rate_elem_in is None:
			return rv.add_child(RV.warning('No frameRate element found in source metadata')), True

		frame_rate_elem_out = out_param.find('./ebucore:frameRate', Constants.ns)
		if frame_rate_elem_out is None:
			return rv.add_child(RV.warning('No frameRate element found in destination metadata stub')), True

		try:
			frame_rate_elem_out.attrib['factorNumerator'] = frame_rate_elem_in.attrib['factorNumerator']
			frame_rate_elem_out.attrib['factorDenominator'] = frame_rate_elem_in.attrib['factorDenominator']
		except KeyError as _:
			# if there are no attributes factorNumerator or factorDenominator in the source ebu code metadata
			# the current videos framerate is not fractional. Thus, we can simply remove (pop) the attributes
			# for the subpackage as well.
			# None as 2nd parameter defines, that no expection is raised in case the attrib does not exist
			frame_rate_elem_out.attrib.pop("factorNumerator", None)
			frame_rate_elem_out.attrib.pop("factorDenominator", None)

		frame_rate_elem_out.text = frame_rate_elem_in.text

		return rv.add_child(RV.info('Successful matched video frame rate: ')), True

	src_root_xpath = './ebucore:coreMetadata/ebucore:format'
	assignments = list()
	assignments.append(
		Assignment(
			name='Video Codec Standard Reference Name',
			xpath_src=XPathAttribute(
				xpath='./'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:technicalAttributeString[@typeLabel="videoCodecStandardReference"]'),
			regex=None,
			complex_map_fct=video_codec_standard_reference_match_name.__func__)
	)
	assignments.append(
		Assignment(
			name='Video Codec Standard Reference Uri',
			xpath_src=XPathAttribute(
				xpath='./'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:technicalAttributeUri[@typeLabel="videoCodecStandardReference"]'),
			regex=None,
			complex_map_fct=video_codec_standard_reference_match_uri.__func__)
	)
	'''
	# Those matches are covered in a function called video_codec_frame_rate now
	assignments.append(
		Assignment(
			name='frame rate numerator',
			xpath_src=
			XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:frameRate',
				attribute='factorNumerator'
			),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:frameRate',
				attribute='factorNumerator'
			),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='frame rate denominator',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:frameRate',
				attribute='factorDenominator'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:frameRate',
				attribute='factorDenominator'),
			regex=None)
	)
	'''
	assignments.append(
		Assignment(
			name='Number of colour components',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat'),
			xpath_dest=XPathAttribute(
				xpath='./ebucore:technicalAttributeUnsignedInteger[@typeLabel="componentNumber"]'),
			regex=None,
			complex_map_fct=number_of_colour_components_match.__func__
		)
	)
	assignments.append(
		Assignment(
			name='component bit depth',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:technicalAttributeInteger[@typeLabel="BitDepth"]'),
			xpath_dest=XPathAttribute(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="componentBitdepth"]'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='width',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:width'),
			xpath_dest=XPathAttribute('./ebucore:width'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='height',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:height'),
			xpath_dest=XPathAttribute('./ebucore:height'),
			regex=None)
	)

	assignments.append(
		Assignment(
			name='aspectRatio - factorNumerator',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:aspectRatio/ebucore:factorNumerator'),
			xpath_dest=XPathAttribute('.ebucore:aspectRatio/ebucore:factorNumerator'),
			regex=None)
	)
	assignments.append(
		Assignment(
			name='aspectRatio - factorDenominator',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:aspectRatio/ebucore:factorDenominator'),
			xpath_dest=XPathAttribute('./ebucore:aspectRatio/ebucore:factorDenominator'),
			regex=None)
	)

	assignments.append(
		Assignment(
			name='colourPrimaries',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:technicalAttributeString[@typeLabel="colour_primaries"]'),
			xpath_dest=
			XPathAttribute(xpath='./ebucore:technicalAttributeUnsignedInteger[@typeLabel="colourPrimaries"]'),
			regex=None,
			complex_map_fct=colour_primaries_match.__func__)
	)
	assignments.append(
		Assignment(
			name='transferCharacteristics',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:technicalAttributeString[@typeLabel="transfer_characteristics"]'),
			xpath_dest=
			XPathAttribute(
				xpath='./ebucore:technicalAttributeUnsignedInteger[@typeLabel="transferCharacteristics"]'),
			regex=None,
			complex_map_fct=transfer_characteristics_match.__func__)
	)
	assignments.append(
		Assignment(
			name='matrixCoefficients',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat/ebucore:technicalAttributeString[@typeLabel="matrix_coefficients"]'),
			xpath_dest=
			XPathAttribute(
				xpath='./ebucore:technicalAttributeUnsignedInteger[@typeLabel="matrixCoefficients"]'),
			regex=None,
			complex_map_fct=matrix_coefficients_match.__func__)
	)
	assignments.append(
		Assignment(
			name='progressiveMode',
			xpath_src=XPathAttribute(xpath='./ebucore:videoFormat/ebucore:scanningFormat'),
			xpath_dest=
			XPathAttribute(
				xpath='./ebucore:technicalAttributeUnsignedByte[@typeLabel="progressiveMode"]'),
			regex=None,
			complex_map_fct=progressive_mode_match.__func__)
	)
	assignments.append(
		Assignment(
			name='chroma subsampling',
			xpath_src=XPathAttribute(
				xpath='./ebucore:videoFormat'),
			xpath_dest=
			XPathAttribute(
				xpath='./ebucore:technicalAttributeUnsignedByte[@typeLabel="chromaSubsampling"]'),
			regex=None,
			complex_map_fct=chroma_subsampling_match.__func__)
	)
	assignments.append(
		Assignment(
			name='Frame rate',
			xpath_src=XPathAttribute(
				xpath='.ebucore:videoFormat'),
			xpath_dest=XPathAttribute(
				xpath='.'),
			regex=None,
			complex_map_fct=video_codec_frame_rate.__func__)
	)



class VideoFormatMatchDcp:
	video_codec_standard_reference = 'ST 429-4:2020 - SMPTE Standard - D-Cinema Packaging — MXF JPEG 2000 Application'
	colour_reference = 'ISO/IEC TR 23091-4:2021'
	chroma_subsampling = '9'  # as written in TR 1.4.9
	component_number = '3'
	channel_number = '3'
	colour_primaries = '11'
	transfer_characteristics = '17'
	matrix_coefficients = '0'
	full_range_flag = '0'
	luminance_min = '100'
	luminance_max = '420000'
	value_range_min = '100'
	value_range_max = '300000'
	colour_reference_uri = 'https://www.iso.org/standards/81585.html'
	video_codec_standard_reference_uri = 'https://doi.org/10.5594/SMPTE.ST429-4.2020'

	@staticmethod
	def video_codec_standard_reference_match_name(in_param, out_param):
		rv = RV.info('Matching video codec standard reference name')

		video_format_name = in_param.get("videoFormatName")
		if video_format_name is None:
			rv.add_child(RV.warning('No video format name found'))
			return rv, False

		video_codec_std_ref_elem = \
			out_param.find(
				'./ebucore:technicalAttributeString[@typeLabel="videoCodecStandardReference"]',
				Constants.ns)

		if video_codec_std_ref_elem is None:
			rv.add_child(RV.warning('No video codec standard reference elem found'))
			return rv, False

		video_codec_std_uri_ref_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUri[@typeLabel="videoCodecStandardReference"]',
				Constants.ns)

		if video_codec_std_uri_ref_elem is None:
			rv.add_child(RV.warning('No video codec standard reference URI elem found'))
			# no return here since it is an optional value

		if video_format_name == 'JPEG 2000':
			video_codec_std_ref_elem.text = VideoFormatMatchDcp.video_codec_standard_reference
			video_codec_std_uri_ref_elem.text = VideoFormatMatchDcp.video_codec_standard_reference_uri
		return rv, True

	@staticmethod
	def colour_parameters_match_name(in_param, out_param):
		rv = RV.info('Matching colour reference name')

		color_space_elem = in_param.find('./ebucore:technicalAttributeString[@typeLabel="ColorSpace"]', Constants.ns)
		if color_space_elem is None:
			rv.add_child(RV.warning('No colour space element found'))
			return rv, False

		chroma_subsampling_elem = \
			in_param.find('./ebucore:technicalAttributeString[@typeLabel="ChromaSubsampling"]', Constants.ns)
		if chroma_subsampling_elem is None:
			rv.add_child(RV.warning('No chroma subsampling element found'))
			# no return here since it is an optional value

		colour_reference_elem = \
			out_param.find(
				'./ebucore:technicalAttributeString[@typeLabel="colourReference"]',
				Constants.ns)

		if colour_reference_elem is None:
			rv.add_child(RV.warning('No colour reference elem found'))
			return rv, False

		chroma_subsampling_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedByte[@typeLabel="chromaSubsampling"]',
				Constants.ns)

		if chroma_subsampling_out_elem is None:
			rv.add_child(RV.warning('No chroma subsampling element elem found'))
			return rv, False

		component_number_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="componentNumber"]',
				Constants.ns)

		if component_number_out_elem is None:
			rv.add_child(RV.warning('No component number element elem found'))
			return rv, False

		channel_number_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="channelNumber"]',
				Constants.ns)

		if channel_number_out_elem is None:
			rv.add_child(RV.warning('No channel number element elem found'))
			return rv, False

		colour_primaries_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="colourPrimaries"]',
				Constants.ns)

		if colour_primaries_out_elem is None:
			rv.add_child(RV.warning('No channel number element elem found'))
			return rv, False

		transfer_characteristics_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="transferCharacteristics"]',
				Constants.ns)

		if transfer_characteristics_out_elem is None:
			rv.add_child(RV.warning('No channel number element elem found'))
			return rv, False

		# matrix coefficients
		matrix_coefficients_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="matrixCoefficients"]',
				Constants.ns)

		if matrix_coefficients_out_elem is None:
			rv.add_child(RV.warning('No matrix coefficients element elem found'))
			return rv, False

		# full range flag
		full_range_flag_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="fullRangeFlag"]',
				Constants.ns)

		if full_range_flag_out_elem is None:
			rv.add_child(RV.warning('No full range flag element elem found'))
			return rv, False

		luminance_min_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="luminanceMin"]',
				Constants.ns)

		if luminance_min_out_elem is None:
			rv.add_child(RV.warning('No luminance min element elem found'))
			# no return here since it is an optional value

		luminance_max_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="luminanceMax"]',
				Constants.ns)

		if luminance_max_out_elem is None:
			rv.add_child(RV.warning('No luminance max element elem found'))
			# no return here since it is an optional value

		value_range_min_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="valueRangeMin"]',
				Constants.ns)

		if value_range_min_out_elem is None:
			rv.add_child(RV.warning('No value range min element elem found'))
			# no return here since it is an optional value

		value_range_max_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="valueRangeMax"]',
				Constants.ns)

		if value_range_max_out_elem is None:
			rv.add_child(RV.warning('No value range max element elem found'))
			# no return here since it is an optional value

		colour_reference_uri_out_elem = \
			out_param.find(
				'./ebucore:technicalAttributeUri[@typeLabel="colourReference"]',
				Constants.ns)

		if colour_reference_uri_out_elem is None:
			rv.add_child(RV.warning('No colour reference uri element elem found'))
			# no return here since it is an optional value

		if color_space_elem.text != 'XYZ':
			rv.add_child(RV.error(
				'Unsupported colour information. Colour: ' + str(color_space_elem.text)))
			return rv, False

		if chroma_subsampling_elem is not None:
			if chroma_subsampling_elem.text != "4:4:4":
				rv.add_child(RV.error(
					'Unsupported chroma subsampling information: ' + str(chroma_subsampling_elem.text)))
				return rv, False

		colour_reference_elem.text = VideoFormatMatchDcp.colour_reference
		chroma_subsampling_out_elem.text = VideoFormatMatchDcp.chroma_subsampling  # as written in TR 1.4.9
		component_number_out_elem.text = VideoFormatMatchDcp.component_number
		channel_number_out_elem.text = VideoFormatMatchDcp.channel_number
		colour_primaries_out_elem.text = VideoFormatMatchDcp.colour_primaries
		transfer_characteristics_out_elem.text = VideoFormatMatchDcp.transfer_characteristics
		matrix_coefficients_out_elem.text = VideoFormatMatchDcp.matrix_coefficients
		full_range_flag_out_elem.text = VideoFormatMatchDcp.full_range_flag
		luminance_min_out_elem.text = VideoFormatMatchDcp.luminance_min
		luminance_max_out_elem.text = VideoFormatMatchDcp.luminance_max
		value_range_min_out_elem.text = VideoFormatMatchDcp.value_range_min
		value_range_max_out_elem.text = VideoFormatMatchDcp.value_range_max
		colour_reference_uri_out_elem.text = VideoFormatMatchDcp.colour_reference_uri

		return rv, True

	src_root_xpath = './ebucore:coreMetadata/ebucore:format'
	assignments = list()

	assignments.append(
		Assignment(
			name='Video Codec Standard Reference Name',
			xpath_src=XPathAttribute(
				xpath='.ebucore:videoFormat'),
			xpath_dest=XPathAttribute(
				xpath='.'),
			regex=None,
			complex_map_fct=colour_parameters_match_name.__func__)
	)

	assignments.append(
		Assignment(
			name='Colour Reference',
			xpath_src=XPathAttribute(
				xpath='.ebucore:videoFormat'),
			xpath_dest=XPathAttribute(
				xpath='.'),
			regex=None,
			complex_map_fct=video_codec_standard_reference_match_name.__func__)
	)
