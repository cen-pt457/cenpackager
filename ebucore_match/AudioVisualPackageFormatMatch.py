import os
import re
from lxml import etree
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute
from utils.utils import convert_iso8601_to_hh_mm_ss_s
from ebucore.Constants import Constants
from RV import RV


class AudioVisualPackageFormatMatch:

    @staticmethod
    def normal_play_time(in_param, out_param):
        rv = RV.info('Normal play time')

        play_time_in = in_param.text
        if play_time_in is None:
            return rv.add_child(RV.warning('Unable to read source normal play time')), False

        converted_playtime = play_time_in
        if play_time_in.startswith("PT"):
            rv_sub, converted_playtime = convert_iso8601_to_hh_mm_ss_s(play_time_in)

            if not rv_sub.is_info():
                rv.add_child(rv_sub)
                if rv_sub.is_error():
                    return rv, False

        out_param.text = converted_playtime
        return rv.add_child(RV.info('Successfully mapped normal play time')), True


    src_root_xpath = './ebucore:coreMetadata/ebucore:format'
    assignments = list()
    '''
    assignments.append(
        Assignment(
            name='normal play time',
            xpath_src=XPathAttribute(xpath='./ebucore:duration/ebucore:normalPlayTime'),
            xpath_dest=XPathAttribute('./ebucore:duration/ebucore:normalPlayTime'),
            regex=None)
    )
    '''
    assignments.append(
        Assignment(
            name='duration timecode',
            xpath_src=XPathAttribute(xpath='./ebucore:duration/ebucore:timecode'),
            xpath_dest=XPathAttribute('./ebucore:duration/ebucore:timecode'),
            regex=None)
    )
    assignments.append(
        Assignment(
            name='timecode start',
            xpath_src=XPathAttribute(xpath='./ebucore:timecodeFormat/ebucore:timecodeStart/ebucore:timecode'),
            xpath_dest=XPathAttribute('./ebucore:start/ebucore:timecode'),
            regex=None)
    )
    assignments.append(
        Assignment(
            name='timecode end',
            xpath_src=XPathAttribute(xpath='./ebucore:timecodeFormat/ebucore:timecodeEnd/ebucore:timecode'),
            xpath_dest=XPathAttribute('./ebucore:end/ebucore:timecode'),
            regex=None)
    )

    assignments.append(
        Assignment(
            name='Normal Play Time',
            xpath_src=XPathAttribute(
                xpath='./ebucore:duration/ebucore:normalPlayTime'),
            xpath_dest=XPathAttribute(
                xpath='./ebucore:duration/ebucore:normalPlayTime'),
            regex=None,
            complex_map_fct=normal_play_time.__func__)
    )
