import os
import re

from RV import RV
from ebucore.Constants import Constants
from ebucore.ReferencedStandard import ReferencedStandards
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute
from lxml import etree


class TimedTextPackageFormatMatch:
    @staticmethod
    def timed_text_standard_by_path(in_path):
        rv = RV.info('Getting time text standard by given path: ' + in_path)

        # get file extension
        _, file_extension = os.path.splitext(in_path)
        if file_extension not in ['.xml']:
            return rv.add_child(RV.warning('Given file is not XML file - unable to check standard!')), None

        try:
            root = etree.parse(in_path)
        except etree.XMLSyntaxError as err:
            return rv.add(RV.error('While trying to parse xml file, error is: ' + str(err))), None

        # read SubtitleReel
        standard = None
        try:
            subtitle_reel_elem = root.find('.')
            regex = r"^\{(.*)\}(.*)$"
            matches = re.findall(regex, subtitle_reel_elem.tag)

            if len(matches) == 1:
                namespace = matches[0][0]
                tag_name = matches[0][1]
                if tag_name == 'SubtitleReel' and '428-7' in namespace:
                    standard = 'SMPTE ST 428-7'
                elif tag_name == 'tt' and namespace == 'http://www.w3.org/ns/ttml':
                    standard = 'W3C ttml-imsc1.1'
        except:
            standard = None

        if standard is None:
            return rv.add_child(RV.warning('No match for timed text standard')), None

        for referenced_standard in ReferencedStandards.timed_text:
            if standard == referenced_standard.standard:
                return rv.add_child(RV.info('Successfully matched timed text standard')), referenced_standard
        return rv.add_child(RV.warning('No match for timed text standard')), None

    @staticmethod
    def timed_text_standard_reference_match_name(in_param, out_param):
        rv = RV.info('Matching timed text standard reference name')
        tt_path_elem = in_param.find('./ebucore:locator', Constants.ns)

        if tt_path_elem is None:
            return rv.add_child(
                RV.warning('Unable to find expected "locator" element in ebucore source data')), False

        tt_path = tt_path_elem.text
        if os.path.exists(tt_path) is False:
            return rv.add_child(
                RV.error('Given timed text file does not exist: ' + str(tt_path))), False

        rv_sub, referenced_standard = TimedTextPackageFormatMatch.timed_text_standard_by_path(tt_path)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, None

        if referenced_standard is None:
            return rv.add_child(
                RV.warning('Given timed text file not supported by CPP')), False

        if referenced_standard.short_designation_loc == '':
            out_param.text = referenced_standard.short_designation_cpp
        else:
            out_param.text = referenced_standard.short_designation_loc
        return rv.add_child(RV.info('Successfully matched timed text standard reference name')), True

    @staticmethod
    def timed_text_standard_reference_match_uri(in_param, out_param):
        rv = RV.info('Matching timed text standard reference uri')
        tt_path_elem = in_param.find('./ebucore:locator', Constants.ns)

        if tt_path_elem is None:
            return rv.add_child(
                RV.warning('Unable to find expected "locator" element in ebucore source data')), False

        tt_path = tt_path_elem.text
        if os.path.exists(tt_path) is False:
            return rv.add_child(
                RV.error('Given timed text file does not exist: ' + str(tt_path))), False

        rv_sub, referenced_standard = TimedTextPackageFormatMatch.timed_text_standard_by_path(tt_path)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, None

        if referenced_standard is None:
            return rv.add_child(
                RV.warning('Given timed text file not supported by CPP')), False

        out_param.text = referenced_standard.link
        return rv.add_child(RV.info('Successfully matched timed text standard reference uri')), True

    src_root_xpath = './ebucore:coreMetadata/ebucore:format'
    assignments = list()
    assignments.append(
        Assignment(
            name='Timed Text Standard Reference Name',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute(
                './ebucore:format[@formatName="timedTextPackageFormat"]/'
                'ebucore:technicalAttributeString[@typeLabel="timedTextStandardReference"]'),
            regex=None,
            complex_map_fct=timed_text_standard_reference_match_name.__func__
        )
    )
    assignments.append(
        Assignment(
            name='Timed Text Standard Reference Uri',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute(
                './ebucore:format[@formatName="timedTextPackageFormat"]/'
                'ebucore:technicalAttributeUri[@typeLabel="timedTextStandardReference"]'),
            regex=None,
            complex_map_fct=timed_text_standard_reference_match_uri.__func__
        )
    )



