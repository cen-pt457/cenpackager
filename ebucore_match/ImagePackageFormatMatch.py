from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute
from utils.log import log
from ebucore.Constants import Constants
import os
from utils.utils import get_file_names_prefix, get_file_numbering_info, get_file_names_extension
from RV import RV


class ImagePackageFormatMatch:
	@staticmethod
	def filename_prefix_match(in_param, out_param):
		rv = RV.info('Custom match for filename prefix')

		path_first_image = in_param.text

		if os.path.exists(path_first_image) is False:
			return rv.add_child(
				RV.error('Given path to first file of sequence does not exists: ' + str(path_first_image))), False

		prefix = get_file_names_prefix(os.path.dirname(path_first_image))
		out_param.text = prefix
		return RV.info('Successfully matched filename prefix'), True

	@staticmethod
	def file_numbering_match(in_param, out_param):
		rv = RV.info('Custom match for file numbering')

		locator_elem = in_param.find('./ebucore:locator', Constants.ns)
		if locator_elem is None:
			return rv.add_child(
				RV.error('Unable to find ebucore:locator element for file numbering match')), False

		path_first_image = locator_elem.text
		if os.path.exists(path_first_image) is False:
			return rv.add_child(
				RV.error('Given path to first file of sequence does not exists: ' + str(path_first_image))), False

		num_digits, min_no, max_no = get_file_numbering_info(os.path.dirname(path_first_image))

		# num_digits
		filename_digits_elem = out_param.find(
			'./ebucore:technicalAttributeUnsignedByte[@typeLabel="filenameDigits"]', Constants.ns)
		if filename_digits_elem is None:
			return rv.add_child(RV.warning('Unable to find filename_digits_elem')), False
		filename_digits_elem.text = str(num_digits)

		# first index
		first_index_elem = out_param.find(
			'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="firstIndex"]', Constants.ns)
		if first_index_elem is None:
			return rv.add_child(RV.warning('Unable to find firstIndex')), False

		first_index_elem.text = str(min_no)

		# last index
		last_indexs_elem = out_param.find(
			'./ebucore:technicalAttributeUnsignedInteger[@typeLabel="lastIndex"]', Constants.ns)
		if last_indexs_elem is None:
			return rv.add_child(RV.warning('Unable to find num_digits')), False
		last_indexs_elem.text = str(max_no)
		return RV.info('Successfully matched file numbering'), True

	@staticmethod
	def filename_extension(in_param, out_param):
		rv = RV.info('Custom match for filename extension')
		path_first_image = in_param.text

		if os.path.exists(path_first_image) is False:
			return rv.add_child(RV.warning(
				'Given path to first file of sequence does not exists: ' + str(path_first_image))), False

		extension = get_file_names_extension(os.path.dirname(path_first_image))
		out_param.text = extension
		return RV.info('Successfully matched filename extension'), True

	src_root_xpath = './ebucore:coreMetadata/ebucore:format'
	assignments = list()
	assignments.append(
		Assignment(
			name='filename prefix',
			xpath_src=XPathAttribute(xpath='./ebucore:locator'),
			xpath_dest=XPathAttribute(
				'./ebucore:format[@formatName="imagePackageFormat"]'
				'/ebucore:technicalAttributeString[@typeLabel="filenamePrefix"]'),
			regex=None,
			complex_map_fct=filename_prefix_match.__func__
		)
	)
	assignments.append(
		Assignment(
			name='filename digits, first index, last index',
			xpath_src=XPathAttribute(xpath='.'),
			xpath_dest=XPathAttribute(
				'./ebucore:format[@formatName="imagePackageFormat"]'),
			regex=None,
			complex_map_fct=file_numbering_match.__func__
		)
	)
	assignments.append(
		Assignment(
			name='filename extension',
			xpath_src=XPathAttribute(xpath='./ebucore:locator'),
			xpath_dest=XPathAttribute(
				'./ebucore:format[@formatName="imagePackageFormat"]'
				'/ebucore:technicalAttributeString[@typeLabel="filenameExtension"]'),
			regex=None,
			complex_map_fct=filename_extension.__func__
		)
	)

