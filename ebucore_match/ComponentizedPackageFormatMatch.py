import os
import re
from lxml import etree
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute
from utils.utils import get_namespace
from ebucore.Constants import Constants
from RV import RV


class ComponentizedPackageFormatMatch:

    @staticmethod
    def get_assets_from_pkl(path):
        if os.path.exists(path) is False:
            return False
        if os.path.isdir(path):
            return False
        try:
            assets = []
            tree = etree.parse(path)
            try:
                rv_sub, ns = get_namespace(path)
                if not rv_sub.is_info():
                    return False
                root_elem = tree.getroot()

                pkl_assets = root_elem.xpath('//xmlns:AssetList/*/xmlns:Id', namespaces={'xmlns': ns})

                for asset in pkl_assets:
                    assets.append(asset.text)

                return assets
            except IndexError:
                return False
        except etree.XMLSyntaxError as err:
            return False

    @staticmethod
    def get_assets_from_cpl(path):
        if os.path.exists(path) is False:
            return False
        if os.path.isdir(path):
            return False
        try:
            assets = []
            tree = etree.parse(path)
            try:
                rv_sub, ns = get_namespace(path)
                if not rv_sub.is_info():
                    return False

                root_elem = tree.getroot()

                # DCP MAP
                cpl_assets = root_elem.xpath('//xmlns:AssetList/*/xmlns:Id', namespaces={'xmlns': ns})

                for asset in cpl_assets:
                    assets.append(asset.text)

                # IMF flavors
                cpl_assets = root_elem.xpath('//xmlns:ResourceList/*/xmlns:Id', namespaces={'xmlns': ns})

                for asset in cpl_assets:
                    assets.append(asset.text)

                return assets
            except IndexError:
                return False
        except etree.XMLSyntaxError as err:
            return False

    '''
    @staticmethod
    def is_cpl(path):
        if os.path.exists(path) is False:
            return False
        if os.path.isdir(path):
            return False
        try:
            tree = etree.parse(path)
            try:
                root_elem = tree.getroot()
                if root_elem.tag.endswith('CompositionPlaylist'):
                    return True
                else:
                    return False
            except IndexError:
                return False
        except etree.XMLSyntaxError as err:
            return False

    @staticmethod
    def is_pkl(path):
        if os.path.exists(path) is False:
            return False
        if os.path.isdir(path):
            return False
        try:
            tree = etree.parse(path)
            try:
                root_elem = tree.getroot()
                if root_elem.tag.endswith('PackingList'):
                    return True
                else:
                    return False
            except IndexError:
                return False
        except etree.XMLSyntaxError as _:
            return False
    '''

    @staticmethod
    def pkl_annotation(in_param, out_param):
        rv = RV.info('Match PKL annotation')

        # find locator to file
        pkl_path_elem = in_param.find('./ebucore:locator', Constants.ns)

        if pkl_path_elem is None:
            return rv.add_child(RV.warning('Unable to find expected "locator" element in ebucore source data')), False

        pkl_path = pkl_path_elem.text
        if os.path.exists(pkl_path) is False:
            return rv.add_child(RV.error('Given CPL file does not exist: ' + str(pkl_path))), False

        from Content.ComponentizedPackage import ComponentizedPackage as ContentComponentizedPackage
        if ContentComponentizedPackage.is_pkl(pkl_path):
            tree = etree.parse(pkl_path)
            root = tree.getroot()

            for element in root.getiterator():
                if element.tag.endswith('AnnotationText'):
                    # find pkl annotation text
                    pkl_annotation_elem = out_param.find(
                        './ebucore:technicalAttributeString[@typeLabel="pklAnnotationText"]', Constants.ns)
                    if pkl_annotation_elem is None:
                        return rv.add_child(RV.error('PKL annotation text element not found')), False
                    pkl_annotation_elem.text = element.text
                    return rv, True
        return rv, False

    @staticmethod
    def cpl_type(cpl_path):
        rv = RV.info('Detecting CPL type ' + cpl_path)

        # open xml file
        try:
            tree = etree.parse(cpl_path)
            root = tree.getroot()
        except etree.XMLSyntaxError as err:
            return rv.add_child(RV.error('While trying to parse XML, error is: ' + str(err))), {}

        # read SubtitleReel
        cpl_type = Constants.unknown_metadata_place_holder
        cpl_sub_type = Constants.unknown_metadata_place_holder
        try:
            subtitle_reel_elem = root.find('.')
            regex = r"^\{(.*)\}(.*)$"
            matches = re.findall(regex, subtitle_reel_elem.tag)

            for element in root.getiterator():
                if element.tag.endswith('ApplicationIdentification'):
                    app_id_elem = element

            if len(matches) == 1:

                namespace = matches[0][0]
                tag_name = matches[0][1]
                if tag_name == 'CompositionPlaylist':
                    if '429-7' in namespace:
                        cpl_type = 'DCP'
                        cpl_sub_type = 'SMPTE'
                    elif 'PROTO-ASDCP-CPL' in namespace:
                        cpl_type = 'DCP'
                        cpl_sub_type = 'Interop'
                    elif namespace.startswith('http://www.iis.fraunhofer.de/schemas/d-cinema/archives/') and \
                            'ArchiveCPL' in namespace:
                        cpl_type = 'DCP'
                        cpl_sub_type = 'MAP'
                    elif '2067-3' in namespace and app_id_elem is not None:
                        app_id = app_id_elem.text

                        if app_id.startswith('http://www.smpte-ra.org/schemas/2067-20/'):
                            cpl_type = 'IMF'
                            cpl_sub_type = 'APP2'
                        elif app_id.startswith('http://www.smpte-ra.org/schemas/2067-21/'):
                            cpl_type = 'IMF'
                            cpl_sub_type = 'APP2E'
                        elif app_id.startswith('http://www.smpte-ra.org/schemas/2067-40/'):
                            cpl_type = 'IMF'
                            cpl_sub_type = 'APP4'
                        elif app_id.startswith('http://www.smpte-ra.org/schemas/2067-50/'):
                            cpl_type = 'IMF'
                            cpl_sub_type = 'APP5'
                        elif app_id.startswith('tag:apple.com,2017:imf:rdd45:'):
                            cpl_type = 'IMF'
                            cpl_sub_type = 'RDD45'
                        elif app_id.startswith('http://www.digitalproductionpartnership.co.uk/schema/imf/TSP2121-1/'):
                            cpl_type = 'IMF'
                            cpl_sub_type = 'TSP2121-1'
                        elif app_id.startswith('UNCLEAR ATM'):
                            # ToDo: Include better filter for APP 'TSP2121-4'
                            cpl_type = 'IMF'
                            cpl_sub_type = 'TSP2121-4'

                elif tag_name == 'tt' and namespace == 'http://www.w3.org/ns/ttml':
                    cpl_type = 'W3C ttml-imsc1.1'
        except:
            cpl_type = Constants.unknown_metadata_place_holder
            cpl_sub_type = Constants.unknown_metadata_place_holder

        types = {
            'cpl_type': cpl_type,
            'cpl_sub_type': cpl_sub_type
        }
        rv.message = 'Successfully detected CPL type ' + cpl_path
        return rv, types

    @staticmethod
    def cpl_package_type(in_param, out_param):
        rv = RV.info('Matching CPL type')

        # for this, we first have to extract the PKL file location and get the type of all CPLs in folder
        pkl_path_elem = in_param.find('./ebucore:locator', Constants.ns)

        if pkl_path_elem is None:
            return rv.add_child(RV.warning('Unable to find expected "locator" element in ebucore source data')), False

        pkl_path = pkl_path_elem.text
        if os.path.exists(pkl_path) is False:
            return rv.add_child(RV.error('Given PKL file does not exist: ' + str(pkl_path))), False

        from Content.ComponentizedPackage import ComponentizedPackage as ContentComponentizedPackage
        if not ContentComponentizedPackage.is_pkl(pkl_path):
            return rv.add_child(RV.error('Given PKL file is no PKL: ' + str(pkl_path))), False

        cp_folder = os.path.dirname(pkl_path)
        cpl_list = list()
        for file in sorted(os.listdir(cp_folder)):
            full_file_path = os.path.join(cp_folder, file)
            if ContentComponentizedPackage.is_cpl(full_file_path):
                cpl_list.append(full_file_path)

        if len(cpl_list) == 0:
            return rv.add_child(RV.error('No CPLs found in folder: ' + str(cp_folder))), False

        cpl_types = list()
        for cpl in cpl_list:
            rv_sub, cpl_type = ComponentizedPackageFormatMatch.cpl_type(cpl)
            if not rv_sub.is_info():
                rv.add_child(rv_sub)
                if rv_sub.is_error():
                    return rv, False
            if cpl_type not in cpl_types:
                cpl_types.append(cpl_type)

        if len(cpl_types) == 0:
            return rv.add_child(RV.error('Unable to detect CPL type: ' + str(cp_folder))), False

        if len(cpl_types) > 1:
            return rv.add_child(RV.error('More than one CPL type in given componentized package: ' +
                                         str(cp_folder))), False

        referenced_standard = cpl_types[0]

        # find package type
        package_type_elem = out_param.find(
            './ebucore:technicalAttributeString[@typeLabel="packageType"]', Constants.ns)
        if package_type_elem is None:
            return rv.add_child(RV.error('Package type element not found')), False

        package_type_elem.text = referenced_standard['cpl_type']

        # find package sub type
        package_type_elem = out_param.find(
            './ebucore:technicalAttributeString[@typeLabel="packageSubtype"]', Constants.ns)
        if package_type_elem is None:
            return rv.add_child(RV.error('Package sub-type element not found')), False
        package_type_elem.text = referenced_standard['cpl_sub_type']

        return RV.info('Successfully matched CPL type'), True

    @staticmethod
    def is_self_contained(in_param, out_param):
        rv = RV.info('Matching Self contained package')

        # find self contained tag in out_param
        # find package sub type
        self_contained_package_elem = out_param.find(
            './ebucore:technicalAttributeBoolean[@typeLabel="selfContainedPackage"]', Constants.ns)
        if self_contained_package_elem is None:
            return rv.add_child(RV.error('Package self-contained element not found')), False
        self_contained_package_elem.text = 'false'

        # for this, we first have to extract the PKL file location and get the type of all CPLs in folder
        pkl_path_elem = in_param.find('./ebucore:locator', Constants.ns)

        if pkl_path_elem is None:
            return rv.add_child(RV.warning('Unable to find expected "locator" element in ebucore source data')), False

        pkl_path = pkl_path_elem.text
        if os.path.exists(pkl_path) is False:
            return rv.add_child(RV.error('Given PKL file does not exist: ' + str(pkl_path))), False

        from Content.ComponentizedPackage import ComponentizedPackage as ContentComponentizedPackage
        if not ContentComponentizedPackage.is_pkl(pkl_path):
            return rv.add_child(RV.error('Given PKL file is no PKL: ' + str(pkl_path))), False

        # get all CPLs in folder
        cp_folder = os.path.dirname(pkl_path)
        cpl_list = list()
        for file in sorted(os.listdir(cp_folder)):
            full_file_path = os.path.join(cp_folder, file)
            if ContentComponentizedPackage.is_cpl(full_file_path):
                cpl_list.append(full_file_path)

        if len(cpl_list) == 0:
            return rv, True

        # collect all assets references in CPLs
        all_assets_cpl = list()
        for path_cpl in cpl_list:
            all_assets_cpl += ComponentizedPackageFormatMatch.get_assets_from_cpl(path_cpl)

        all_assets_pkl = ComponentizedPackageFormatMatch.get_assets_from_pkl(pkl_path)

        for cpl_asset in all_assets_cpl:
            if cpl_asset not in all_assets_pkl:
                return rv, True

        self_contained_package_elem.text = 'true'

        # remove tag <referencedPackage> since this package is self-contained
        referenced_package_elem = self_contained_package_elem.find(
            '../ebucore:technicalAttributeString[@typeLabel="referencedPackage"]', Constants.ns)
        if referenced_package_elem is None:
            rv.add_child(RV.warning('Unable to find <referencedPackage> tag.'))
        else:
            out_param.remove(referenced_package_elem)
        return rv, True

    src_root_xpath = './ebucore:coreMetadata/ebucore:format'
    assignments = list()

    assignments.append(
        Assignment(
            name='CPL Type and Subtype',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute('./ebucore:format[@formatName="componentizedPackageFormat"]'),
            regex=None,
            complex_map_fct=cpl_package_type.__func__
        )
    )

    assignments.append(
        Assignment(
            name='PKL annotation',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute('./ebucore:format[@formatName="componentizedPackageFormat"]'),
            regex=None,
            complex_map_fct=pkl_annotation.__func__
        )
    )

    assignments.append(
        Assignment(
            name='Self contained Package',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute('./ebucore:format[@formatName="componentizedPackageFormat"]'),
            regex=None,
            complex_map_fct=is_self_contained.__func__
        )
    )
