import os
from lxml import etree
from ebucore_match.Assignment import Assignment
from ebucore_match.XPathAttribute import XPathAttribute
from utils.utils import convert_iso8601_to_hh_mm_ss_s
from utils.utils import hh_mm_ss_s_to_frames
from ebucore.Constants import Constants
from ebucore_match.ComponentizedPackageFormatMatch import ComponentizedPackageFormatMatch
from RV import RV


class CplFormatMatch:

    @staticmethod
    def get_content_title(cpl_path):
        rv = RV.info('Getting content title from cpl: ' + cpl_path)
        try:
            tree = etree.parse(cpl_path)
            root = tree.getroot()

            for element in root.getiterator():
                if element.tag.endswith('ContentTitleText') or element.tag.endswith('ContentTitle'):
                    return rv, element.text

        except etree.XMLSyntaxError as err:
            return rv.add_child(RV.error('While trying to parse XML, error is: ' + str(cpl_path))), \
                   Constants.unknown_metadata_place_holder

        return rv, Constants.unknown_metadata_place_holder

    @staticmethod
    def cpl_content_title(in_param, out_param):
        rv = RV.info('Matching content title for cpl')
        # find locator to file
        cpl_path_elem = in_param.find('./ebucore:locator', Constants.ns)

        if cpl_path_elem is None:
            return rv.add_child(RV.warning('Unable to find expected "locator" element in ebucore source data')), False

        cpl_path = cpl_path_elem.text
        if os.path.exists(cpl_path) is False:
            return rv.add_child(RV.error('Given CPL file does not exist: ' + str(cpl_path))), False

        rv_sub, referenced_standard = ComponentizedPackageFormatMatch.cpl_type(cpl_path)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, None

        if referenced_standard['cpl_type'] not in ['DCP', 'IMF']:
           return rv.add_child(RV.warning('Given path to CPL has unknown CPL type.')), False

        rv_sub, content_title = CplFormatMatch.get_content_title(cpl_path)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, False

        out_param.text = content_title
        return RV.info('Successfully matched CPLs content title'), True

    @staticmethod
    def cpl_duration(in_param, out_param):
        rv = RV.info('Matching duration for cpl')

        frame_rate_elem = in_param.find('./ebucore:videoFormat/ebucore:frameRate', Constants.ns)
        if frame_rate_elem is None:
            return rv.add_child(RV.warning('Unable to find expected "frameRate" element in ebucore source data')), False

        frame_rate = frame_rate_elem.text
        frame_rate_as_float = float(frame_rate)
        frame_rate_as_int = int(frame_rate)

        is_fractional = True
        if frame_rate_as_float == frame_rate_as_int:
            is_fractional = False

        normal_playtime_elem = in_param.find('./ebucore:duration/ebucore:normalPlayTime', Constants.ns)
        if normal_playtime_elem is None:
            return rv.add_child(RV.warning('Unable to find expected "duration" element in ebucore source data')), False
        normal_playtime = normal_playtime_elem.text

        rv_sub, playtime_as_duration = convert_iso8601_to_hh_mm_ss_s(normal_playtime)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, False

        rv_sub, duration_in_frames = hh_mm_ss_s_to_frames(playtime_as_duration, frame_rate_as_float)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, False

        edit_unit_number_elem = out_param.find('./ebucore:editUnitNumber', Constants.ns)
        if edit_unit_number_elem is None:
            return rv.add_child(RV.warning('Unable to find "editUnitNumber" element in ebucore source data')), False
        edit_unit_number_elem.text = str(int(duration_in_frames))

        return RV.info('Successfully matched CPLs duration'), True

    src_root_xpath = './ebucore:coreMetadata/ebucore:format'
    assignments = list()

    assignments.append(
        Assignment(
            name='CPL Type and Subtype',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute(
                './ebucore:technicalAttributeString[@typeLabel="cplContentTitleText"]'),
            regex=None,
            complex_map_fct=cpl_content_title.__func__
        )
    )
    assignments.append(
        Assignment(
            name='Duration',
            xpath_src=XPathAttribute(xpath='.'),
            xpath_dest=XPathAttribute(
                './ebucore:duration[@typeLabel="durationInFrames"]'),
            regex=None,
            complex_map_fct=cpl_duration.__func__
        )
    )




