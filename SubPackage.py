import os
from utils.log import log
import shutil
from SubPackagePkl import SubPackagePkl
from RV import RV
from utils.utils import num_provenance_metadata_files, get_id_from_filename


class SubPackage:
    prefixes = ['image', 'sound', 'timedText', 'audiovisual', 'componentized', 'extra']

    def __init__(self):
        """Constructor: Unused"""
        pass

    @staticmethod
    def is_sub_package(path_abs):
        """
        Checks if a given path points to a sub-package or not

        :param path_abs: absolute path to be checked whether it is a sub-package or not
        :return: True if given path points to a sub-package, otherwise False
        """

        if os.path.isdir(path_abs) is False:
            return False
        folder_name = os.path.basename(path_abs)
        for prefix in SubPackage.prefixes:
            if folder_name.startswith(prefix + 'Package_'):
                return True
        return False

    @staticmethod
    def get_content_type_from_path(path_item):
        """
        Extracts a supported content type from a given file path. Useful to get the content type from
        a sub-package path

        Parameters
        ----------
        path_item : str, mandatory
            absolute or relative path to sub-package
        """
        rv = RV.info('Extracting content type from path: ' + path_item)

        path_item = os.path.basename(path_item)
        content_type = path_item.split('_')
        if len(content_type) != 2:
            return rv.add_child(RV.error('Unable to read content type from sub-package folder: ' + path_item)), None

        content_type = content_type[0]
        content_type_without_package = content_type.replace('Package', '')
        if content_type_without_package not in SubPackage.prefixes:
            return rv.add_child(RV.error('Unknown content type for sub package: ' + content_type)), None

        return rv, content_type

    @staticmethod
    def add_item(content_type, sub_package_path_abs, provenance_metadata_path_abs):
        """
        Generic interface function to add items to a sub-package. This function simply checks the type and
        forwards the call to a specific function if the type is supported

        Parameters
        ----------
        content_type : str, mandatory
            can be one of the following: ancillaryData or provMD
        sub_package_path_abs : str, mandatory
            absolute path the the existing sub package
        provenance_metadata_path_abs : str, mandatory
            absolute path pointing to existing provenance metadata file to be added
        :return: cenpackager return value - might contain child elements describing warnings and errors
        """
        rv = RV.info('Adding ' + provenance_metadata_path_abs + ' + to sub-package: ' + sub_package_path_abs)

        if content_type == "ancillaryData":
            rv_sub = SubPackage.add_ancillary_data(sub_package_path_abs, provenance_metadata_path_abs)
            if not rv_sub.is_info():
                rv.add_child(rv_sub)
                if rv_sub.is_error():
                    return rv
        elif content_type == "provMD":
            rv_sub = SubPackage.add_provenance_metadata(sub_package_path_abs, provenance_metadata_path_abs)
            if not rv_sub.is_info():
                rv.add_child(rv_sub)
                if rv_sub.is_error():
                    return rv
        else:
            return rv.add_child(RV.error("This content type cannot be added to a sub-package: " + content_type))

        # -----------------------------------------------------------------
        # write Subpackage's Packing List
        # -----------------------------------------------------------------
        # get UUID of existing sub package
        sub_package_id = get_id_from_filename(sub_package_path_abs)
        rv_sub, subpackage_type = SubPackage.get_content_type_from_path(sub_package_path_abs)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv

        rv_sub = SubPackagePkl.update(sub_package_path_abs, subpackage_type, sub_package_id)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv

        return rv

    @staticmethod
    def add_ancillary_data(path_destination_abs, path_item):
        """
        Adds ancillary data to an existing CEN Preservation Package
        preservationPackingList.xml will be updated by calling function

        Parameters
        ----------
        path_destination_abs : str, mandatory
            absolute path to existing CEN Preservation Package
        path_item : str, mandatory
            absolute path the content that shall be added to CEN Preservation Package
        """
        rv = RV.info('Adding ancillary data (' + path_item + ') to sub-package: ' + path_destination_abs)

        if not os.path.exists(path_destination_abs):
            return rv.add_child(RV.error('sub-package path not existing: ' + path_destination_abs))
        if not os.path.exists(path_item):
            return rv.add_child(RV.error('Item to add not existing: ' + path_item))

        ancillary_data_path = os.path.join(path_destination_abs, 'ancillaryData')
        if not os.path.exists(ancillary_data_path):
            os.makedirs(ancillary_data_path)

        if os.path.isdir(path_item):
            return rv.add_child(RV.error('Given path points to a folder - provide path to a file: ' + path_item))

        # ToDo: Add some checks if this item shall be added.
        try:
            shutil.copy(path_item, ancillary_data_path)
        except OSError as e:
            return rv.add_child(RV.error('While trying to copy ancillary data file. Error: %s' % e))
        return rv

    @staticmethod
    def add_provenance_metadata(sub_package_path_abs, provenance_metadata_path_abs):
        """
        Add provenance metadata to existing subpackage.

        Parameters
        ----------
        sub_package_path_abs : str, mandatory
            absolute path the the existing sub package
        provenance_metadata_path_abs : str, mandatory
            absolute path pointing to existing provenance metadata file to be added
        """
        rv = RV.info(
            'Adding provenance metadata ' + provenance_metadata_path_abs + ' + to existing package: ' +
            sub_package_path_abs)

        if os.path.exists(sub_package_path_abs) is False:
            return rv.add_child(RV.error('Given sub package does not exists: ' + sub_package_path_abs))

        if os.path.exists(provenance_metadata_path_abs) is False:
            return rv.add_child(RV.error(
                'Given path to existing provenance metadata does not exists: ' + provenance_metadata_path_abs))

        destination_metadata_folder = os.path.join(sub_package_path_abs, 'metadata')
        if os.path.exists(destination_metadata_folder) is False:
            return rv.add_child(RV.error('Given sub package has no metadata folder: ' + destination_metadata_folder))

        # get first available provenance md number
        num_existing_provenance_md_files = num_provenance_metadata_files(destination_metadata_folder)

        new_file_id = str(num_existing_provenance_md_files).zfill(4)
        new_filename_rel = 'digiProvMD_' + sub_package_id + '_' + new_file_id + '.xml'

        new_filename_abs = os.path.join(destination_metadata_folder, new_filename_rel)
        # ToDo: Add some checks if this item shall be added.
        try:
            shutil.copy(provenance_metadata_path_abs, new_filename_abs)
        except OSError as e:
            return rv.add_child(RV.error('While trying to copy ancillary data file. Error: %s' % e))

        return rv

    @staticmethod
    def subpackage_uuid(sub_package_path_abs):
        """
        Returns a sub-package's uuid from given path sub_package_path_abs. The uuid is extracted from the
        file-name.

        :param sub_package_path_abs: absolute path tp CPP
        :return: The uuid if it can be extracted, otherwise an empty string ('')
        """
        if os.path.isdir(sub_package_path_abs) is False:
            log.warning('Given path does not point to a subpackage folder: ' + sub_package_path_abs)
            return ''

        # find packing list
        pkl_path = os.path.join(sub_package_path_abs, 'packingList.xml')
        if os.path.exists(pkl_path) is False:
            log.error('no packing list in subpackage path: ' + sub_package_path_abs)
            return ''

        folder_name = os.path.basename(sub_package_path_abs)

        subpackage_uuid = get_id_from_filename(folder_name)

        return subpackage_uuid

    @staticmethod
    def prefix(path_abs):
        """
        Tries to extract a sub-packages prefix from given path path_abs

        :param path_abs: absolute path including a sub-package prefix
        :return: Returns the prefix if it can be detected, otherwise an empty string ('')
        """

        if os.path.isdir(path_abs) is False:
            log.warning('Given path does not point to a subpackage folder: ' + path_abs)
            return ''

        folder_name = os.path.basename(path_abs)
        for prefix in SubPackage.prefixes:
            if folder_name.startswith(prefix + 'Package_'):
                return prefix

        log.warning('Could not read prefix from subpackage folder: ' + path_abs)
        return ''