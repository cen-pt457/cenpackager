from Content.AbstractPackage import AbstractPackage
from RV import RV
import os
from lxml import etree
import errno
import shutil
from ebucore.ImagePackage import ImagePackage as EbuCoreImagePackage
from ebucore.EbuCoreMain import EbuCoreMain
from utils.utils import image_sequence_continuous_checker


class ImagePackage(AbstractPackage):
	def __init__(self):
		AbstractPackage.__init__(self)

	@staticmethod
	def copy_subpackage_data(path_source_data, path_subpackage_data_folder):
		rv = RV.info('Copying data to image subpackage folder: ' + path_subpackage_data_folder)

		continuous_image_sequence = image_sequence_continuous_checker(path_source_data)
		if len(continuous_image_sequence) == 0:
			return rv.add_child(
				RV.error('Can''t find any image sequence: ' + str(path_source_data)))
		elif len(continuous_image_sequence) > 1:
			rv.add_child(
				RV.warning('Given image sequence has no continuous file numbering: ' + str(path_source_data)))

		if os.path.exists(path_subpackage_data_folder) is False:
			os.makedirs(path_subpackage_data_folder)
		image_counter = 0

		for image_sequence in continuous_image_sequence:
			src_images_rel = image_sequence
			src_images_abs = list()
			for src_image_rel in src_images_rel:
				src_images_abs.append(os.path.join(path_source_data, src_image_rel))

			for src_image_abs in src_images_abs:
				_, file_extension = os.path.splitext(src_image_abs)

				new_file_name = 'image_' + str(image_counter).zfill(8) + file_extension
				new_file_path_abs = os.path.join(path_subpackage_data_folder, new_file_name)
				shutil.copy(src_image_abs, new_file_path_abs)
				image_counter += 1

		return rv

	@staticmethod
	def write_subpackage_metadata(sub_package_uuid, path_subpackage):
		rv = RV.info('Writing subpackage metadata for subpackage: ' + path_subpackage)

		path_subpackage_data_folder = os.path.join(path_subpackage, 'data')
		path_subpackage_metadata_folder = os.path.join(path_subpackage, 'metadata')

		ebucore_main = EbuCoreMain()
		rv_sub, ebucore_main_md = ebucore_main.collect_metadata(
			media_source_file=None,
			xml_metadata_file_in=None)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		md_file_counter = 0

		image_package = EbuCoreImagePackage()
		rv_sub, image_package_md_elem = image_package.collect_metadata(path_subpackage_data_folder, None)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv
		if image_package_md_elem is not None:
			file_base_name = 'techMD_' + str(sub_package_uuid) + '_' + ('%04d' % md_file_counter) + '.xml'

			metadata_file_name = os.path.join(path_subpackage_metadata_folder, file_base_name)

			ebucore_main_md.insert(len(ebucore_main_md), image_package_md_elem)

			tree = etree.ElementTree(ebucore_main_md)
			tree.write(metadata_file_name, pretty_print=True, xml_declaration=True, encoding="utf-8")
		else:
			return rv.add_child(RV.error('While trying to collect metadata. Returned XML element is None'))

		# rv.message = 'Successfully written subpackage metadata for subpackage: ' + path_subpackage
		return rv
