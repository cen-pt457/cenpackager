from Content.AbstractPackage import AbstractPackage
import os
from ebucore.Constants import Constants
from mets.FileGrp import FileGrp
from mets.Fptr import Fptr
import shutil
from utils.Hash import Hash
from utils.log import log
import magic
from mets.File import File
from RV import RV
from lxml import etree
from ebucore.ComponentizedPackage import ComponentizedPackage as ComponentizedPackageMD
from ebucore.EbuCoreMain import EbuCoreMain
from utils.utils import get_file_numbering_info, get_folders_and_files


class ComponentizedPackage(AbstractPackage):
    def __init__(self):
        AbstractPackage.__init__(self)

    @staticmethod
    def is_cpl(path):
        if os.path.exists(path) is False:
            return False
        if os.path.isdir(path):
            return False
        try:
            tree = etree.parse(path)
            try:
                root_elem = tree.getroot()
                if root_elem.tag.endswith('CompositionPlaylist'):
                    return True
                else:
                    return False
            except IndexError:
                return False
        except etree.XMLSyntaxError as err:
            return False

    @staticmethod
    def is_pkl(path):
        if os.path.exists(path) is False:
            return False
        if os.path.isdir(path):
            return False
        try:
            tree = etree.parse(path)
            try:
                root_elem = tree.getroot()
                if root_elem.tag.endswith('PackingList'):
                    return True
                else:
                    return False
            except IndexError:
                return False
        except etree.XMLSyntaxError as _:
            return False

    @staticmethod
    def get_cpls(abs_path_package_source):
        rv = RV.info('Detecting absolute CPL file-names in package folder: ' + abs_path_package_source)
        cpls = list()
        for file in sorted(os.listdir(abs_path_package_source)):
            full_data_path = os.path.join(abs_path_package_source, file)
            if not ComponentizedPackage.is_cpl(full_data_path):
                continue
            cpls.append(full_data_path)
        return rv, cpls

    @staticmethod
    def copy_subpackage_data(path_source_data, path_subpackage_data_folder):
        rv = RV.info('Copying data to subpackage folder: ' + path_subpackage_data_folder)
        if not ComponentizedPackage.is_pkl(path_source_data):
            return rv.add_child(RV.error('Given file is no PacKingList (PKL) for DCP, IAP, MAP or IMF'))

        package_folder = os.path.dirname(path_source_data)
        for data_file in sorted(os.listdir(package_folder)):
            if data_file in ['.DS_Store']:
                continue
            try:
                shutil.copy(os.path.join(package_folder, data_file), path_subpackage_data_folder)
            except OSError as e:
                return rv.add_child(RV.error('Directory not copied. Error: %s' % e))
        rv.message = 'Successfully copied data to subpackage folder: ' + path_subpackage_data_folder
        return rv

    @staticmethod
    def write_subpackage_metadata(sub_package_uuid, path_subpackage):
        rv = RV.info('Writing subpackage metadata for subpackage: ' + path_subpackage)

        path_subpackage_data_folder = os.path.join(path_subpackage, 'data')
        path_subpackage_metadata_folder = os.path.join(path_subpackage, 'metadata')

        ebucore_main = EbuCoreMain()
        rv_sub, ebucore_main_md = ebucore_main.collect_metadata(
            media_source_file=None,
            xml_metadata_file_in=None)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv

        componentized_package = ComponentizedPackageMD()

        tech_md_file_cnt = 0
        for file in sorted(os.listdir(path_subpackage_data_folder)):
            if not file.endswith('.xml'):
                continue

            full_data_path = os.path.join(path_subpackage_data_folder, file)
            if ComponentizedPackage.is_pkl(full_data_path):

                rv_sub, componentized_package_md_elem = \
                    componentized_package.collect_metadata(full_data_path, None)
                if rv_sub.is_error():
                    rv.add_child(rv_sub)
                    return rv

                if componentized_package_md_elem is not None:
                    file_base_name = 'techMD_' + str(sub_package_uuid) + '_' + ('%04d' % tech_md_file_cnt) + '.xml'
                    metadata_file_name = os.path.join(path_subpackage_metadata_folder, file_base_name)

                    ebucore_main_md.insert(len(ebucore_main_md), componentized_package_md_elem)

                    tree = etree.ElementTree(ebucore_main_md)
                    tree.write(metadata_file_name, pretty_print=True, xml_declaration=True, encoding="utf-8")
                else:
                    return rv.add_child(RV.error('While trying to collect metadata. Returned XML element is None'))
                tech_md_file_cnt += 1

        rv.message = 'Successfully written subpackage metadata for subpackage: ' + path_subpackage
        return rv

    @staticmethod
    def get_id_path_pairs_from_assetmap(abs_package_path):
        rv = RV.info('Converting asset it to absolute path using the asset map in: ' + abs_package_path)

        id_path_pairs_dict = {}

        if os.path.exists(abs_package_path) is False:
            return rv.add_child(RV.error('Given path does not exists: ' + abs_package_path))

        abs_assetmap_path = None
        source_files = os.listdir(abs_package_path)

        for source_file in source_files:
            if source_file in ['ASSETMAP', 'ASSETMAP.xml']:
                abs_assetmap_path = os.path.join(abs_package_path, source_file)
                break

        if abs_assetmap_path is None:
            return rv.add_child(RV.error('No ASSETMAP or ASSETMAP.xml in folder: ' + abs_assetmap_path))

        with open(abs_assetmap_path) as assetmap:
            doc = etree.parse(assetmap)

            test = doc.find('./')

            try:
                all_elements =  test = doc.find('./')
                if len(all_elements.nsmap.values()) != 1:
                    return rv.add_child(RV.error('No or more than one namespace(s) in assetmap: ' + abs_assetmap_path))

                for namespace in all_elements.nsmap.values():
                    assetmap_ns = {
                        "am": namespace
                    }
                    break
                # print(assetmap_ns)
            except Exception as e:
                return rv.add_child(RV.error('While trying to read namespace in assetmap: ' + abs_assetmap_path))

            assets = doc.find('./am:AssetList', assetmap_ns)
            if assets is None:
                return rv.add_child(RV.error('No Asset entries in assetmap: ' + abs_assetmap_path))

            if len(assets) == 0:
                return rv.add_child(RV.error('No Asset entries in assetmap: ' + abs_assetmap_path))

            for asset in assets:

                id = asset.find('./am:Id', assetmap_ns)
                path = asset.find('./am:ChunkList/am:Chunk/am:Path', assetmap_ns)
                # vol_idx = asset.find('./am:ChunkList/am:Chunk/am:VolumeIndex', assetmap_ns)
                # offset = asset.find('./am:ChunkList/am:Chunk/am:Offset', assetmap_ns)
                # length = asset.find('./am:ChunkList/am:Chunk/am:Length', assetmap_ns)
                id_path_pairs_dict[id.text] = os.path.join(abs_package_path, path.text)
        return rv, id_path_pairs_dict

    @staticmethod
    def get_original_file_names_by_ids(asset_id_list, abs_pkl_path):

        rv = RV.info('Getting original file-names from PKL: ' + abs_pkl_path)
        picture_track_asset_paths = list()

        rv_sub, id_path_pairs_dict = ComponentizedPackage.get_id_path_pairs_from_assetmap(os.path.dirname(abs_pkl_path))
        if rv_sub.is_error():
            rv.add_child(rv_sub)
            return rv

        abs_data_folder = os.path.dirname(abs_pkl_path)

        for picture_track_asset_id in asset_id_list:

            try:
                path = id_path_pairs_dict[picture_track_asset_id]
                picture_track_asset_paths.append(path)
            except KeyError as _:
                pass

        return rv, picture_track_asset_paths


    @staticmethod
    def get_picture_track_files(abs_cpl_path, abs_pkl_path):
        rv = RV.info('Getting picture track files from cpl and pkl: ' + abs_cpl_path + ', ' + abs_pkl_path)
        picture_track_asset_ids = list()
        try:
            tree = etree.parse(abs_cpl_path)
            root = tree.getroot()
            namespace = ''
            for _key, value in root.nsmap.items():
                if 'http://www.smpte-ra.org/schemas/429-7' in value:
                    namespace = value
                    break
                elif 'http://www.digicine.com/PROTO-ASDCP-CPL-' in value:
                    namespace = value
                    break

            main_picture_id_elems = root.findall(".//*{" + namespace + "}MainPicture/{" + namespace + "}Id")
            for main_picture_id_elem in main_picture_id_elems:
                picture_track_asset_ids.append(main_picture_id_elem.text)

        except etree.XMLSyntaxError as err:
            return rv.add_child(RV.error('While trying to parse XML, error is: ' + str(abs_cpl_path))), ()

        rv_sub, picture_track_asset_paths =\
            ComponentizedPackage.get_original_file_names_by_ids(picture_track_asset_ids, abs_pkl_path)

        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, []

        return rv, picture_track_asset_paths


class ComponentizedImfPackage(ComponentizedPackage):
    @staticmethod
    def get_picture_track_files(abs_cpl_path, abs_pkl_path):
        rv = RV.info('Getting picture track files from cpl and pkl: ' + abs_cpl_path + ', ' + abs_pkl_path)
        image_sequence_track_file_ids = list()
        try:
            tree = etree.parse(abs_cpl_path)
            root = tree.getroot()
            namespace_2067_2 = ''
            namespace_2067_3 = ''

            for _key, value in root.nsmap.items():
                if 'http://www.smpte-ra.org/schemas/2067-3' in value:
                    namespace_2067_3 = value
                    namespace_2067_2 = namespace_2067_3.replace('2067-3', '2067-2')
                    break

            main_image_sequence_elems = root.findall(".//*{" + namespace_2067_2 + "}MainImageSequence")

            for main_image_sequence_elem in main_image_sequence_elems:
                track_file_id_elem = main_image_sequence_elem.findall(".//*{" + namespace_2067_3 + "}TrackFileId")
                if len(track_file_id_elem) > 0:
                    image_sequence_track_file_ids.append(track_file_id_elem[0].text)

        except etree.XMLSyntaxError as err:
            return rv.add_child(RV.error('While trying to parse XML, error is: ' + str(abs_cpl_path))), ()

        rv_sub, picture_track_asset_paths =\
            ComponentizedPackage.get_original_file_names_by_ids(image_sequence_track_file_ids, abs_pkl_path)

        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, []

        return rv, picture_track_asset_paths
