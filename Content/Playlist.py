from RV import RV
from lxml import etree
import os


class Playlist:
    ns = {
        "cpp": "urn:cen.eu:en17650:2022:ns:playlist",
        "cpl": "http://www.smpte-ra.org/schemas/2067-3/2016"
    }

    def __init__(self):
        pass

    @staticmethod
    def check(abs_cpp_path, path_playlist_abs):
        rv = RV.info('Consistency checks for playlist ' + path_playlist_abs)

        rv_sub, state = Playlist.check_referenced_assets_exist(abs_cpp_path, path_playlist_abs)
        if not rv_sub.is_info():
            rv.add_child(rv_sub)
            if rv_sub.is_error():
                return rv, state

        return rv, True

    @staticmethod
    def check_referenced_assets_exist(abs_cpp_path, abs_playlist_past):
        rv = RV.info('Checking referenced assets for playlist ' + abs_playlist_past)
        try:
            tree = etree.parse(abs_playlist_past)
            root = tree.getroot()

            video_packing_list_id_elems = \
                root.findall(".//cpp:ImageSequence/cpl:ResourceList/cpl:Resource/cpp:CppPackingListId", Playlist.ns)

            if video_packing_list_id_elems is None:
                return rv.add_child(
                    RV.error('Unable to find video packing list id  element in playlist')), False

            for video_elem in video_packing_list_id_elems:
                abs_path_sub_folder = os.path.join(abs_cpp_path, video_elem.text)
                if os.path.exists(abs_path_sub_folder) is False:
                    rv.add_child(RV.warning(
                            video_elem.text + ' is currently not part of the current CEN Package: ' + abs_cpp_path))

            audio_packing_list_id_elems = \
                root.findall(".//cpp:AudioSequence/cpl:ResourceList/cpl:Resource/cpp:CppPackingListId", Playlist.ns)

            if audio_packing_list_id_elems is None:
                return rv.add_child(
                    RV.error('Unable to find audio packing list id  element in playlist')), False

            for audio_elem in audio_packing_list_id_elems:
                abs_path_sub_folder = os.path.join(abs_cpp_path, audio_elem.text)
                if os.path.exists(abs_path_sub_folder) is False:
                    rv.add_child(RV.warning(
                        audio_elem.text + ' is currently not part of the current CEN Package: ' + abs_cpp_path))

        except etree.XMLSyntaxError as err:
            return rv.add_child(
                RV.error('While trying to parse XML ' + str(abs_playlist_past) + ' , error is: ' + str(err))), False

        return rv, True
