from lxml import etree
from RV import RV
from ebucore.Constants import Constants
import lxml
from lxml import etree
from ebucore.ImagePackage import ImagePackage
from ebucore.ComponentizedPackage import ComponentizedPackage
import pickle


ebucore_md_example = '/Users/sbg/Downloads/techMD_a5ac3d6e-0380-4680-a207-ecdf6c8725dc_0000.xml'
ebucore_xsd = './xsd/ebucore.xsd'

provMd = './unittest/test_material/provenance_metadata/provMD_example.xml'
mets_xsd = './xsd/mets.xsd'

checker_report = './unittest/test_material/checker_report/checker_report_example.xml'
checker_report_xsd = './xsd/checker_report_registry.xsd'


def xmlschema():
	import xmlschema
	xmlschema.validate(ebucore_md_example, ebucore_xsd)


def xsd_lxml_check(filename_xsd, filename_xml):
	rv = RV.info("Validating " + filename_xml + " against schema: " + filename_xsd)

	with open(filename_xsd) as f:
		doc = etree.parse(f)

	print("Validating schema ... ")
	try:
		schema = etree.XMLSchema(doc)

	except lxml.etree.XMLSchemaParseError as e:
		print(e)
		exit(1)

	print("Schema OK")

	with open(filename_xml) as f:
		doc = etree.parse(f)

	print("Validating document ...")
	try:
		schema.assertValid(doc)
	except lxml.etree.DocumentInvalid as e:
		for error in schema.error_log:
			rv.add_child(RV.error("  Line {}: {}".format(error.line, error.message)))
		return rv

	print("Document OK")
	return RV.info("Document OK")


def main():
	pass


def hybrid_check():
	rv = RV.info('Trying to apply a hybrid check')
	path_src_file = 'legacy/example_image_package.xml'
	metadata_type = 'imagePackage'

	try:
		tree = etree.parse(path_src_file)
	except etree.XMLSyntaxError as err:
		return rv.add(RV.error('While trying to parse xml file ' + path_src_file + ', error is: ' + str(err)))

	ebucore_main_check = tree.xpath('/ebucore:ebucoreMain', namespaces=Constants.ns)
	if len(ebucore_main_check) == 0:
		return rv.add_child(RV.error('No ebucoreMain element found in given metadata file: ' + path_src_file))
	if len(ebucore_main_check) > 1:
		return rv.add_child(
			RV.error('More than one ebucoreMain element found in given metadata file: ' + path_src_file))
	else:
		ebucore_main_elem = ebucore_main_check[0]

	if metadata_type == 'imagePackage':
		xpath_abs = '/ebucore:ebucoreMain/ebucore:ebucoreMetadata/ebucore:imagePackage'
		image_package_elem = tree.xpath(xpath_abs, namespaces=Constants.ns)
		if len(image_package_elem) == 0:
			return rv.add_child(
				RV.error('no imagePackage format element found in: ' + path_src_file))

		line_offset = 0
		ip = ImagePackage()
		rv_sub = ip.validate(source_element=image_package_elem[0], xpath_abs=xpath_abs, line_offset=line_offset)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv
		return rv

	elif metadata_type == 'componentizedPackage':
		xpath_abs = '/ebucore:ebucoreMain/ebucore:ebucoreMetadata/ebucore:componentizedPackage'
		componentized_package_elem = tree.xpath(xpath_abs, namespaces=Constants.ns)
		if len(componentized_package_elem) == 0:
			return rv.add_child(
				RV.error('no componentizedPackage format element found in: ' + path_src_file))

		line_offset = 0
		cp = ComponentizedPackage()
		rv_sub = cp.validate(source_element=ebucore_main_elem, xpath_abs=xpath_abs, line_offset=line_offset)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv
		return rv


if __name__ == '__main__':
	# rv = xsd_lxml_check(checker_report_xsd, checker_report)
	rv = xsd_lxml_check(ebucore_xsd, ebucore_md_example)

	# rv = xsd_lxml_check(mets_xsd, provMd)

	rv.dump()

