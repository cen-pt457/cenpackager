import sys
import os
import getopt
from utils.log import log
from PreservationPackage import PreservationPackage
from SubPackage import SubPackage
from utils.utils import check_mediainfo_exists
from RV import RV
import Config


def print_usage(p_b_exit=True):
	print()
	print()
	print("Usage: %s [args]" % sys.argv[0])
	print()
	print("args:")
	print('\t-p: path to existing CEN package or sub-package combined with several arguments like add (-a), delete(-d)...')
	print('\t-c: create new CEN package and initialize folder structure')
	print('\t-a: add content to existing CEN package or sub-package. Use in conjunction with -p and -t')
	print('\t-d: relative path of content to delete from existing CEN package given with -p')
	print('\t-t: type of content to be added - used together with -p, -a. ')
	print('\tSupported sub-package-types (can only be added to a CEN package) are:')
	print('\t\timagePackage, soundPackage, timedTextPackage, audiovisualPackage, componentizedPackage, extraPackage')
	print('\tSupported additional content types are (* = can only be added to a CEN package):')
	print('\t\tancillaryData, playlist*, checkerReport*, provMD, descMD*')
	print('\t-u: update CEN package incl. (re-)calculation of hash values if necessary.')
	print('\t-f: force metadata to be added to CEN package (skip XSD check).')
	print()
	print('\t-h: prints this text')
	print()
	print("Example 1: create new CEN Package")
	print("\tUsage: %s -c /path/to/new/cen/package" % sys.argv[0])
	print()
	print("Example 2: add componentized package to existing CEN Package")
	print(
		"\tUsage: %s -p /path/to/new/cen/package -a /path/to/comp/package -t \"componentizedPackage\"" %
		sys.argv[0])
	print()
	print("Example 3: delete content from existing CEN Package")
	print("\tUsage: %s -p /path/to/cen/package -d relative/path/to/existing/content" % sys.argv[0])

	if p_b_exit is True:
		sys.exit(1)


def main(argv):

	rv = RV.info("Starting " + argv[0])
	package_path = None
	add_content_path = None
	update_path = None
	delete_path = None
	content_type = None
	generate_stub_path_abs = None
	create_package = False
	name_item = ''
	Config.force_add_and_skip_schema_check = False

	try:
		opts, args = getopt.getopt(argv[1:], 'p:a:u:d:t:c:g:fh', ['vds='])
		for opt, arg in opts:
			if opt in '-h':
				print_usage()
			elif opt in '-p':
				package_path = arg
			elif opt in '-a':
				add_content_path = arg
			elif opt in '-u':
				update_path = arg
			elif opt in '-d':
				delete_path = arg
			elif opt in '-t':
				content_type = arg
			elif opt in '-g':
				generate_stub_path_abs = arg
			elif opt in '-f':
				Config.force_add_and_skip_schema_check = True
			elif opt in '-c':
				create_package = True
				package_path = arg
			else:
				print_usage()
	except getopt.GetoptError as e:
		log.error("While trying to parse command line arguments: " + str(e))
		print_usage()

	rv_media_info_exists = check_mediainfo_exists()
	if rv_media_info_exists.is_error():
		log.error('Unable to find mediainfo on current system.')
		log.error('Please install mediainfo and make sure it is in the system path.')
		print_usage()

	if package_path is not None:
		if os.path.exists(package_path) is False and create_package is False:
			log.error('Given path for package not valid: ' + package_path)
			print_usage()

	if create_package is True:
		if os.path.exists(package_path) is False:
			rv_create = PreservationPackage.create(package_path)
			rv.add_child(rv_create)
			if rv.is_info():
				log.info('Successfully created CEN package: ' + package_path)
			else:
				rv.dump()
		else:
			log.error('Given path for new package already exists: ' + package_path)
			print_usage()

	elif add_content_path is not None:
		if os.path.exists(add_content_path) is False:
			log.error('Given path for asset to add is not valid: ' + add_content_path)
			print_usage()
		if package_path is None:
			log.error('Please provide path to existing package.')
			print_usage()
		if os.path.exists(package_path) is False:
			log.error('Given path for existing package not valid: ' + package_path)
			print_usage()
		if content_type is None:
			log.error('Please provide type of asset you want to add!')
			print_usage()
		if content_type not in PreservationPackage.sub_package_types and content_type \
			not in PreservationPackage.additional_content_types:
			log.error('Unknown/Unsupported asset type: ' + content_type)
			print_usage()

		if PreservationPackage.is_preservation_package(package_path):
			log.info("Adding " + add_content_path + " to CENPackage " + package_path)
			rv_add_item = PreservationPackage.add_item(content_type, package_path, add_content_path, name_item)
			rv_add_item.dump()
			if rv_add_item.is_error():
				log.error("While trying to add item to CENPackage: " + package_path)
			else:
				log.info("DONE: Added new file/folder to CENPackage: " + add_content_path)
			return
		elif SubPackage.is_sub_package(package_path):
			if content_type in PreservationPackage.sub_package_types:
				# sub-package can only be added to a CPP's root folder. Check given path
				if not PreservationPackage.is_preservation_package(package_path):
					log.error(
						'sub-packages can only be added to CPP root folders. Given argument not pointing '
						'to a CPP: ' + package_path)
					print_usage()

			if content_type in ["playlist", "checkerReport", "descMD"]:
				log.error(content_type + ' can only be added to a CPP. Given path not valid: ' + package_path)
				print_usage()

			if content_type == "ancillaryData":
				rv_add_item = SubPackage.add_item("ancillaryData", package_path, add_content_path)
				rv_add_item.dump()
				if rv_add_item.is_error():
					log.error("While trying to add item to CENPackage: " + package_path)
				else:
					log.info("DONE: Added provMD to sub-package: " + add_content_path)
				return
			elif content_type == "provMD":
				rv_add_item = SubPackage.add_item("provMD", package_path, add_content_path)
				rv_add_item.dump()
				if rv_add_item.is_error():
					log.error("While trying to add item to CENPackage: " + package_path)
				else:
					log.info("DONE: Added provMD to CENPackage: " + add_content_path)
				return
		else:
			log.error('Given path is neither a Preservation package nor a sub-package.')
			print_usage()

	elif update_path is not None:
		rv_update = RV.info("Updating " + update_path + " in CENPackage")

		full_update_path = os.path.join(package_path, update_path)
		if os.path.exists(full_update_path) is False:
			rv_update.add_child(RV.error('Given path for asset to update is not valid: ' + update_path))
			print_usage(p_b_exit=False)
		else:
			rv_sub = PreservationPackage.update_item(package_path, update_path)
			rv_update.add_child(rv_sub)
		rv.add_child(rv_update)
		rv.dump()
		if rv.is_error():
			log.error("While trying to update sub-package item to CENPackage: " + update_path)
		elif rv.is_info():
			log.info("DONE: Updated (sub-)package: " + full_update_path)
		return

	elif delete_path is not None:
		if package_path is None:
			log.error("No parameter for existing CEN Package given! Add -p parameter.")
			print_usage()
		if not os.path.exists(package_path):
			log.error('Given CENPackage does not exists: ' + package_path)
			print_usage()
		full_path = os.path.join(package_path, delete_path)
		if os.path.exists(full_path) is False:
			log.error('CENPackage does not contain a sub folder with relative path: ' + delete_path)
			print_usage()
		rv_sub = PreservationPackage.delete_item(package_path, delete_path)
		rv.add_child(rv_sub)
		rv.dump()
		return

	elif generate_stub_path_abs is not None:
		rv_gen_stub = RV.info("Generating metadata stub: " + generate_stub_path_abs)
		if os.path.exists(generate_stub_path_abs):
			log.error(
				"File for storing metadata stub already exists: " + generate_stub_path_abs)
			print_usage()

		dir_name_abs = os.path.dirname(generate_stub_path_abs)
		if os.path.exists(dir_name_abs) is False:
			log.error(
				"Directory for storing descriptive metadata stub not existing: " + dir_name_abs)
			print_usage()

		if content_type not in PreservationPackage.stub_generators:
			log.error(
				"Stub generation for content type (-t) not supported: " + content_type)
			print_usage()

		if content_type == "variantDescMD":
			rv_sub = PreservationPackage.generate_descriptive_metadata_stub(generate_stub_path_abs)
			rv_gen_stub.add_child(rv_sub)
			rv.add_child(rv_gen_stub)
			if rv.is_error():
				log.error(
					"While trying to generate variant descriptive metadata stub: " + generate_stub_path_abs)
				rv.dump()
			elif rv.is_info():
				log.info("DONE: Generated variant descriptive metadata stub: " + generate_stub_path_abs)
		else:
			log.error('Unknown content type for generating a stub: ' + content_type)
			print_usage()

	else:
		print_usage()


if __name__ == "__main__":
	main(sys.argv)
