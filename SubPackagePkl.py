import os
import Config
import magic
from utils.Hash import Hash
from utils.log import log
from lxml import etree
from mets.Root import Root
from mets.TechMd import TechMd
from mets.FileGrp import FileGrp
from mets.File import File
from mets.Fptr import Fptr
from mets.PhysicalStructMapDiv import PhysicalStructMapDiv
from mets.DigiprovMd import DigiprovMd
from utils.utils import get_file_numbering_info
from ebucore.Constants import Constants
from Content.AbstractPackage import AbstractPackage
from Content.ComponentizedPackage import ComponentizedPackage
from RV import RV
from utils.utils import get_folders_and_files


class SubPackagePkl:
	def __init__(self):
		pass

	@staticmethod
	def package_type_to_prefix(package_type):
		"""
		Returns a package-prefix from a given package type if possible. Example: sub-packages type is
		componentizedPackage -> function would return componentized

		:param package_type: type of the sub-package
		:return: If successful, function returns package-type, otherwise an empty string ''
		"""
		if 'Package' in package_type:
			prefix = package_type.replace('Package', '')
			return prefix
		else:
			log.warning('Unknown package type - cannot translate to CPP prefix: ' + package_type)
			return ''

	@staticmethod
	def check_data_changed(path_package_abs, path_subpackage_rel):
		# ToDo: Maybe this function can be made more generic so that it can also check the ancillaryData folder.
		# ToDo: Add considering file's modification time to the result
		# ToDo: Maybe merge with gen_mets_file_elements_list since code looks similar
		"""
		Checks if files in a sub-package's data-folder have been changed. This function is looking for a simple
		file existence - it does not look for time-stamps detecting if e.g. a data file in the folder was updated
		after the PKL has been written.

		:param path_package_abs: absolute path to package
		:param path_subpackage_rel: relative path to sub package
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) True if data changes detected, otherwise False
		"""
		path_sub_package_abs = os.path.join(path_package_abs, path_subpackage_rel)
		rv = RV.info('Checking if data part in subpackage changed or not: ' + path_sub_package_abs)

		path_subpackage_pkl = os.path.join(path_sub_package_abs, 'packingList.xml')

		if not os.path.exists(path_subpackage_pkl):
			return rv.add_child(RV.error('Cannot find a PKL in subpackage: ') + path_sub_package_abs)

		try:
			root_pkl = etree.parse(path_subpackage_pkl)
		except etree.XMLSyntaxError as err:
			rv.add_child(RV.error('While trying to parse subpackage PKL: ' + str(err)))

		mets_file_elems = root_pkl.xpath('mets:fileSec/mets:fileGrp/mets:file', namespaces=Constants.ns)
		mets_file_ptrs = root_pkl.xpath(
			'mets:structMap/mets:div/mets:div[@TYPE="data"]/mets:ftpr', namespaces=Constants.ns)

		data_files = get_folders_and_files(os.path.join(path_sub_package_abs, 'data'), folder_depth=1)

		paths_existing_data_files_abs = []
		paths_removed_data_files_abs = []
		for data_file in data_files:
			paths_existing_data_files_abs.append(os.path.join(path_sub_package_abs, 'data', data_file[2]))

		for mets_file_ptr in mets_file_ptrs:
			# go through list of known METS file entries
			for existing_file_element in mets_file_elems:
				if existing_file_element.get('ID') == mets_file_ptr.get('FILEID'):
					# read path of known file
					try:
						flocat_elem = existing_file_element.xpath('mets:FLocat', namespaces=Constants.ns)[0]
					except IndexError as _:
						return rv.add_child(RV.error('Cannot find FLocat Element in existing METS stub.'))

					data_path_rel = flocat_elem.get('{%s}href' % Constants.ns["xlink"])
					data_path_abs = os.path.join(path_sub_package_abs, data_path_rel)

					if data_path_abs in paths_existing_data_files_abs:
						paths_existing_data_files_abs.remove(data_path_abs)

					if not os.path.exists(data_path_abs):
						paths_removed_data_files_abs.append(data_path_abs)
						continue

		data_changed = False
		if len(paths_existing_data_files_abs) > 0:
			rv.add_child(RV.info('Files have been added to sub-package.' + path_package_abs))
			data_changed = True
		if len(paths_removed_data_files_abs) > 0:
			rv.add_child(RV.info('Files have been removed from sub-package.' + path_package_abs))
			data_changed = True
		return rv, data_changed

	@staticmethod
	def get_ppkl_adm_md_id(path):
		# ToDo: move this function to CENPackage
		"""
		Opens the preservationPackingList.xml and tries to read the admMd
		:param path: points to a CEN Package's root folder
		:return: If successful, function returns adm_md, otherwise None
		"""

		if os.path.exists(path) is False:
			log.error('Given path to CEN Package does not exists: ' + path)
			return None

		ppkl_path = os.path.join(path, 'preservationPackingList.xml')
		if os.path.exists(ppkl_path) is False:
			log.error('No preservationPackingList.xml in given path: ' + path)
			return None

		try:
			root = etree.parse(ppkl_path)
			try:
				amdSec_elem = root.xpath('//mets:mets/mets:amdSec', namespaces=Constants.ns)[0]
				return amdSec_elem.get('ID')
			except IndexError:
				log.error('No amdSec element in preservationPackingList.xml: ' + str(ppkl_path))
				return None
		except etree.XMLSyntaxError as err:
			log.error('While trying to parse preservationPackingList.xml xml file: ' + str(ppkl_path))
			return None

	@staticmethod
	def _write(path_sub_package_abs, sub_package_type, sub_package_id):
		'''
		Central function for writing a sub-package's PacKing List (PKL). Calls various co-routines delivering the
		necessary data for a proper PKL.

		:param path_sub_package_abs: absolute path to sub package
		:param sub_package_type: type of sub package - must be one of CENPackage.content_types
		:param sub_package_id: identifier of sub package
		:return:cenpackager return value - might contain child elements describing warnings and errors
		'''
		rv = RV.info('Writing subpackage packing list for subpackage: ' + path_sub_package_abs)
		sub_package_id = str(sub_package_id)
		root = Root()
		rv_sub, root_elem = root.collect_metadata(None, None)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ---------------------------------------------------------------------------
		# set creator tool name
		# ---------------------------------------------------------------------------
		name_elem = root_elem.find('./mets:metsHdr/mets:agent/mets:name', Constants.ns)
		if name_elem is None:
			rv.add_child(RV.warning('Cannot find name Element in generated METS stub.'))
		name_elem.text = Config.APP_NAME + ' ' + Config.APP_VERSION

		# ---------------------------------------------------------------------------
		# remove dmdSec according to 7.4.4.1
		# ---------------------------------------------------------------------------
		dmd_sec_elem = root_elem.find('./mets:dmdSec', Constants.ns)
		if dmd_sec_elem is not None:
			root_elem.remove(dmd_sec_elem)

		# ---------------------------------------------------------------------------
		# update amdMD UUID according to 7.4.4.1
		# ---------------------------------------------------------------------------
		amd_sec_elem = root_elem.find('./mets:amdSec', Constants.ns)
		if amd_sec_elem is None:
			return rv.add_child(RV.error('Cannot find amdSec Element in generated METS stub.'))

		# ---------------------------------------------------------------------------
		# add either techMD or digiprovMd element for each metadata file in ./metadata
		# ---------------------------------------------------------------------------
		metadata_folder_path = os.path.join(path_sub_package_abs, 'metadata')
		for metadata_file in os.listdir(metadata_folder_path):
			if metadata_file.endswith('.xml') is False:
				continue

			full_metadata_file_path = os.path.join(path_sub_package_abs, 'metadata', metadata_file)

			numbering_info = get_file_numbering_info(full_metadata_file_path)
			number = str(numbering_info[1]).zfill(numbering_info[0])

			if metadata_file.startswith('techMD_'):
				tech_md = TechMd()
				rv_sub, tech_md_elem = tech_md.gen_stub()
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv

				tech_md_elem.set('ID', 'techMD_' + sub_package_id + '_' + number)

				# get mdRef element
				md_ref_elem = tech_md_elem.find('./mets:mdRef', Constants.ns)
				if md_ref_elem is None:
					return rv.add_child(RV.error('Cannot find mdRef Element in generated METS stub.'))

				md_ref_elem.set('{%s}href' % Constants.ns["xlink"], os.path.join('metadata', metadata_file))
				md_ref_elem.set('SIZE', str(os.path.getsize(full_metadata_file_path)))
				md_ref_elem.set('CHECKSUM', Hash.get_string_representation(full_metadata_file_path))
				md_ref_elem.set('CHECKSUMTYPE', 'SHA-256')

				amd_sec_elem.insert(len(list(amd_sec_elem)), tech_md_elem)

			elif metadata_file.startswith('digiProvMD_'):
				digiprov_md = DigiprovMd()
				rv_sub, digiprov_md_elem = digiprov_md.gen_stub()
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv

				digiprov_md_elem.set('ID', 'digiprovMD_' + sub_package_id + '_' + number)
				# get mdRef element
				md_ref_elem = digiprov_md_elem.find('./mets:mdRef', Constants.ns)
				if md_ref_elem is None:
					return rv.add_child(RV.error('Cannot find mdRef Element in generated METS stub.'))

				md_ref_elem.set('{%s}href' % Constants.ns["xlink"], os.path.join('metadata', metadata_file))
				md_ref_elem.set('SIZE', str(os.path.getsize(full_metadata_file_path)))
				md_ref_elem.set('CHECKSUM', Hash.get_string_representation(full_metadata_file_path))
				md_ref_elem.set('CHECKSUMTYPE', 'SHA-256')

				amd_sec_elem.insert(len(list(amd_sec_elem)), digiprov_md_elem)
			else:
				return rv.add_child(RV.error('Unknown file name in supackage''s metadata folder.' + metadata_file))

		# get updated file list and file pointer list for "data" in subpackage
		rv_sub, mets_data_file_list, mets_data_file_ptr_list = \
			SubPackagePkl.get_mets_file_ftprs_and_mets_file_elements(path_sub_package_abs, sub_package_type,
																	 sub_package_id, mets_type="data")
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		rv_sub, mets_anc_data_file_list, mets_anc_data_file_ptr_list = \
			SubPackagePkl.get_mets_file_ftprs_and_mets_file_elements(path_sub_package_abs, sub_package_type,
																	 sub_package_id, mets_type="ancillaryData")
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv
		# ---------------------------------------------------------------------------
		# fileSec (7.4.4.3)
		# ---------------------------------------------------------------------------
		file_sec_elem = root_elem.find('./mets:fileSec', Constants.ns)
		if file_sec_elem is None:
			return RV.error('Cannot find fileSec Element in generated METS stub.')

		# ---------------------------------------------------------------------------
		# fileSec for data
		# ---------------------------------------------------------------------------
		file_grp = FileGrp()
		rv_sub, file_grp_elem = file_grp.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		file_grp_elem.set('ID', 'data_' + sub_package_id)
		file_sec_elem.insert(len(file_sec_elem), file_grp_elem)

		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		for mets_file_elem in mets_data_file_list:
			file_grp_elem.insert(len(file_grp_elem), mets_file_elem)

		# ---------------------------------------------------------------------------
		# fileSec for ancillary data
		# ---------------------------------------------------------------------------
		rv_sub, file_grp_elem = file_grp.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		file_grp_elem.set('ID', 'ancillaryData_' + sub_package_id)
		file_sec_elem.insert(len(file_sec_elem), file_grp_elem)

		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		for mets_file_elem in mets_anc_data_file_list:
			file_grp_elem.insert(len(file_grp_elem), mets_file_elem)

		# ---------------------------------------------------------------------------
		# Find physical struct map
		# ---------------------------------------------------------------------------
		try:
			struct_map_physical_elem = root_elem.xpath(
				'//mets:mets/mets:structMap[@TYPE="physical"]', namespaces=Constants.ns)[0]

			for child in struct_map_physical_elem:
				struct_map_physical_elem.remove(child)  # remove child elements if there are any
		except IndexError:
			return RV.error('No physical structMap found in subpackage packingList.xml')

		# ---------------------------------------------------------------------------
		# physical struct map 7.4.4.6
		# ---------------------------------------------------------------------------
		struct_map_div = PhysicalStructMapDiv()
		rv_sub, struct_map_div_elem = struct_map_div.gen_stub()
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		type_prefix = SubPackagePkl.package_type_to_prefix(sub_package_type)
		struct_map_div_elem.set('ID', 'subPackage_' + sub_package_id)
		struct_map_div_elem.set('LABEL', type_prefix + '_' + sub_package_id)
		struct_map_div_elem.set('TYPE', type_prefix)

		# ---------------------------------------------------------------------------
		# Append new physical struct map div to physical struct map element
		# ---------------------------------------------------------------------------
		struct_map_physical_elem.insert(len(struct_map_physical_elem), struct_map_div_elem)

		# ---------------------------------------------------------------------------
		# physical struct map for ancillaryData
		# ---------------------------------------------------------------------------
		try:
			data_elem = struct_map_div_elem.xpath('./mets:div[starts-with(@ID, "ancillaryData_")]', namespaces=Constants.ns)[0]
			data_elem.set('ID', 'ancillaryData' + sub_package_id)

			for mets_file_ptr in mets_anc_data_file_ptr_list:
				data_elem.insert(len(data_elem), mets_file_ptr)

		except IndexError as _e:
			# data_elem is optional and might be missing
			pass

		# ---------------------------------------------------------------------------
		# physical struct map for data
		# ---------------------------------------------------------------------------
		try:
			data_elem = struct_map_div_elem.xpath('./mets:div[starts-with(@ID, "data_")]', namespaces=Constants.ns)[0]
			data_elem.set('ID', 'data_' + sub_package_id)

			# ---------------------------------------------------------------------------
			# get admMD-id from preservationPackingList
			# ---------------------------------------------------------------------------
			amd_md_id = SubPackagePkl.get_ppkl_adm_md_id(os.path.dirname(path_sub_package_abs))
			if amd_md_id is None:
				return RV.error('No amdSec element in preservationPackingList.xml: ' + str(path_sub_package_abs))

			data_elem.set('ADMID', amd_md_id)

			for mets_file_ptr in mets_data_file_ptr_list:
				data_elem.insert(len(data_elem), mets_file_ptr)

		except IndexError as _e:
			# data_elem is optional and might be missing
			pass

		# ---------------------------------------------------------------------------
		# write subpackage pkl
		# ---------------------------------------------------------------------------
		path_packing_list = os.path.join(path_sub_package_abs, 'packingList.xml')
		tree = etree.ElementTree(root_elem)
		tree.write(path_packing_list, pretty_print=True, xml_declaration=True, encoding="UTF-8")

		return rv

	@staticmethod
	def get_mets_file_ftprs_and_mets_file_elements(
			path_sub_package_abs, sub_package_type, sub_package_id, mets_type="data"):
		'''
		Reads existing mets:ftpr and mets:file elements from sub package's PKL (if exists).
		Calls co-routine to update existing mets entries and/or to generate new entries for new or unknown files
		in case of an update (e.g. adding ancillary files to an existing sub package)

		:param path_sub_package_abs: absolute path to sub package
		:param sub_package_type: type of sub package - must be one of CENPackage.content_types
		:param sub_package_id: identifier of sub package
		:param mets_type: can be "data" or "ancillary_data"
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) Updated mets:file list representing files according to param mets_type
				(3) Updated mets:ftpr list representing files according to param mets_type
		'''
		rv = RV.info('Read existing mets file ptrs and mets file elements for : ' + path_sub_package_abs)

		if mets_type not in ["data", "ancillaryData"]:
			return rv.add_child(RV.error("Given type must be either data or ancillaryData, not: " + mets_type))

		known_mets_file_elems = []
		known_mets_file_ptrs = []

		# -----------------------------------------------------------------
		# Read existing mets:file and mets:ftps from sub package's PKL
		# -----------------------------------------------------------------
		# open existing PKL from sub package (if exists)
		path_subpackage_pkl = os.path.join(path_sub_package_abs, 'packingList.xml')
		if os.path.exists(path_subpackage_pkl):
			try:
				root_pkl = etree.parse(path_subpackage_pkl)
			except etree.XMLSyntaxError as err:
				rv.add_child(RV.error('While trying to parse subpackage PKL: ' + str(err)))

			# read mets:fileGrp/mets:file from exsiting PKL
			# This reads all elements, no matter which type they are.
			mets_file_elems_tmp = root_pkl.xpath('mets:fileSec/mets:fileGrp/mets:file', namespaces=Constants.ns)

			# filter for either "data" or "ancillaryData" defined by param mets_type
			known_mets_file_elems = []
			for mets_file_elem in mets_file_elems_tmp:
				if mets_file_elem.attrib['ID'].startswith(mets_type):
					known_mets_file_elems.append(mets_file_elem)

			# read mets file ptrs from structMap. Here, we can directly filter for either "data" or "ancillayData"
			# defined by param mets_type
			known_mets_file_ptrs = root_pkl.xpath(
				'mets:structMap/mets:div/mets:div[@TYPE="' + mets_type + '"]/mets:ftpr', namespaces=Constants.ns)

		use_data_numbering = True
		if sub_package_type in ['componentizedPackage', 'audiovisualPackage']:
			use_data_numbering = False

		# -----------------------------------------------------------------
		# generate mets:file elements
		# -----------------------------------------------------------------
		# Call co-routine to generate a list of mets:file elements representing the current files in the sub package.
		# As parameter, we submit all exiting/known mets:file elements if we were able to read them from the PKL.
		# Thus, the co-routine can detect, if some of the files are already known. This can be utilized to prevent
		# from unnecessary re-calculation of hash values (especially for large files).
		# If files are older (timestamp taken from file-system) than the existing PKL, we assume that
		# they haven't been changed. Thus, we can re-use the pre-calculated hash values and save time.
		rv_sub, mets_file_list = SubPackagePkl.gen_mets_file_elements_list(
			path_sub_package_abs, str(sub_package_id), known_mets_file_elems, known_mets_file_ptrs, mets_type,
			use_data_numbering)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# -----------------------------------------------------------------
		# generate mets:ftpr elements
		# -----------------------------------------------------------------
		# Finally, generate mets:ftpr elements for each mets:file generated by co-routine.
		updated_file_ptr_list = []
		for file_elem in mets_file_list:
			fptr = Fptr()
			rv_sub, fptr_elem = fptr.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv
			fptr_elem.set('FILEID', file_elem.get('ID'))
			updated_file_ptr_list.append(fptr_elem)

		return rv, mets_file_list, updated_file_ptr_list

	@staticmethod
	def update(path_sub_package_abs, sub_package_type, sub_package_id):
		"""
		Function might be obsolete since update functionality moved to interval function _write

		:param path_sub_package_abs: absolute path to sub package
		:param sub_package_type: type of sub package - must be one of CENPackage.content_types
		:param sub_package_id: identifier of sub package
		:return: cenpackager return value - might contain child elements describing warnings and errors which occurred
				during processing of this request
		"""
		rv = RV.info('Update subpackage packing list for subpackage: ' + path_sub_package_abs)

		rv_sub = SubPackagePkl._write(
			path_sub_package_abs, sub_package_type, sub_package_id)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		return rv

	@staticmethod
	def gen_mets_file_elements_list(
			path_sub_package_abs, sub_package_id, existing_mets_file_elements=[], existing_mets_file_ptrs=[],
			sub_folder='data', use_data_numbering=True):
		"""
		Scans files given by params path_sub_package_abs and sub_folder and generates met:file elements.
		If params existing_mets_file_elements and/or existing_mets_file_ptrs carry values from an exising sub-package's
		PKL, function checks if the files, the existing entries are pointing to, are younger or older than the PKL
		(file-system timestamp). If the files are older than the PKL it is valid to take existing values rather than
		recalculating them. Especially, the hash value calculation for large files can take long and should be avoided
		if possible. Indeed, if the files are younger than the PKL hash values must be recalculated.

		:param path_sub_package_abs: absolute path to sub package
		:param sub_package_id: identifier of sub package
		:param existing_mets_file_elements: list of existing (or known) mets:file elements taken from sub-packages PKL
		:param existing_mets_file_ptrs: list of existing (or known) mets:ftpr elements taken from sub-packages PKL
		:param sub_folder: sub-package's sub-folder that should be scanned for files to be listed as mets-file elements
		:param use_data_numbering: Defines if data in @sub_folder shall be enriched with a number (e.g. image sequences)
									true or false depending on the sub-packages types
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) Updated mets:file list representing files according to param sub_folder
		"""
		rv = RV.info('Generating METS file elements list for subpackage: ' + sub_package_id)

		# -----------------------------------------------------------------
		# Get reference time from sub-packages PKL (if exists)
		# -----------------------------------------------------------------
		path_subpackage_pkl = os.path.join(path_sub_package_abs, 'packingList.xml')
		if not os.path.exists(path_subpackage_pkl):
			mod_time_pkl = 0
		else:
			mod_time_pkl = os.path.getmtime(path_subpackage_pkl)

		# -----------------------------------------------------------------
		# Retrieve list of all files in given sub-folder
		# -----------------------------------------------------------------
		subpackage_data_path_abs = os.path.join(path_sub_package_abs, sub_folder)
		data_files = get_folders_and_files(subpackage_data_path_abs, folder_depth=1)

		paths_existing_data_files_abs = []
		paths_removed_data_files_abs = []
		for data_file in data_files:
			paths_existing_data_files_abs.append(os.path.join(subpackage_data_path_abs, data_file[2]))

		file_cnt = 0
		file_list = []
		# -----------------------------------------------------------------
		# For each known/existing mets:ftpr
		# -----------------------------------------------------------------
		for mets_file_ptr in existing_mets_file_ptrs:
			# -----------------------------------------------------------------
			# For each known/existing mets:file => look for matches
			# -----------------------------------------------------------------
			for existing_file_element in existing_mets_file_elements:
				if existing_file_element.get('ID') == mets_file_ptr.get('ID'):
					# -----------------------------------------------------------------
					# Find relative path of file defined in mets:file element and check
					# if the file is still present in the folder we're scanning.
					# It is possible that files have been removed manually, without
					# updating the PKL.
					# -----------------------------------------------------------------
					try:
						flocat_elem = existing_file_element.xpath('mets:FLocat', namespaces=Constants.ns)[0]
					except IndexError as _:
						return rv.add_child(RV.error('Cannot find FLocat Element in existing METS stub.'))

					data_path_rel = flocat_elem.get('{%s}href' % Constants.ns["xlink"])
					data_path_abs = os.path.join(path_sub_package_abs, data_path_rel)

					if data_path_abs in paths_existing_data_files_abs:
						paths_existing_data_files_abs.remove(data_path_abs)

					if not os.path.exists(data_path_abs):
						paths_removed_data_files_abs.append(data_path_abs)
						continue
					# -----------------------------------------------------------------
					# Generate mets:file element for this file.
					# -----------------------------------------------------------------
					file_instance = File()
					rv_sub, new_file_element = file_instance.gen_stub()
					if not rv_sub.is_info():
						rv.add_child(rv_sub)
						if rv_sub.is_error():
							return rv, []

					# -----------------------------------------------------------------
					# Set FLocat's parameters: href as relative path
					# -----------------------------------------------------------------
					new_file_locat_elem = new_file_element.find('./mets:FLocat', Constants.ns)
					known_file_path = flocat_elem.get('{%s}href' % Constants.ns["xlink"])
					new_file_locat_elem.set('{%s}href' % Constants.ns["xlink"], known_file_path)

					# -----------------------------------------------------------------
					# Set mets:file's ID parameter
					# -----------------------------------------------------------------
					if use_data_numbering:
						numbering_info = get_file_numbering_info(data_path_abs)
						number = str(numbering_info[1]).zfill(numbering_info[0])
						file_ptr_id = sub_folder + '_' + sub_package_id + '_' + number
					else:
						file_ptr_id = sub_folder + '_' + sub_package_id + '_' + str(file_cnt)
						file_cnt += 1
					new_file_element.set('ID', file_ptr_id)

					# -----------------------------------------------------------------
					# Re-use or re-calculate values for MIMETYPE, SIZE and CHECKSUM and
					# CHECKSUMTYPE, depending on the data-file's modification time
					# -----------------------------------------------------------------
					mod_time_data_file = os.path.getmtime(data_path_abs)
					if mod_time_data_file > mod_time_pkl:
						new_file_element.set('MIMETYPE', magic.from_file(data_path_abs, mime=True))
						new_file_element.set('SIZE', str(os.path.getsize(data_path_abs)))
						new_file_element.set('CHECKSUM', Hash.get_string_representation(data_path_abs))
						new_file_element.set('CHECKSUMTYPE', 'SHA-256')
					else:
						new_file_element.set('MIMETYPE', existing_file_element.get('MIMETYPE'))
						new_file_element.set('SIZE', existing_file_element.get('SIZE'))
						new_file_element.set('CHECKSUM', existing_file_element.get('CHECKSUM'))
						new_file_element.set('CHECKSUMTYPE', existing_file_element.get('CHECKSUMTYPE'))

					file_list.append(new_file_element)

		# -----------------------------------------------------------------
		# Take care about files that have not been listed in the PKL yet.
		# Such files were added after the last PKL generation.
		# -----------------------------------------------------------------
		for new_file in paths_existing_data_files_abs:
			# log.info("Found new file in data folder: " + new_file)
			# -----------------------------------------------------------------
			# Generate mets:file element for this file.
			# -----------------------------------------------------------------
			file_instance = File()
			rv_sub, new_file_element = file_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, []

			# -----------------------------------------------------------------
			# Set FLocat's parameters: href as relative path
			# -----------------------------------------------------------------
			new_file_locat_elem = new_file_element.find('./mets:FLocat', Constants.ns)
			new_file_locat_elem.set(
				'{%s}href' % Constants.ns["xlink"], os.path.join(sub_folder, os.path.basename(new_file)))

			if use_data_numbering:
				numbering_info = get_file_numbering_info(new_file)
				number = str(numbering_info[1]).zfill(numbering_info[0])
				file_ptr_id = sub_folder + '_' + sub_package_id + '_' + number
			else:
				file_ptr_id = sub_folder + '_' + sub_package_id + '_' + str(file_cnt)
				file_cnt += 1

			# -----------------------------------------------------------------
			# Calculate values for MIMETYPE, SIZE and CHECKSUM and CHECKSUMTYPE
			# -----------------------------------------------------------------
			new_file_element.set('ID', file_ptr_id)
			new_file_element.set('MIMETYPE', magic.from_file(new_file, mime=True))
			new_file_element.set('SIZE', str(os.path.getsize(new_file)))
			new_file_element.set('CHECKSUM', Hash.get_string_representation(new_file))
			new_file_element.set('CHECKSUMTYPE', 'SHA-256')

			file_list.append(new_file_element)

		for lost_file in paths_removed_data_files_abs:
			log.info("File that was present in old PKL not exists anymore: " + lost_file)

		return rv, file_list
