import os
from utils.log import log
from utils.Hash import Hash
import magic
from SubPackage import SubPackage
from lxml import etree
from mets.Mptr import Mptr
from mets.MdRef import MdRef
from utils.utils import num_descriptive_metadata_files, get_id_from_filename
from mets.Root import Root
from mets.FileGrp import FileGrp
from mets.File import File
from mets.Fptr import Fptr
from mets.DigiprovMd import DigiprovMd
from utils.utils import get_file_numbering_info
from ebucore.Constants import Constants
from ebucore.XmlTypes import XmlEntry, XmlAttribute
from RV import RV


class PreservationPackagePkl:

	"""
	Handles all (CEN)PackingList related functionality.
	Most of the functions are implemented statically
	"""
	def __init__(self):
		self.dmdSec_id = None
		self.amdSec_id = None

	@staticmethod
	def get_folders_and_files(dir_path, folder_depth=999999):
		"""
		Provides a list of all files and folders in given path and deeper path structure.
		Example return value for folders
		[(0, ''),
		(1, 'subfolder1')] where 1st tupel value is the depth (0=same folder, 1=one folder underneath...) and 2nd tupel
		value is the path to the sub folder.
		Example return value for files
		[(0, '', 'filename1.txt),
		(1, 'subfolder1', , 'filename1.txt)] where 1st tupel value is the depth (0=same folder, 1=one folder
		underneath...), 2nd tupel value is the path to the sub folder and 3rd tupel value the file name.

		Parameters
		----------
		dir_path : str, mandatory
			path of the folder to be scanned for files, sub folders and files in sub folders
		folder_depth : int, optional
			search depth
		"""
		all_folders = []
		dirs = [x[0] for x in os.walk(dir_path)]
		for directory in dirs:
			dir_rel = directory.replace(dir_path, '')

			if dir_rel.count(os.path.sep) < folder_depth:
				if dir_rel.startswith('/'):
					dir_rel = dir_rel[1:]
				all_folders.append((dir_rel.count(os.path.sep), dir_rel))

		# all files with path
		all_files = []

		for root, dirs, files in os.walk(dir_path):
			for file in files:
				if os.path.basename(file) in [".DS_Store"]:
					continue
				relative_path = os.path.relpath(root, dir_path)
				if relative_path == '.':
					relative_path = ''
				if relative_path.count(os.path.sep) < folder_depth:
					all_files.append((relative_path.count(os.path.sep), relative_path, file))

		all_files.sort(reverse=True)
		l = list(dict.fromkeys(all_files))
		l.sort(reverse=True)

		return all_folders, l

	@staticmethod
	def get_deepest_nested_paths(root):
		"""
		Provides list starting with the root's sub folders. Routine than finds the deepest path for each sub folder.
		Example: There are two folder structure in sub_folder_1:
		1. sub_folder_1/Content/00/sub1/sub2/sub3/
		2. sub_folder_1/Content/01/sub1/
		Routine will return 1. since the path is deeper compared to 2.

		Parameters
		----------
		root : str, mandatory
			path of the folder to be scanned for sub folders
		"""
		deep_paths_list = list()
		for dir_paths, dir_names, file_names in os.walk(root):
			if not dir_names:
				relative_path = dir_paths.replace(root, '')
				deep_paths_list.append(relative_path)
		return deep_paths_list

	def update_administrative_metadata(self, cpp_path_abs, pkl_elem, cpp_uuid):
		"""
		updates mets:amdSec element in the PKL. For this, existing elements that might already exist in the PKL will
		be removed and selected items in the metadata folder (e.g. digiProvMD-files) will be added.

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_uuid: unique identifier of CEN Preservation Package
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info('Updating administrative metadata for: ' + cpp_path_abs)
		# find descriptive metadata
		amd_sec_elem = pkl_elem.find('./mets:amdSec', Constants.ns)

		if amd_sec_elem is None:
			return RV.error('amdSec element is None')

		# remove child elements (if any)
		for child in list(amd_sec_elem):
			amd_sec_elem.remove(child)

		# set ID
		self.amdSec_id = 'amdMD_' + str(cpp_uuid)
		amd_sec_elem.set('ID', self.amdSec_id)

		metadata_folder_path = os.path.join(cpp_path_abs, 'metadata')
		for metadata_file in os.listdir(metadata_folder_path):

			if metadata_file in ['.DS_Store']:
				continue

			full_metadata_file_path = os.path.join(cpp_path_abs, 'metadata', metadata_file)

			if metadata_file.startswith('digiProvMD_'):
				digiprov_md = DigiprovMd()
				rv_sub, digiprov_md_elem = digiprov_md.gen_stub()
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv

				numbering_info = get_file_numbering_info(full_metadata_file_path)
				number = str(numbering_info[1]).zfill(numbering_info[0])

				digiprov_md_elem.set('ID', 'digiprovMD_' + cpp_uuid + '_' + number)
				# get mdRef element
				md_ref_elem = digiprov_md_elem.find('./mets:mdRef', Constants.ns)
				if md_ref_elem is None:
					return rv.add_child(RV.error('Cannot find mdRef Element in generated METS stub.'))

				md_ref_elem.set('{%s}href' % Constants.ns["xlink"], os.path.join('metadata', metadata_file))
				md_ref_elem.set('SIZE', str(os.path.getsize(full_metadata_file_path)))
				md_ref_elem.set('CHECKSUM', Hash.get_string_representation(full_metadata_file_path))
				md_ref_elem.set('CHECKSUMTYPE', 'SHA-256')

				amd_sec_elem.insert(len(list(amd_sec_elem)), digiprov_md_elem)

			elif metadata_file.startswith('descMD_'):
				# nothing to do here - will be added to dmdSec
				pass
			else:
				return rv.add_child(RV.error('Unknown file name in package''s metadata folder.' + metadata_file))

		rv.message = 'Successfully updated administrative metadata for: ' + cpp_path_abs
		return rv

	def update_descriptive_metadata(self, cpp_path_abs, pkl_elem, cpp_uuid):
		"""
		updates mets:dmdSec element in the PKL. For this, existing elements that might already exist in the PKL will
		be removed and selected items in the metadata folder (e.g. descMD-files) will be added.

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_uuid: unique identifier of CEN Preservation Package
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info('Updating descriptive metadata for: ' + cpp_path_abs)

		dmd_sec_elem = pkl_elem.find('./mets:dmdSec', Constants.ns)

		# remove child elements (if any)
		for child in list(dmd_sec_elem):
			dmd_sec_elem.remove(child)

		# update ID
		self.dmdSec_id = 'descMD_' + str(cpp_uuid)
		dmd_sec_elem.set('ID', self.dmdSec_id)

		cpp_metadata_path = os.path.join(cpp_path_abs, 'metadata')
		for f in os.listdir(cpp_metadata_path):
			if f.startswith('descMD_') is False:
				continue

			full_path = os.path.join(cpp_metadata_path, f)
			md_ref = MdRef()
			rv_sub, md_ref_elem = md_ref.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

			md_ref_elem.set('{%s}href' % Constants.ns["xlink"], os.path.join('metadada', f))
			md_ref_elem.set('SIZE', str(os.path.getsize(full_path)))
			md_ref_elem.set('CHECKSUM', Hash.get_base_64(full_path))
			md_ref_elem.set('CHECKSUMTYPE', 'SHA-256')

			dmd_sec_elem.append(md_ref_elem)

		rv.message = 'Successfully updated descriptive metadata for: ' + cpp_path_abs
		return rv

	@staticmethod
	def preservation_package_uuid(preservation_package_path_abs):
		"""
		Reads the preservation package's uuid from mets:structMap

		:param preservation_package_path_abs: absolute path tp CPP
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) package's uuid if it could be extracted, otherwise None
		"""

		rv = RV.info("Read preservation package uuid from: " + preservation_package_path_abs)

		if os.path.isdir(preservation_package_path_abs) is False:
			return rv.add_child(RV.error(
					'Given path does not point to a preservation package folder: ' +
					preservation_package_path_abs), None)

		pkl_path = os.path.join(preservation_package_path_abs, 'preservationPackingList.xml')
		if os.path.exists(pkl_path) is False:
			return rv.add_child(RV.error(
				'No packing list in preservation package path: ' + preservation_package_path_abs), None)

		try:
			root = etree.parse(pkl_path)
			mets_struct_map = root.xpath('/mets:mets/mets:structMap[@TYPE="physical"]/mets:div', namespaces=Constants.ns)
			if len(mets_struct_map) == 0:
				return rv.add_child(RV.error(
					'No mets:structMap found in preservation packing list: ' + preservation_package_path_abs), None)

			mets_struct_map = mets_struct_map[0]
			preservation_package_id = mets_struct_map.get("ID")
			if preservation_package_id is None:
				return rv.add_child(RV.error(
					'No ID in mets:structMap element here: ' + preservation_package_path_abs), None)

			if preservation_package_id.startswith('preservationPackage_') is False:
				return rv.add_child(RV.error(
					'Unexpected ID format, expected "preservationPackage_XYZ", got: ' + preservation_package_id), None)

			uuid_only = preservation_package_id.replace('preservationPackage_', '')
			return rv, uuid_only

		except etree.XMLSyntaxError as err:
			return rv.add_child(
				RV.error('While trying to parse preservation package packing list xml file: ' + str(err)), None)

		return rv, None

	@staticmethod
	def update_ancillary_data_struct_map(pkl_elem, cpp_path_abs, ancillary_data_file_ptr_list):
		"""
		Updates mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="ancillaryData"] according to 8.3.4.10

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param ancillary_data_file_ptr_list: list of mets:ftpr elements usually referring to mets:file elements
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""

		rv = RV.info('Updating ancillary data structMap for CPP: ' + cpp_path_abs)

		try:
			ancillary_data_div_elem = pkl_elem.xpath(
				'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="ancillaryData"]',
				namespaces=Constants.ns)[0]
			# remove child elements (if any)
			for child in list(ancillary_data_div_elem):
				ancillary_data_div_elem.remove(child)
		except IndexError:
			return rv.add_child(RV.error(
				'Cannot find ancillaryData div in packing list:' + cpp_path_abs))

		for file_ptr_id in ancillary_data_file_ptr_list:
			fptr_instance = Fptr()
			rv_sub, fptr_element = fptr_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

			fptr_element.set('ID', file_ptr_id)

			ancillary_data_div_elem.insert(len(ancillary_data_div_elem), fptr_element)

		return rv

	@staticmethod
	def update_playlist_data_struct_map(pkl_elem, cpp_path_abs, playlist_data_file_ptr_list):
		"""
		Updates mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="playlists"]' according to 8.3.4.11

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param playlist_data_file_ptr_list: list of mets:ftpr elements usually referring to mets:file elements
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""

		rv = RV.info('Updating playlist data structMap for CPP: ' + cpp_path_abs)

		try:
			playlist_div_elem = pkl_elem.xpath(
				'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="playlists"]',
				namespaces=Constants.ns)[0]
			# remove child elements (if any)
			for child in list(playlist_div_elem):
				playlist_div_elem.remove(child)
		except IndexError:
			return rv.add_child(RV.error(
				'Cannot find playlists div in packing list:' + cpp_path_abs))

		for file_ptr_id in playlist_data_file_ptr_list:
			fptr_instance = Fptr()
			rv_sub, fptr_element = fptr_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, playlist_data_file_ptr_list

			fptr_element.set('FILEID', file_ptr_id)

			playlist_div_elem.insert(len(playlist_div_elem), fptr_element)

		return rv

	@staticmethod
	def update_checker_reports_data_struct_map(pkl_elem, cpp_path_abs, checker_report_file_ptr_list):
		"""
		Updates mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="checkerReports"]' according to 8.3.4.14

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param checker_report_file_ptr_list: list of mets:ftpr elements usually referring to mets:file elements
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""
		rv = RV.info('Updating checker report data structMap for CPP: ' + cpp_path_abs)
		# rv.add_child(RV.warning("Function is skipped since checker reports should not be part of the package at all"))
		return rv

		checker_report_div_elem = Root.checker_reports_elem(pkl_elem)

		if checker_report_div_elem is None:
			rv_sub, status = Root.add_checker_reports_div_element(pkl_elem)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

			checker_report_div_elem = Root.checker_reports_elem(pkl_elem)

		else:
			# remove child elements (if any)
			for child in list(checker_report_div_elem):
				checker_report_div_elem.remove(child)

		for file_ptr_id in checker_report_file_ptr_list:
			fptr_instance = Fptr()
			rv_sub, fptr_element = fptr_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, checker_report_file_ptr_list

			fptr_element.set('FILEID', file_ptr_id)

			checker_report_div_elem.insert(len(checker_report_div_elem), fptr_element)

		return rv

	@staticmethod
	def update_subpackage_data_struct_map(pkl_elem, cpp_path_abs, subpackages_packing_lists_rel_path_list):
		"""
		Updates mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="subPackages"]' according to 8.3.4.12

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param subpackages_packing_lists_rel_path_list: list of mets:ftpr elements usually referring to mets:file elements
		:return: cenpackager return value - might contain child elements describing warnings and errors
		"""

		rv = RV.info('Updating subpackages data structMap for CPP: ' + cpp_path_abs)

		try:
			sub_packages_div_elem = pkl_elem.xpath(
				'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div/mets:div[@TYPE="subPackages"]',
				namespaces=Constants.ns)[0]
			# remove child elements (if any)
			for child in list(sub_packages_div_elem):
				sub_packages_div_elem.remove(child)
		except IndexError:
			return rv.add_child(RV.error(
				'Cannot find ancillaryData div in packing list:' + cpp_path_abs))

		for packing_List_path_rel in subpackages_packing_lists_rel_path_list:
			mptr_instance = Mptr()
			rv_sub, mptr_element = mptr_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv
			mptr_element.set('{%s}href' % Constants.ns["xlink"], packing_List_path_rel)

			sub_packages_div_elem.insert(len(sub_packages_div_elem), mptr_element)

		return rv

	@staticmethod
	def update_ancillary_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid):
		"""
		Updates /mets:mets/mets:fileSec/mets:fileGrp[starts-with(@ID, "ancillaryData_") according to 8.3.4.5.
		Function is looking for files in sub-folder ancillaryData

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param cpp_uuid: uuid of CPP
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) list of mets:ftpr elements pointing to the new mets:file elements generated here
		"""

		rv = RV.info('Updating ancillary data fileGrp element for CPP: ' + cpp_path_abs)

		ancillary_data_file_ptr_list = list()
		# ------------------------------------------------------------
		# 8.3.4.4
		# ------------------------------------------------------------
		try:
			file_sec_elem = pkl_elem.xpath('/mets:mets/mets:fileSec', namespaces=Constants.ns)[0]
		except IndexError:
			return rv.add_child(RV.error(
				'While trying to write packing list: fileSec element is None')), ancillary_data_file_ptr_list

		ancillary_data_path = os.path.join(cpp_path_abs, 'ancillaryData')

		try:
			ancillary_data_grp_elem = file_sec_elem.xpath(
				'./mets:fileGrp[starts-with(@ID, "ancillaryData_")]', namespaces=Constants.ns)[0]

			# remove child elements (if any)
			for child in list(ancillary_data_grp_elem):
				ancillary_data_grp_elem.remove(child)
		except IndexError:
			ancillary_data_file_grp = FileGrp()
			rv_sub, ancillary_data_grp_elem = ancillary_data_file_grp.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, ancillary_data_file_ptr_list
			ancillary_data_grp_elem.set('ID', 'ancillaryData' + '_' + cpp_uuid + '_fileGrp')
			file_sec_elem.insert(len(file_sec_elem), ancillary_data_grp_elem)

		if os.path.exists(ancillary_data_path) is False:
			return rv, ancillary_data_file_ptr_list

		file_cnt = 0
		for file in sorted(os.listdir(ancillary_data_path)):
			if os.path.basename(file) in [".DS_Store"]:
				continue
			full_data_file_path = os.path.join(ancillary_data_path, file)
			data_file_path_rel = os.path.join('ancillaryData', file)
			file_instance = File()
			rv_sub, file_elem = file_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, ancillary_data_file_ptr_list

			# ---------------------------------------------------------------------------
			# get FLocat element
			# ---------------------------------------------------------------------------
			file_locat_elem = file_elem.find('./mets:FLocat', Constants.ns)
			if file_locat_elem is None:
				return rv.error('Cannot find FLocat Element in generated METS stub.'), ancillary_data_file_ptr_list

			file_locat_elem.set('{%s}href' % Constants.ns["xlink"], data_file_path_rel)

			ancillary_file_id = 'ancillaryData_' + cpp_uuid + '_' + str(file_cnt).zfill(4)
			file_elem.set('ID', ancillary_file_id)
			file_elem.set('MIMETYPE', magic.from_file(full_data_file_path, mime=True))
			file_elem.set('SIZE', str(os.path.getsize(full_data_file_path)))
			file_elem.set('CHECKSUM', Hash.get_string_representation(full_data_file_path))
			file_elem.set('CHECKSUMTYPE', 'SHA-256')

			ancillary_data_grp_elem.insert(len(ancillary_data_grp_elem), file_elem)
			ancillary_data_file_ptr_list.append(ancillary_file_id)
			file_cnt += 1

		return rv, ancillary_data_file_ptr_list

	@staticmethod
	def update_playlist_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid):
		"""
		Updates /mets:mets/mets:fileSec/mets:fileGrp[starts-with(@ID, "playlists_") according to 8.3.4.6.
		Function is looking for files in sub-folder playlists

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param cpp_uuid: uuid of CPP
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) list of mets:ftpr elements pointing to the new mets:file elements generated here
		"""

		rv = RV.info('Updating playlists data fileGrp element for CPP: ' + cpp_path_abs)
		playlist_data_file_ptr_list = list()

		# ------------------------------------------------------------
		# 8.3.4.4
		# ------------------------------------------------------------
		try:
			file_sec_elem = pkl_elem.xpath('/mets:mets/mets:fileSec', namespaces=Constants.ns)[0]
		except IndexError:
			return rv.add_child(RV.error(
				'While trying to write packing list: fileSec element is None')), playlist_data_file_ptr_list

		# check if there is an existing entry for playlists
		playlist_file_grp_elem = file_sec_elem.xpath(
			'./mets:fileGrp[starts-with(@ID, "playlists_")]', namespaces=Constants.ns)
		if len(playlist_file_grp_elem) == 0:
			playlist_file_grp = FileGrp()
			rv_sub, playlist_file_grp_elem = playlist_file_grp.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, playlist_data_file_ptr_list
			file_sec_elem.insert(len(file_sec_elem), playlist_file_grp_elem)
		else:
			playlist_file_grp_elem = playlist_file_grp_elem[0]

		# set id playlist_file_grp_elem
		playlist_file_grp_elem.set('ID', 'playlists_' + cpp_uuid + '_fileGrp')
		# remove all entries from
		for child in playlist_file_grp_elem:
			playlist_file_grp_elem.remove(child)

		playlists_path = os.path.join(cpp_path_abs, 'playlists')
		if os.path.exists(playlists_path) is False:
			return rv, playlist_data_file_ptr_list

		file_cnt = 0
		for file in sorted(os.listdir(playlists_path)):
			if os.path.basename(file) in [".DS_Store"]:
				continue

			full_data_file_path = os.path.join(playlists_path, file)
			data_file_path_rel = os.path.join('playlists', file)
			file_instance = File()
			rv_sub, file_elem = file_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, playlist_data_file_ptr_list

			# ---------------------------------------------------------------------------
			# get FLocat element
			# ---------------------------------------------------------------------------
			file_locat_elem = file_elem.find('./mets:FLocat', Constants.ns)
			if file_locat_elem is None:
				return rv.error('Cannot find FLocat Element in generated METS stub.'), playlist_data_file_ptr_list

			file_locat_elem.set('{%s}href' % Constants.ns["xlink"], data_file_path_rel)

			playlist_file_id = 'playlist_' + cpp_uuid + '_' + str(file_cnt).zfill(4)
			file_elem.set('ID', playlist_file_id)
			file_elem.set('MIMETYPE', magic.from_file(full_data_file_path, mime=True))
			file_elem.set('SIZE', str(os.path.getsize(full_data_file_path)))
			file_elem.set('CHECKSUM', Hash.get_string_representation(full_data_file_path))
			file_elem.set('CHECKSUMTYPE', 'SHA-256')

			playlist_file_grp_elem.insert(len(playlist_file_grp_elem), file_elem)
			playlist_data_file_ptr_list.append(playlist_file_id)
			file_cnt += 1

		return rv, playlist_data_file_ptr_list

	@staticmethod
	def update_checker_report_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid):
		"""
		Updates /mets:mets/mets:fileSec/mets:fileGrp[starts-with(@ID, "checkerReports_") according to 8.3.4.7.
		Function is looking for sub-folders in cpp_path_abs containing sub-packages

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param cpp_uuid: uuid of CPP
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) list of relative paths (from cpp_path_abs) to all sub-packages currently available
		"""

		rv = RV.info('Updating checker reports data fileGrp element for CPP: ' + cpp_path_abs)
		checker_report_data_file_ptr_list = list()

		# ------------------------------------------------------------
		# 8.3.4.4
		# ------------------------------------------------------------
		try:
			file_sec_elem = pkl_elem.xpath('/mets:mets/mets:fileSec', namespaces=Constants.ns)[0]
		except IndexError:
			return rv.add_child(RV.error(
				'While trying to write packing list: fileSec element is None')), checker_report_data_file_ptr_list

		# check if there is an existing entry for playlists
		checker_reports_file_grp_elem = file_sec_elem.xpath(
			'./mets:fileGrp[starts-with(@ID, "checkerReports_")]', namespaces=Constants.ns)
		if len(checker_reports_file_grp_elem) == 0:
			checker_report_file_grp = FileGrp()
			rv_sub, checker_reports_file_grp_elem = checker_report_file_grp.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, checker_report_data_file_ptr_list
			file_sec_elem.insert(len(file_sec_elem), checker_reports_file_grp_elem)
		else:
			checker_reports_file_grp_elem = checker_reports_file_grp_elem[0]

		# set id checkerReports_file_grp_elem
		checker_reports_file_grp_elem.set('ID', 'checkerReports_' + cpp_uuid + '_fileGrp')
		# remove all entries from
		for child in checker_reports_file_grp_elem:
			checker_reports_file_grp_elem.remove(child)

		checker_reports_path = os.path.join(cpp_path_abs, 'checkerReports')
		if os.path.exists(checker_reports_path) is False:
			return rv, checker_report_data_file_ptr_list

		file_cnt = 0
		for file in sorted(os.listdir(checker_reports_path)):
			if os.path.basename(file) in [".DS_Store"]:
				continue

			full_data_file_path = os.path.join(checker_reports_path, file)
			data_file_path_rel = os.path.join('checkerReports', file)
			file_instance = File()
			rv_sub, file_elem = file_instance.gen_stub()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv, checker_report_data_file_ptr_list

			# ---------------------------------------------------------------------------
			# get FLocat element
			# ---------------------------------------------------------------------------
			file_locat_elem = file_elem.find('./mets:FLocat', Constants.ns)
			if file_locat_elem is None:
				return rv.error('Cannot find FLocat Element in generated METS stub.'), checker_report_data_file_ptr_list

			file_locat_elem.set('{%s}href' % Constants.ns["xlink"], data_file_path_rel)

			checker_report_file_id = 'checkerReport_' + cpp_uuid + '_' + str(file_cnt).zfill(4)
			file_elem.set('ID', checker_report_file_id)
			file_elem.set('MIMETYPE', magic.from_file(full_data_file_path, mime=True))
			file_elem.set('SIZE', str(os.path.getsize(full_data_file_path)))
			file_elem.set('CHECKSUM', Hash.get_string_representation(full_data_file_path))
			file_elem.set('CHECKSUMTYPE', 'SHA-256')

			checker_reports_file_grp_elem.insert(len(checker_reports_file_grp_elem), file_elem)
			checker_report_data_file_ptr_list.append(checker_report_file_id)
			file_cnt += 1

		return rv, checker_report_data_file_ptr_list

	@staticmethod
	def update_subpackage_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid):
		"""
		Updates /mets:mets/mets:fileSec/mets:fileGrp[starts-with(@ID, "subpackages_") according to 8.3.4.8.
		Function is looking for sub-folders in cpp_path_abs containing sub-packages

		:param pkl_elem: XML Element of PKL to be written. Might be an PKL-element pointing to an existing PKL
		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param cpp_uuid: uuid of CPP
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
				(2) list of relative paths (from cpp_path_abs) to all sub-packages currently available
		"""

		rv = RV.info('Updating subpackages data fileGrp element for CPP: ' + cpp_path_abs)
		subpackages_packing_lists_rel_path_list = list()

		# ------------------------------------------------------------
		# 8.3.4.4
		# ------------------------------------------------------------
		try:
			file_sec_elem = pkl_elem.xpath('/mets:mets/mets:fileSec', namespaces=Constants.ns)[0]

		except IndexError:
			return rv.add_child(RV.error(
				'While trying to write packing list: fileSec element is None')), subpackages_packing_lists_rel_path_list

		# go through all children an remove sub elements for subpackages
		# ToDo: Check if that makes sense. Rather than continuing on ancillaryData_, playlists_ or checkerReports_
		# ToDO: if might make more sense to check on subpackages and to delete those entries
		
		# ToDo: Check if functionality can be moved to a function and used by other update_xyz functions here
		for child in list(file_sec_elem):
			file_grp_id = child.get('ID')
			if file_grp_id.startswith('ancillaryData_'):
				continue
			elif file_grp_id.startswith('playlists_'):
				continue
			elif file_grp_id.startswith('checkerReports_'):
				continue
			else:
				file_sec_elem.remove(child)

		for file in sorted(os.listdir(cpp_path_abs)):
			if SubPackage.is_sub_package(os.path.join(cpp_path_abs, file)):

				subpackage_prefix = SubPackage.prefix(os.path.join(cpp_path_abs, file))

				# ToDo: should this raise an error?
				if subpackage_prefix == '':
					continue

				subpackage_prefix += 'Package'

				subpackage_uuid = SubPackage.subpackage_uuid(os.path.join(cpp_path_abs, file))
				# ToDo: should this raise an error?
				if subpackage_uuid == '':
					continue

				subpackage_file_grp_id = subpackage_prefix + '_' + subpackage_uuid + '_fileGrp'

				sub_packages_file_grp = FileGrp()
				rv_sub, sub_packages_file_grp_elem = sub_packages_file_grp.gen_stub()
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv
				file_sec_elem.insert(len(file_sec_elem), sub_packages_file_grp_elem)

				sub_packages_file_grp_elem.set('ID', subpackage_file_grp_id)

				file_instance = File()
				rv_sub, file_elem = file_instance.gen_stub()
				if not rv_sub.is_info():
					rv.add_child(rv_sub)
					if rv_sub.is_error():
						return rv, subpackages_packing_lists_rel_path_list

				# ---------------------------------------------------------------------------
				# get FLocat element
				# ---------------------------------------------------------------------------
				file_locat_elem = file_elem.find('./mets:FLocat', Constants.ns)
				if file_locat_elem is None:
					return rv.error(
						'Cannot find FLocat Element in generated METS stub.'), subpackages_packing_lists_rel_path_list

				path_subpackage_pkl_abs = os.path.join(cpp_path_abs, file, 'packingList.xml')
				path_subpackage_pkl_rel = os.path.join(file, 'packingList.xml')
				file_locat_elem.set('{%s}href' % Constants.ns["xlink"], path_subpackage_pkl_rel)

				subpackage_file_id = subpackage_prefix + '_' + subpackage_uuid + '_packingList'
				file_elem.set('ID', subpackage_file_id)
				file_elem.set('MIMETYPE', 'text/xml')
				file_elem.set('SIZE', str(os.path.getsize(path_subpackage_pkl_abs)))
				file_elem.set('CHECKSUM', Hash.get_string_representation(path_subpackage_pkl_abs))
				file_elem.set('CHECKSUMTYPE', 'SHA-256')

				sub_packages_file_grp_elem.insert(len(sub_packages_file_grp_elem), file_elem)
				subpackages_packing_lists_rel_path_list.append(path_subpackage_pkl_rel)

		return rv, subpackages_packing_lists_rel_path_list

	def update_preservation_packing_list(self, cpp_path_abs, cpp_uuid):
		"""
		Updates a Preservation Package PaKing List PPKL according to 7.3. This function serves as the main access
		point for updating the PPKL - it calls various other function internally. In no errors occurs,
		function writes PPKL to file-system.

		:param cpp_path_abs: absolute path pointing to the CPP's root folder
		:param cpp_uuid: uuid of CPP
		:return:(1) cenpackager return value - might contain child elements describing warnings and errors
		"""

		rv = RV.info('Updating preservation packing list for CPP: ' + cpp_path_abs)
		# see 7.3

		cpp_pkl_path = os.path.join(cpp_path_abs, 'preservationPackingList.xml')
		# open existing packing list if exists or build new stub
		pkl = Root()
		if os.path.exists(cpp_pkl_path) is True:
			try:
				tree = etree.parse(cpp_pkl_path)
			except etree.XMLSyntaxError as err:
				return rv.add_child(RV.error(
					'While trying to parse xml file ' + cpp_path_abs + ', error is: ' + str(err)))

			pkl_elem = pkl.elem = tree.getroot()
		else:
			rv_sub, pkl_elem = pkl.collect_metadata(media_source_file=None, xml_metadata_file_in=None)
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

		if pkl_elem is None:
			return rv.add_child(RV.error('PKL XML Root element is None.'))

		if num_descriptive_metadata_files(os.path.join(cpp_path_abs, 'metadata')) > 0:
			rv_sub = pkl.add_dmd_sec_element()
			if not rv_sub.is_info():
				rv.add_child(rv_sub)
				if rv_sub.is_error():
					return rv

		# ------------------------------------------------------------
		# Descriptive metadata
		# ------------------------------------------------------------
		rv_sub = self.update_descriptive_metadata(cpp_path_abs, pkl_elem, cpp_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# Administrative metadata
		# ------------------------------------------------------------
		rv_sub = self.update_administrative_metadata(cpp_path_abs, pkl_elem, cpp_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# Ancillary files
		# ------------------------------------------------------------
		rv_sub, ancillary_data_file_ptr_list = self.update_ancillary_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# Playlist files
		# ------------------------------------------------------------
		rv_sub, playlist_file_ptr_list = self.update_playlist_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# Subpackages
		# ------------------------------------------------------------
		rv_sub, subpackages_packing_lists_rel_path_list = \
			self.update_subpackage_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# Checker report files
		# ------------------------------------------------------------
		rv_sub, checker_report_file_ptr_list = \
			self.update_checker_report_file_grp_element(pkl_elem, cpp_path_abs, cpp_uuid)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ---------------------------------------------------------------------------
		# Set structMap sub div parameters
		# ---------------------------------------------------------------------------
		try:
			struct_map_div_elem = pkl_elem.xpath(
				'/mets:mets/mets:structMap[@TYPE="physical"]/mets:div', namespaces=Constants.ns)[0]

			struct_map_div_elem.set('ID', 'preservationPackage_' + cpp_uuid)
			struct_map_div_elem.set('ADMID', self.amdSec_id)
			struct_map_div_elem.set('DMDID', self.dmdSec_id + '_0000')
			struct_map_div_elem.set('LABEL', os.path.basename(cpp_path_abs))

		except IndexError:
			return rv.add_child(RV.error('Cannot find subPackages div in packing list:' + cpp_pkl_path))

		# ------------------------------------------------------------
		# Ancillary files
		# ------------------------------------------------------------
		rv_sub = self.update_ancillary_data_struct_map(pkl_elem, cpp_path_abs, ancillary_data_file_ptr_list)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# Playlist files
		# ------------------------------------------------------------
		rv_sub = self.update_playlist_data_struct_map(pkl_elem, cpp_path_abs, playlist_file_ptr_list)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# Sub-packages
		# ------------------------------------------------------------
		rv_sub = self.update_subpackage_data_struct_map(pkl_elem, cpp_path_abs, subpackages_packing_lists_rel_path_list)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# checkerReports
		# ------------------------------------------------------------
		rv_sub = self.update_checker_reports_data_struct_map(pkl_elem, cpp_path_abs, checker_report_file_ptr_list)
		if not rv_sub.is_info():
			rv.add_child(rv_sub)
			if rv_sub.is_error():
				return rv

		# ------------------------------------------------------------
		# write new PKL
		# ------------------------------------------------------------
		tree = etree.ElementTree(pkl_elem)
		tree.write(cpp_pkl_path, pretty_print=True, xml_declaration=True, encoding="utf-8")

		rv.message = 'Successfully updated preservation packing list for CPP: ' + cpp_path_abs
		return rv
